# Architectures for Big Data

## Table of contents

1. Introduction
   - Definition of software architecture
2. Software architecture
   1. Building- vs Software- architecture
      - Multiple points of view
        - Zachman framework
      - Architectural styles
        - Interpretations
        - Erosion and Drift
        - High-level set of elements
        - Properties, relationships and rationale
        - Examples
      - Styles are connected to materials
        - Subsystems
      - Project pyramid
   2. Architectures as a framework for abstractions
      - Abstraction definition
      - Abstraction development
      - Subsystems
      - Abstraction phase
   3. Architecture pillars Satisfying requirements Design
      - Requirements
      - Design
      - Cost estimation and process management
      - Component reuse
      - Centralization
      - Productivity and security
      - Enterprise application integration
      - Scalability
      - Execution
      - Handover and lock-in
   4. Design patterns
      - Definition
      - ETL/ELT
        - Data loss
      - Change data capture
        - Log data vs registry data
        - Invasive approaches
        - Diff&Where
        - Transactional CDC
        - Implementation
      - Wrapper
      - MapReduce
      - Job scheduling
3. Big data
   1. Hadoop
      - Components
      - Architecture
      - Sequence
      - Tools
   2. HDFS
      - Architecture
      - Replication
      - Quorum journal manager
   3. Delta lake
      1. Concurrency control
         - Lock
         - Optimistic vs pessimistic
         - Approaches
      2. Transactional logs
         - Log shipping
      3. Inter-process communication
      4. Delta-lake
         - Motivation
         - Operations
         - Features
4. Docker
   - Motivation
   - Dockerfile
     - Commands
   - Jupyter Docker Stacks
   - Containers
5. Apache Spark
   1. RDD
      - Pair RDD
   2. Architecture
   3. Operations
      - Laziness
      - Bad practices
   4. Shared variables
   5. Join
   6. Dataframe and datasets
6. From Spark to SQL
   1. Memory usage problem
      - SQL architecture
      - SQL statement execution
   2. Connection problem
      - Direct connection
      - Connection pool
   3. Indexed tables problem
   4. BCP and Merge
7. Natural language processing
   - Bag of words
   - Cosine similarity
8. Service Oriented Architecture
   1. Definitions
      - IBM
      - Open group
      - OASIS
      - OMG
      - W3C and XML.com
   2. Modeling languages and tools
      - SOMA
      - SOAML
   3. Core concepts
      - Service
      - Service catalogue
      - Service queue
      - Enterprise service bus
      - Service broker
   4. Communication models
      - Synchronous service calls
      - Asynchronous service calls
      - Stream outbound file reference
      - Pub/sub service callback
      - Batch sync data process
9. From big data to big money
   1. Projects
   2. Costs
   3. AAS vs bare metal

## Introduction

A **software architecture** is a set of _architectural elements_ that have a
_particular form_. The form consists of weighted _properties_ and
_relationships_. An underlying, but integral, part of an architecture is the
_rationale_ for the various choices made in defining an architecture.

The software architecture level requires _new kinds of abstractions_ that
capture essential properties of major _subsystems_ and the _ways they interact_.

## Software architecture

### Building- vs Software- architecture

_"The art or practice of designing and building structures and especially
habitable ones"_

Software architecture principles try to apply concepts of classical architecture
to software.

An architect has to take on **multiple points of view** - economical, technical,
etc. A building has different stakeholders, same as a software application.
Hence, software architecture also needs multiple points of view - processes,
data, network, etc. _Zachman_ proposes a framework for building taking into
account different views:

- _Bubble charts_, a conceptual high-level representation.
- _Architect's drawing_, the transcription of the owner's requirements from the
  owner perspective.
- _Architect's plan_, a final view on the product, focusing on materials, to
  ensure the product reflects the need of the customer
- _Contractor's plan_, the plan from the builder perspective.
- _Shop plan_, a very narrow view for a single specific worker, who doesn't need
  the whole picture.

Then, he proposes another one, focused on information systems:

- _Scope_, a description of the system and how it is going to be used by the
  customer.
- _Business description_, how the scope fits in the customer business, in
  his/her words.
- _ITC systems description_, a representation of the architecture w.r.t. the
  customer information system.
- _Technology model_, a description of the technological choices.
- _Detailed description_, a description of each component, to double check with
  the customer.

**Architectural styles** are employed. An architectural style may be interpreted
_descriptively_, as a codification of _design elements_, or _prescriptively_, as
a set of constraint - _formal relationships_ - for each component. Software
architectural style encapsulates important decisions about elements. It both
constrains the architecture and coordinates the architects. It also prevents two
dangerous issues like _erosion_ - the loss of clearness in the rational between
the architect and developers - and _drift_ - the loss of focus on the purpose of
the project in time.

We consider 3 high-level set of elements:

- _Data elements_, components that contain the information that is used and
  transformed.
- _Connecting elements_, components that glue different pieces of the
  architecture together.
- _Processing elements_, components that supply the transformation on the data
  elements.

**Properties** are used to constrain the choice of architectural elements. They
allow to keep tidiness and avoid erosion and drift. **Relationships** are used
to constrain the placement of architectural elements. The **Rationale** captures
the motivation for every choice in the project.

Some _examples_ of architectural styles: Common object request broker
architecture, Service[^service] oriented architecture, message oriented
architecture.

[^service]:
    A service is a piece of software that has a very peculiar feature:
    it's based on the reuse of itself, decoupling the definition from the
    implementation. It can be understood in a general way by anyone, but it can
    then be implemented in any specific fashion. A sort of intermediate layer
    between business and technology.

**Styles are connected to materials**. Materials have certain properties, both
on aesthetics and engineering. In software architectures material will be
mirrored by _subsystems_. Exactly as materials, different subsystems are better
suited for different use cases.

A software architecture should be built after fully understanding the
_requirements_, as a basis for _design_.

![project pyramid](./assets/images/project_pyramid.png)

### Architectures as a framework for abstractions

An **abstraction** consists in recognizing a pattern, naming it, defining it,
and finding ways to invoke it by its name. A good abstraction is able to
_suppress some details_, understating which are important and which are not.
Examples of abstractions: pipe & filter in unix, object-oriented programming,
layered systems, rule-based systems.

**Abstractions development** starts when someone is tackling a problem in an _ad
hoc_ way. Eventually, some knowledge about the problem and how to best solve it
accumulates: a sort of _folklore_ starts spreading. This knowledge is then
_codified_ and analyzed and hopefully enables a more sophisticated level of
practice that allows to solve harder problems.

Abstractions can be used to combine different **subsystems** into an
architecture. Each subsystem also has its own architecture itself. Sometimes
it's interesting to design only a section of the architecture, instead of the
whole. Each subsystem can perform a _specific function_ for the system or a
_common function_ - e.g. communication, storage. Identifying and classifying the
system functions that are common to many application is a significant first step
to the development of software architecture.

The abstraction phase should be concluded only when the whole process is
visualized through different steps, and an implementation idea exists for each
of those steps. Only then, the implementation phase can start.

### Architecture pillars

1. Being the framework for **satisfying requirements**. With an architecture in
   place, it is easier to check if all the _questions_ posed by the requirements
   have the desired answers. This includes functional, technical and security
   requirements.
2. Being the technical basis for **design**, by which we mean the modularization
   and detailed interfaces of the design elements, their algorithms and
   procedures, and the data types needed to support the architectures and to
   satisfy the requirements.
3. Being the managerial basis for **cost estimation** and **process
   management**.
4. Enabling **component reuse**.
5. Focus on **centralization**, putting a _single source of truth_ in place.
6. Enhancing **productivity** - natural consequence of the previous points - and
   **security**. Which door to open? Is a gateway to SQL data needed?
7. Enabling enterprise systems integration (**Enterprise Application
   Integration**)
8. Allowing tidy **scalability**
9. Controlling software processes **execution**
10. Avoiding **handover** and **people lock-in**[^lock_in]

[^lock_in]:
    The concept of making a customer dependent on a vendor for products
    and services. A supplier successfully locks in a customer when the cost of
    changing supplier is higher than the cost of keeping it even though without
    that cost other suppliers could outperform the actual supplier. This is an
    undesirable situation that should be avoided when designing the
    architecture. A different kind of lock-in - people lock-in - is related to
    _knowledge_. It happens when the cost of knowledge transfer is higher than
    the benefit to dismiss a person/team.

### Design patterns

Design patterns are formalized best practices found to solve common problems.
Hundreds of design patterns exist. Usually we are provided unstructured data and
we want to obtain something to present.

![big data challenge](./assets/images/big_data_challenge.png)

#### ETL/ELT

The passage from unstructured data to structured data can be performed through
an _ETL pattern_[^etl]. This causes some data loss, though. To avoid this, we
can use an **ELT pattern** - e.g. data lake[^data_lake] -, that first load data
and transforms it only afterwards. NEVER transform data before saving it as it
is.

[^etl]:
    Extract Transform Load. The general procedure of copying data from one
    or more sources into a destination system. Data is _extracted_ from
    homogenous or heterogeneous sources. Data is then _cleaned and transformed_
    into a proper format. Data is then _inserted_ into the target database. ETL
    can be used to increase data quality, to normalize data, to apply logics to
    data or to prepare the data for a presentation layer. ETL can be one-shot or
    incremental.

[^data_lake]:
    Tries to tackle the issue of data loss: any bit that is discarded,
    cannot be re-acquired later. It's based on the concept that all available
    information should be acquired and stored forever - e.g. when integrating a
    legacy system. To do so, it uses its capabilities to handle unprocessed
    data, to read/write with good performances, and to provide a schema when
    reading data.

![elt pattern](./assets/images/elt_pattern.png)

#### Change data capture

**Change data capture** is a set of design patterns used to determine - and
track - the data that has changed, so that action can be taken using the changed
data. Two kinds of data exists: _log data_, any insert-only data that records
events and has an idea of chrono-sequence - e.g. a list of your bank
transactions; _register data_, any data that describes something that could
change over time, for which no idea of chrono-sequence exists and edit-delete
operations are available - e.g. your account balance. The chrono-sequence can be
represented explicitly, using a timestamp, or implicitly, using something like a
counter.

![log data](./assets/images/log_data.png)

![registry data](./assets/images/registry_data.png)

This pattern can be implemented in several ways.

**Invasive database-side** adds information directly in the database tables,
like timestamps, version numbers, statuses. This is very complex to handle. For
registry data this slows down `SELECT`, `INSERT`, `UPDATE` and `DELETE`
statements, could cause timeouts and it's not possible if not performed by the
database admin. This could also be done with triggers that trigger any time an
update occurs. This may make the database functioning very cryptic and, as a
consequence, very dangerous.

**Invasive application-side** gives the application the responsibility to
propagate changes. Every time the application commits something, it also sends
the change to the CDC system. Sometimes this is just not possible.

**Invasive database CPU-side** uses CPU heavy processes in order to not touch
database tables or the application code. _Transaction log scanners_ read and
process database technical logs to propagate changes on the CDC system. These
relies on the specific format of the log. _Log shipping_ uses an automatic
database backup process to propagate changes. It's usually employed as a
disaster recovery solution.

All the approaches listed up to now are not very effective because they require
to change tables and database logics, they are very application- and technology-
specific, they don't take into account unpredictable users behavior and could
have a very bad impact on performances.

In the **Diff&Where** pattern, log data are handled with a _where-like_
strategy: anytime a query is done, the information of the last created entry at
that moment is stored, to be used as a `WHERE` condition in the next query.
Registry data is handled with a _diff-like_ strategy: keys and values are
hashed, their hashes are associated to a timestamp and are used later to detect
insertions, updates and deletions. This has the advantage of comparing only two
strings - the hashes - instead of database rows. It also doesn't ask for any
administrator permission and doesn't invade code or database tables in any way.

![diff like](./assets/images/diff_like.png)

![cdc diff and where](./assets/images/cdc_diff_and_where.png)

This system has a big flaw: if the process is interrupted while copying fresh
rows to data lake and before updating `sync.json`, the content of the `.json`
file doesn't represent the actual situation anymore. This is because this
implementation is stateless, it cannot keep track of its state[^state]. We can
fix this problem by introducing a **transactional** behavior that uses a
different file extension - e.g. `.tmp` - when first writing the copied files,
and renaming them only at the end of a successful copy. When starting a new
copy, it first erases each file that has the temporary extension. The act of
renaming the new files is still a point of failure, but a pretty safe one - and
surely much safer than a bulk copy.

[^state]:
    The set of variables and instances of a system at a given time. A
    stateful system is capable of remembering preceding events or user
    interactions - e.g. website's sessions. A stateless system is capable of
    providing a response always in the same way, independently to any previous
    state - e.g. REST API.

![cdc transactional write](./assets/images/cdc_transactional_write.png)

![cdc transactional](./assets/images/cdc_transactional.png)

The CDC pattern turns every master data table into a log table. Each verb on a
row - `INSERT`, `UPDATE`, `DELETE` - creates a new row on the data lake. This
could be a disadvantage when making a join, but also a huge advantage when
computing a join over time.

When **implementing** the CDC, the following components should be implemented:

A _CDC process_, that holds together the whole execution.

| concrete           | abstract         |
| ------------------ | ---------------- |
| `executionTrace()` | `connect()`      |
| `manageSyncJson()` | `getFreshData()` |
| `run()`            | `prepareData()`  |
| `commit()`         | `writeData()`    |
| `rollback()`       |

An abstract _source adapter_.

| abstract        |
| --------------- |
| `connect()`     |
| `submitQuery()` |

An abstract _data manager_.

| abstract             |
| -------------------- |
| `getFreshData()`     |
| `dataToDictParser()` |
| `serialize()`        |

A _destination adapter_, in which the destination path must not be chosen. It
must contain all standard file OS capabilities.

| concrete           | abstract  |
| ------------------ | --------- |
| `writeFreshRows()` | `open()`  |
| `writeSyncFile()`  | `touch()` |
| `readSyncFile()`   | `mkDir()` |
|                    | etc...    |

A _configuration object_, where connection strings and logical names are
specified, as well as the CDC process unique ID/name and the logic of
`getFreshData()` - e.g. an SQL query, the file path and how to parse filenames -
and `parseRow()` - e.g. from `.txt` to key/value. This component is what makes
the CDC really adaptable for the developer.

![cdc abstract implementation](./assets/images/cdc_abstract_implementation.png)

![cdc concrete implementation](./assets/images/cdc_concrete_implementation.png)

#### Adapter

Particularly useful to face changes in the technologies - avoiding the lock-in
effect. It consists in a wrapper on the external service interfaces, so that the
application can interact only with the interface of the wrapper, agnostic of how
the external service interfaces works. If in need of changing the external
service, only the wrapper interface on the external side needs to be changed.
The application is not effected in any way. The wrapper can also impose some
behaviors on the application, allowing a set of operations and hiding - or
constraining - others.

![wrapper](./assets/images/wrapper.png)

#### MapReduce

A programming model to ease multi-node process _parallelization_. It was first
formalized in a ground-braking Google paper in 2003. Combines map functions and
reduce functions. It is a _functional_ style of programming. The complexity lies
in how to split the data in an efficient way. How the data is distributed on the
different nodes can influence the quality of an algorithm on its own. The map
operations will take a single row and return a single row. Reduce operations
will aggregate different rows together. MapReduce works well if we are able to
reduce the communications between the nodes.

![map reduce distribute](./assets/images/map_reduce_distribute.png)

![map reduce](./assets/images/map_reduce.png)

#### Job scheduling

A job is an atomic piece of work - a process. It can be started interactively or
scheduled. Each job needs a name and a unique id. A job can be finite, when it
can complete, fail or terminate, or online, if it can only be stopped by
terminating it. Each job has to be scheduled by a job scheduler, which will
consume it from a job queue. Sometimes a job needs to act as a given client,
this is called job impersonation. Jobs can be sequential or concurrent, which
can overlap over the same time period. A job scheduler might be implemented
using a **broker** pattern.

![job scheduler](./assets/images/job_scheduler.png)

## Big data

### Hadoop

Hadoop is the most common framework to distribute computation. It is built of
mostly open-source components to facilitate the development of multi-node
services. It works on the premise that hardware can fail and that moving
computation is cheaper than moving data. Therefore, it aims at enhancing
_reliability_ of clusters. It's built of three main **components**: A storage
component - _Hadoop distributed file system storage - HDFS_, designed to run on
low-level hardware, highly fault tolerant, providing a very high throughput; a
resource management component - Hadoop _YARN_, which decouples resource
management and job monitoring into separate daemons, using a global resource
manager, a per-machine node manager and a per-application application master; a
parallel computation component - Hadoop _MapReduce_, that takes care of
scheduling and monitoring tasks, re-executing them when they fail. Hadoop is
_location aware_, it needs to know the name of the network switch where each
node is.

Hadoop is based on a very common **architecture**. It has a _job tracker_, which
decides where each task should be executed - ideally, the node that has the
needed data, or the closest one. The _task tracker_ accept tasks and executes
them. It exposes the number of slots available for new tasks. Each task is a
spawned JVM. It also sends an heartbeat to the job tracker every minute, to
ensure it's still alive. A _name node_ it's a list of where each piece of data
is inside the cluster. There can also be a backup NameNode. The _data node_ is
where the data is stored. All these components can be part of the _master node_,
whereas a _slave node_ can only contain data nodes and task trackers.

![hadoop architecture](./assets/images/hadoop_architecture.png)

The **sequence** can evolve like so:

1. The application submits an asynchronous job to the job tracker, which will
   return the job status when polled.
2. The job tracker gets the data locations in the name node.
3. The job tracker identifies the best task tracker with available slots and
   nearest to the data nodes.
4. The job tracker submits the work to the chosen task tracker.
5. Job tracker monitor task tracker's heartbeat. If it doesn't hear back, it
   presumes the task tracker has failed.
6. Upon finishing, each task tracker notifies the final status to the job
   tracker. If it failed, the job tacker can resubmit the job elsewhere, mark
   that specific record as to-skip and blacklist the task tracker as unreliable.

Numerous **tools** have been built on top of Hadoop. _Apache Sqoop_ is a
command-line interface application for transferring data between relational
databases and Hadoop. It supports incremental loads of a single table into HDFS.
_Apache Hive_ is a data warehouse software that provides a SQL-like interface
with a specific dialect with the goal of running queries on HDFS data.

### HDFS

HDFS has a master/slave **architecture** with a single name node and several
data nodes. It uses the name node to expose a logical unique file system, but it
then splits each file into one or more blocks and distributes it in a set of
data nodes. The name node executes file system operations - e.g. opening,
closing, renaming, etc. Data nodes are responsible of serving read and write
requests. Data never flows through the name node, which is and always remains
only a registry. The name node makes all the decisions. Each file is stored as a
sequence of same sized blocks. Each block is replicated for fault tolerance.
Files cannot be updated - expect for appends and truncates - and only one writer
can write on a certain block at a certain moment. Data nodes actively send
information to name node about blocks - which is important, because it allows to
easily add new data nodes, without the name node having to do anything.

![hdfs architecture](./assets/images/hdfs_architecture.png)

HDFS uses several **replication** policies to achieve both reliability and high
throughput. Data nodes are grouped inside _racks_[^racks]. Each rack has an id
and the name node is aware of its location. Being rack-aware allows to optimize
network bandwidth, increase fault tolerance and I/O performance.

[^racks]:
    Containers for virtual machine hardware inside which connections are
    very efficient.

The first possible policy is to _fully replicate_ an entire rack onto two other
racks. Upon a rack failing, I have full redundancy and I can resume computing on
the replicated rack. I also can parallelize and redirect reading between the two
racks. The cost of writes increases significantly. _The following image is
showing only one out of two replicas of the rack_.

![hdfs rack replication](./assets/images/hdfs_rack_replication.png)

_Two rack factor 3 replication policy_ replicates each block one time in the
same rack, but in a different data nodes, and two times in a remote rack, also
in different data nodes. Write performances are improved. Reading and file
distribution are sub-optimal.

![hdfs rack replication 2f3](./assets/images/hdfs_rack_replication_2f3.png)

Data nodes are reliable. If the heartbeat times out, the node is considered dead
and it's replaced by a new data node. Data on data node is also reliable, thanks
to replication and checksums. Node points may cause data loss upon failing. This
can be prevented using **Quorum journal manager**, which handles more that one
name node, keeping one in _active_ state, while the others are in _standby_
state. Updates on the name node are tracked using a _journal node_. The active
node is the only one that is able to write on this log, whereas the standby
nodes are only able to read it. Data nodes know the location of all name nodes,
and they send their heartbeat to all of them. Having a single active node helps
avoiding inconsistencies - split-brain scenario. When the active node fails, a
standby node is elected as active and will take over the role of writing.

### Delta lake

#### Concurrency control

A way to ensure that operations that could overlap in time generate a correct
result as quickly as possible. Typical problems that arise when not performing
this kind of control are _lost updates_, _dirty reads_, _incorrect summary_.
**Locks** are used to monitor the access to a given resource, to ensure
transactionality[^transaction]. This can be as simple as a binary semaphore, or
more complex ones that distinguish different operations - like reading and
writing. Locks come with the risk of _deadlocks_. Several deadlocks prevention
algorithms exists.

[^transaction]:
    A sequence of operations that satisfies the _ACID_ - atomicity,
    consistency, isolation, durability - properties, over which a _rollback_
    operation is always possible, to restore the system to the original -
    consistent - state.

The simplest approach is **optimistic** and it imposes no lock on the resources.
Each transactions perform its operations and commits the changes only if after
completion a check on the fact that the read resource has not being updated by
others is passed. This can lead to a considerable waste of time. The opposite
**pessimistic** approach locks all the resources before beginning any kind of
operation, blocking other transactions if there is a chance of conflict. A
_semi-optimistic_ approach would employ one of the two previous approaches
depending on the kind of operation to be performed.

**Pessimistic transactionality via lock** marks each resource is going to access
and informs the system, releasing the resource only after the transaction ends.
This lock has to be performed before the transaction, even for read-only
operations. This approach results in a substantial overhead as well as blocking
other transactions in a waiting queue. **Optimistic two-phase locking**, based
on _atomic commitment protocol_, begins with a voting phase, in which a
coordinator process instructs each worker on the steps for either committing or
aborting the transaction. The worker then votes to commit or abort the whole
transaction. In the committing phase, the coordinator decides to commit or abort
the transaction depending on the votes of the participants, notifying them on
the result so that they can perform follow-up actions. In **optimistic
multi-version** reading operations always receive an unmodified version of
resources, so that a lock isn't needed. Write operations still lock the
resource. This reduces the overhead but it may provide old versions of data.

#### Transaction logs

Transaction logs contain the history of transactions that happened on a database
and can be used to guarantee ACID properties, fixing inconsistent states by
rolling back uncommitted transactions and by re-applying already committed
transactions. This kind of logs may create huge sized files which could be the
reason for a database failure, by eating up all the disk storage. They allow to
perform **log shipping**, a disaster recovery strategy aimed at increasing
database reliability. Transaction logs are sent on a _slave_ server to create an
exact copy of the _master_ server, where the transactions are happening. This is
of course very performance heavy on the master server. For this reason, this is
usually performed over a subset of the tables.

#### Inter-process communication

It allows different processes to manage shared data. It is often linked with the
client/server architecture where the first requests data to the latter. This
mechanism can be synchronous or asynchronous. The easiest strategy is the
_file_: a process writes a file - a record store on a disk - which can then be
accessed by multiple processes. A _socket_ can be employed to send data over a
network interface to a different process on the same computer or to another
computer on the network. An _anonymous pipe_ uses the OS I/O to create a
one-direction data channel where data is written on one end of the pipe and
consumed at the other end. A _named pipe_ works as a pipe but is treated like a
file, which means it can be accessed by multiple consumers. _Shared memory_
dangerously allows a process to access the memory another process wrote via a
direct pointer.

#### Delta Lake

Big data processes are usually long to compute. Therefore, the needs to ensure
data consistency and to allow processes to communicate between them arise. Delta
Lake is a way to obtain transactionality over HDFS, with the due premise that
this is usually not a good idea. It's been proposed by the same authors of
Spark. The goal is to allow high-performance transactions over cloud object
stores.

Delta lakes **operates** using transaction logs in _Apache
Parquet_[^apache_parquet] format to provide ACID properties. Each table is
stored as a set of objects. The drawback is that multi-object - multi-file -
updates are not atomic, there is no isolation between queries and rollback is
not possible. To fix this, delta lake maintains information about which object
are part of which table using a _write-ahead log_[^write_ahead_log]. The logs
also contain metadata and statistics for each data file, improving performances
on metadata searches. Transactions are achieved using an optimistic concurrency
protocols.

[^apache_parquet]:
    A file format aimed at storing flat columnar files in an
    efficient way, using the _record shredding and assembling_ algorithm. It
    features smart ways for efficient data compression and encoding. Instead of
    serializing and compressing data row-wise, Parquet does it column-wise.
    Compression is more efficient because data is more uniform inside a single
    column. Compression can also be type aware, to get even better result and
    column-specific compression techniques. This is also very effective when
    reading a single column, but much less effective when reading all the
    columns.

[^write_ahead_log]:
    Ensures data consistency and durability by writing all
    modifications to a log before applying them.

Delta lake offers different **features**:

- _Time travel_, that allows to roll back on previous states of the data set as
  well as to query point-in-time snapshots.
- `UPDATE`, `DELETE` and `MERGE` operations, which are usually not available in
  data lakes.
- _Efficient I/O streaming_, decoupling the task of writing from the task of
  optimizing the object structure, making both writes and reads very efficient.
- _Caching_, by using immutable objects and logs.
- _Data layout optimization_.
- _Schema evolution_.
- _Audit logging_.

## Docker

Docker aims at virtualizing[^virtualization] just a part of a virtual machine,
instead of a whole one. It's a set of platform as a service products that uses
OS-level virtualization to deliver software in packages, called _containers_.
Containers are isolated from one another, but able to communicate with each
other through well-defined channel. Isolation is obtained using _cgroups_, a
Linux kernel feature that limits and isolates the resource usage. A single
operating system kernel is able to run all containers and therefore to use fewer
resources tan virtual machines.

[^virtualization]:
    Hardware or platform virtualization refers to the creation of
    a virtual machine that acts like a real computer with an operating system.
    Software executed on these virtual machines is separated from the underlying
    hardware resources. The virtualization is handled by an _hypervisor_. With
    OS-level virtualization, the kernel allows for multiple isolated user space
    instances.

![docker vs vm](./assets/images/docker_vs_vm.png)

A **Dockerfile** is a text document that contains all the commands needed to
assemble the Docker **image**. _Docker build_ commands are executed to create
the docker image in a context relative way. Once the build command is launched,
the _Docker daemon_ takes care of executing all the commands, freeing the CLI.
Each line of the Dockerfile is executed as single step. When updating a line of
a Dockerfile, upon the next execution, Docker re-builds the image starting from
the modified line, to avoid repeating unnecessary steps.

- The first statement of the Dockerfile must be a `FROM` instruction, which
  states the parent image from which we want to define our image - optionally
  preceded only by `ARG` instructions, to pass global scope variables to the
  `FROM` command. It can point to another Dockerfile in the machine, or to any
  public repository. When resolving the `FROM` statement - which may be nested
  -, the root needs to be a valid Linux distribution.
- The `RUN` statement is used to execute any command during the docker build
  command execution - e.g. `RUN pip install pre-commit`.
- The `COPY` command allows to copy a file from the same Dockerfile folder, to
  any location inside te container.
- `CMD` statements don't impact the build phase, but define what needs to be
  executed after starting up the container. Only one `CMD` statement will be
  executed - the last one, if more are defined.
- `ADD` behaves like `COPY`, but allows to copy from a URL.
- `EXPOSE` can be used to expose a protocol/port to the operating system.
- `ENV` adds any kind of local environment.

![docker cheat-sheet](./assets/images/docker_cheat_sheet.png)

Creating a Docker image from scratch would be complex and time consuming. Images
and repositories are open-sourced, such as in [Jupyter Docker
Stacks](https://github.com/jupyter/docker-stacks)[^jupyter].

[^jupyter]:
    The [Jupyter notebook](https://jupyter.org/) is an open-source web
    application that allows to create and share documents that contain live
    code, equations, visualizations and narrative text.

The `docker run` command creates a **container** from an image. A container is
an instance of an image. The `-p` option exposes ports from the container to the
host system. The `-v` option allows to mount folders of the host system inside
the container file system. `docker start` starts the container. Using the
`docker exec -it` command allows to ssh connect to an interactive session inside
the container.

**Docker Compose** uses yaml files to orchestrate docker containers.

## Apache Spark

### RDD

Born in the context of a Netflix challenge to improve the platform algorithm by
10%. The first data abstraction of Apache spark is the **resilient distributed
dataset - RDD**, an object based collection of arbitrary - not bounded in type -
elements partitioned across the nodes. The abstraction is created by reading an
HDFS filesystem, using `.textFile()`, or parallelizing objects in memory, using
`.parallelize()`. Once inside the RDD, the rows are partitioned with an hash
partitioner function, in order to distribute them. The number of partitions can
be set by the user using `.repartition(n)`, to tune the parallelization.

In **Pair-RDD** each element is a composition of a _key_ and a _value_. Both the
key and the value can be anything that can be serialized by a programming
language. The hash partition function is applied to the value of key, so
elements with the same key can be partitioned in the same way. This is the only
version of RDD where is possible to perform key-based transformations. Playing
with the keys can enhance the performance considerably, creating a better
distribution or redistributing the current one.

### Architecture

The **architecture** of an Apache Spark application works like this: a driver
holds the application process; the driver tells to the master to distribute some
kind of computation over workers, using the spark context for scheduling; the
workers are the computational nodes where such computation happens, via their
executors. Everything is based on JVM. Specifically:

- A _driver_, the process responsible for the execution of the spark
  application. It converts a user application in smaller executions called tasks
  and schedules them.
- Inside the driver there is the _SparkContext_, a singleton[^singleton] that
  allows the application to use spark. The _spark context_ is the core object of
  apache spark. It exposes the primitives and it's the only way to create an
  RDD. It takes care of executors, receiving their heartbeat. It must be a
  singleton, w.r.t. a single JVM.
- The driver also stores the _metadata_ about all the RDDs.
- The _master_ is a framework-specific entity, unique per application,
  responsible for negotiating resource with the resource manager on behalf of
  the workers. It is also responsible for their statuses, receiving their
  heartbeats. Several masters may be employed to improve reliability.
- The _worker_ is the computational node. Each worker is aware of every other
  worker thanks to a local block manager. Each worker can start several
  executors as separate processes, all bound the same application.
- _Executors_ are distributed agents, responsible for executing tasks. They
  enable the resiliency of RDD: if one executor fails, all its tasks are moved
  to another executor. Tasks are tracked with an unique id. Executors are also
  used to provide in-memory storage if the user calls the `persist()` or the
  `cache()` methods. This allows to store no data, nor computation inside
  masters.

[^singleton]:
    A design pattern that ensures just one instance of a class or
    object is created. It is called the get or create pattern, because after
    creating the class/object for the first time, every subsequent attempt of
    creation will just result in a get on the already created instance.

![apache spark architecture](./assets/images/apache_spark_architecture.png)

### Operations

**Operations** in Apache Spark are either _transformations_ - map, filter,
reduce -, which don't create any new dataset and don't impose any computation,
or _actions_ - count, first, take _n_ results, sum, max, etc - which do impose
computation. Any transformation made over an RDD just adds a node to a _direct
acyclic graph - DAG_, which is a logical _execution plan_. Transformations can
be piped one after the other. When calling an action, a _DAG scheduler_ is in
charge of creating a physical execution plan starting from the logical execution
plan. The execution plan is composed by stages. Each stage implements the
transformation logic with internal operations which are different from the one
exposed to the application. A stage is executed over a specific group of rows,
giving birth to a task. Tasks are handled by a task scheduler, which launches
them via the cluster manager. The number of tasks depends on the number of
partitions in which the RDD is splitted - which also represents the maximum
number of tasks. Each worker then takes care of the execution of tasks.

![apache spark operations](./assets/images/apache_spark_operations.png)

So, an end to end spark job works like so:

1. A spark application is submitted.
2. The cluster resource manager starts the application master and allocates the
   resources.
3. The application master register itself on the resource manager.
4. The spark driver runs the code.
5. The driver implicitly converts the user code from a logical execution plan to
   a physical execution plan.
6. The driver negotiates the resources and stages are created.
7. Executors start on workers and register themselves with the driver.
8. Executor's status is monitored and redistribution is performed, if needed.
9. The driver sends tasks to the cluster manager, based on data location.
10. The application master launches the container by providing the node manager
    with a container configuration.
11. The first RDD is created by reading data from HDFS into different partitions
    on different nodes in parallel.
12. During the application execution, the driver communicates with the
    application master to obtain the status of the application.
13. Upon completing the application, the application master de-registers itself
    from the resource manager, so that all the resources are freed.

Spark is **lazy**: a result is computed only when needed, and only in the
portion that is needed. This allows spark to optimize the processes without the
user knowing. The result can then be persisted, if needed, but this is not
mandatory as well. To do this, it offers several _map functions_ and several
_reduce functions_, which have to be _commutative_, since we don't know for sure
the order in which the operations will occur.

When working with such a system there are some things to **avoid**: never assume
to be working with a small subset; don't count on row sorting; avoid using
`.collect()`; avoid using `for` iterators both inside and outside lambda
functions.

### Shared variables

An alternative to RDD are **shared variables**. Every time a function is to be
distributed, a copy of each needed variable - values, dictionaries, etc - is
shipped to each task executor. Variables can be write-only, in which case they
are called _accumulators_ and allow workers and executors to increase their
value. Using accumulators can lead to unexpected results. Read-only variables
are called _broadcast_ variables and are shared only once with each node, at the
beginning of the computation. This variable can the be read locally by each task
on that node, reducing communication costs. This is particularly useful when
needing the same data across multiple stages. Broadcast variables can only
contain serializable elements.

### Join

When **joining** two RDDs, we encounter different scenarios. The left operands
will always have _n_ lines, whereas the right operand will always have _m_
lines:

- Joining a registry with a log will produce at most _m_ lines, because the log
  may have multiple lines for the same key.
- For the same reason, joining two logs will produce at most `n * m` lines.
- Joining a registry with another registry will produce at most `min(n, m)`
  lines, because every key appears at most once per table.
- A full outer join[^full_outer_join] between a registry and a log will produce
  _m_ lines if the log is the real log of the registry, and at most `n + m`
  otherwise.
- A full outer join between a log and another log will produce `max(n, m)` if
  one of the two logs has just a single line. It will produce `n * m` if the two
  logs share every key - with possible in-betweens.
- A full outer join between a registry and another registry behaves the same as
  the previous one, except for the fact that being sure about the uniqueness of
  the keys inside each registry lowers the maximum number of lines to `n + m`.

[^full_outer_join]:
    A join that keeps any line from the operand tables, even
    lines that have no respective in the other table. These unmatched lines will
    have undefined attributes in the result where the attributes of the other
    table should have been located.

`.join()` should be used when the amount of lines on the left operand is pretty
big. Using a repartition before joining ensures better distribution of the
computation between workers. When the amount of keys in a log is low, we can
enhance it by adding a random integer to the key, as already seen in previous
use cases. When the amount of keys in a registry is low, we can turn it into a
dictionary, and perform a dict lookup inside a `.map()`, instead of the
`.join()`. This could worsen the performance significantly, so it's worth to use
a broadcast variable to share the dictionary to every worker. In this case, this
is even more performant than a direct join.

![join comparison](./assets/images/join_comparison.png)

### Dataframes and datasets

Introduced with the high level library _Spark SQL_, with the goal of supporting
structured data processing. It requires more information, about the structure of
both the data and the computation, but it enjoys several optimizations that
allow the code to outperform code written for RDDs. It is particularly useful
for developers that don't know the inside and out of Spark. It also provides an
interface for providing classic SQL queries.

A **dataset** is a distributed collection of data that is supposed to combine
advantages of RDDs - strong typing, lambda functions - and Spark SQL - optimized
execution engine. They cannot be used in Python. A **dataframe** is a dataset
organized in named columns. It is conceptually equivalent to a table in a
relational database. Given the prior knowledge of the schema, a dataframe is
able to employ a optimized DAG scheduler that improves the performance.

![apache spark operations
dataframe](./assets/images/apache_spark_operations_dataframe.png)

## From Spark to SQL

When transferring data from numerous Spark workers to a single database,
concurrency on the physical connection is a problem, along with memory usage of
write operations and indexed[^index] tables.

[^index]:
    A data structure that allows to retrieve data in a more efficient way.
    An index is created on a subset of column. They use more space on disk, but
    they allow to quickly locate data without having to search every row,
    providing rapid random lookups and efficient access to ordered columns.
    Writing operations become heavier as a result of introducing indexes.

### Memory usage problem

Inside an **SQL server architecture** two kind of files are present. _Data
files_, are collections of _extents_, which are collections of 8 pages. Inside a
single page there is a page header, the address of the following page, a list of
rows, along with their pointers, and some free space - available to speed up
writing. An extent can be uniform, if every page is a data page, or mixed, if
some pages contain indexes. The other kind of files are _log files_.

![sql architecture](./assets/images/sql_architecture.png)

An SQL statement is executed following these steps:

1. The query is converted into tabular data stream packets.
2. Once the packets reach the server endpoint, they are de-capsulated into SQL
   commands and passed to the query parser.
3. A query plan is generated, optimized and presented to the query executor.
4. The query executor requests data to the access methods.
5. Access methods fetch the needed data pages and pass them to the executor
6. If a non-select query has to be executed, the access methods contact
   transaction manager components - log manager and lock manager - to generate a
   log sequential number and use it to ensure transactionality. Dirty pages are
   written to ensure the capability of rolling back.
7. The tabular data stream is returned.

![sql execution](./assets/images/sql_execution.png)

The **log manager** keeps track of all updates in transaction logs. Each row of
the transaction log is signed with a log sequence number, containing a
transaction id and data modification record. This is used to keep track of
committed and rolled-back transactions. The **lock manager** is in charge of
preventing inconsistencies and granting isolation. The log manager starts
logging, and the lock manager locks the needed pages. A copy of the original
data is maintained in the buffer cache, whereas a copy of the data that is going
to be updated is stored in the data buffer - the dirty pages.

### Physical connection problem

A client may connect to an SQL server in different ways. **Direct connection**
establishes a direct channel - such as a socket. This connection requires a
handshake, an authentication and several checks. It is very time-consuming.
**Connection pooling** provides a pool of alive physical connections, available
for clients to use and handled by a _pooler_. Once the application closes the
connection, the channel is not killed, but made available to other applications.
_Pool fragmentation_ might arise when an application creates a large number of
pools that are going to be kept alive until the process exits, using a lot of
memory. None of these approaches is well suited for the usage Spark would do of
the connection.

### Index tables problem

Index tables are built as binary trees, with the same distance between each leaf
and the root. Data is stored only in leaf nodes. Data is sorted by search key in
ascending order. To perform an update, we have to perform a search to determine
what page the new record should go into. Once the page is found, if the page is
not full the record is added. Otherwise, we recursively split the page up to the
root. This cannot be parallelized. Each time a record needs to be updated, the
whole table is locked, otherwise we incur in the risk of breaking the index.

### How NOT to write

The first bad approach i to write **directly from the Spark workers** to the SQL
server. Every worker will open a connection pool, which will be kept open until
all workers have finished. A lot of dirty pages will be created and the table
will be often locked as a result of inserting on indexed tables.

![direct write](./assets/images/direct_write.png)

A second option is to write **directly from the driver** to the SQL server,
using a single connection pool and solving the physical connection problem. The
driver performs a global `.collect()` of all workers. This operation is very
time consuming and stresses the memory of the driver at the point where it could
cause _disk spilling_ or even an out of memory error .We are still creating a
lot of dirty pages and the indexed tables issue persist.

![direct write from writer](./assets/images/direct_write_from_driver.png)

We could employ a **data lake queue**. Every worker will write directly into the
data lake, which is the best way to unload the Spark process - granting better
speed and no memory issues. A `C#` agent may then use a native client to then
write into the SQL server. In this scenario, we can free the worker as soon as
they have written on the data lake, freeing the resources - and stop paying.

![data lake queue](./assets/images/data_lake_queue.png)

We are still not able to solve the problems of dirty pages and indexed tables.

### BCP and merge

Therefore, insertion into a table is tricky. It will create a lot of dirty
pages, it will require a connection pool for every worker - that will be kept
open until every worker has finished - and it will be incredibly time consuming
on indexed tables, which will be locked for a lot of time.

**BCP** - bulk copy program - is the most effective way to import large numbers
of new rows. The `MERGE` statement runs `INSERT`, `UPDATE` or `DELETE`
operations on a target table from the result of a join with a source table.
These two tools combined are the quickest way to write a large number of data in
an SQL server, including writes to indexed tables. Every worker will write
directly into a data lake queue, which decouples the workers from the target
database, solving the connection problem. Then, a `C#` agent uses a BCP program
to create a temporary table to insert into the target database. Dirty pages are
not created during this insert operation, because the table is being created
from scratch. Then a `MERGE` query is used to update the target table. This
doesn't avoid the indexed tables problem directly, but it mitigates it by
reducing the number of operations to perform on it.

![bcp merge](./assets/images/bcp_merge.png)

## Natural language processing

When dealing with a corpus of documents, we face two main kind of problems:
express them in a quantitative way, and create a metric to compute how similar
they are. The easiest way to represent a document is the **bag of words**. Each
text is represented as a vector where for each token we store how many time that
token appears in the _absolute dictionary_ of all tokens. This representation
creates a space in M dimensions, where M is the number of distinct words in the
corpora of all messages. This is obviously not ideal.

![bag of words](./assets/images/bag_of_words.png)

The only efficient way to measure the distance between such vectors is **cosine
similarity**, because it only counts contributions from non-0 elements, which
greatly decrease the amount of computation needed. The distance is computed as
the cosine of the angle between the two vectors. The closest the cosine is to 1,
the more similar the two texts are. This suffers from typical problems of
natural language processing: the difficulty in dealing with linguistic rules for
_stems_, spelling and word roots; _polysemy_, the fact that the same symbol
could bring different meanings; _synonymous_, the fact that the same concept can
be expressed with several symbols.

![cosine similarity](./assets/images/cosine_similarity.png)

![cosine similarity example](./assets/images/cosine_similarity_example.png)

## Service oriented architecture

### Definitions

#### IBM

IBM defines a service as a _discoverable resource_ that executes a _repeatable
task_, and is described by an _externalized service specification_. According to
IBM, a service is:

- _Business aligned_. It's based on IT capabilities, but on what the business
  needs.
- _Reusable_.
- _Agreed upon_ between different entities: service providers and consumers.
  These agreements are based on the specification, not the implementation.
- _Specified_. It's self-contained and described in term of interfaces,
  operations, semantics, dynamic behaviors, policies, and quality of service.
- _Hosted_ and _discoverable_.
- _Aggregated_.

As a consequence, a service oriented architecture is an architectural style that
exploits the principles of service-orientation to achieve a tighter relationship
between the business and the information systems. IBM identifies 5 layers to
this architecture:

1. _Operational systems_: the most technical layer, composed of every already
   existing IT asset.
2. _Service components_: modules and pieces of software that can be used to
   create services - e.g. the module that allows read/write from/to a data lake.
3. _Services_: services that have been implemented and deployed to the
   environment. This is were we truly decouple the IT perspective from the
   business perspective.
4. _Business process_: the abstract operational artifacts that implement the
   business process.
5. _Consumers_: users or other application.

![soa ibm](./assets/images/soa_ibm.png)

#### Open group

The open group, an industry consortium, defines a service oriented architecture
as an architectural style that supports _service-orientation_. In this
definition, service-orientation represents a way of thinking in terms of
services, service-based development and service outcomes. This completely shifts
the perspective, promoting the service to be the core concept of the
architecture, instead of objects, API, etc. The architectural style is based on
the design of services, where each service mirrors a real-world business
activity. Service representation utilizes business description to provide
context. It requires strong governance/authority of service representation and
implementation.

In this context, a _service_ is a self-contained, logical representation of a
repeatable business activity, that may be composed of other services, and is a
black-box to the consumers of the service. Each service is identified and what
it does is clearly set out in the form of a _contract_.

#### OASIS

The definition given by OASIS, another consortium, focuses on the difficulty
behind simply decomposing a complex system, and on the context within which the
system functions, which is called _ecosystem_ and it's a space where people,
processes and machines act together. More technically, an ecosystem is a network
of discrete processes and machines that, together with a community of people,
creates, uses and governs specific services, as well as external suppliers of
resources required by those services. The three key principles of this approach
are:

- The exchange of value between independently acting participants.
- The ownership of the resources that are made available within the ecosystem.
- A set of rules - policies and contracts - that dictates the behavior and
  performance of the participants.

Under this lenses, a service oriented architecture is a paradigm for organizing
and utilizing distributed capabilities that may be under control of different
ownership domains. A service is a mechanism by which needs and capabilities are
brought together, and the central focus of the architecture is to get something
done.

#### OMG

The Object Management Group consortium defines service oriented architecture as
a way to describe and understand organizations, communities and systems to
maximize agility, scale and interoperability. People, organizations and systems
provide services to each other. Services enable us to offer our capabilities to
others in exchange for some value. A service is value delivered to another
through a well-defined interface and available to a community.

OMG also proposes a layered view of the architecture:

1. Business processes, a sequence of business activities.
2. Business services, capable of automating business process activities.
3. Software components and orchestrations, that allow the business services to
   access enterprise-level resources as needed.
4. Applications, packages and databases, that might be called by the components.

![soa omg](./assets/images/soa_omg.png)

#### W3C and XML.com

A key concept of this definition is the _distributed system_: diverse, discrete
software agents that must work together to perform some tasks. The complexity
arises from unpredictable latencies, concurrency and the possibility of partial
failures. A service oriented architecture is a form of distributed system
architecture characterized by the following properties:

- it has a _logical view_, where each service is an abstraction of a concrete
  component, defined in terms of what it does, not in terms of how it is
  realized.
- it is _message oriented_ in the way inputs and outputs between different
  services are exchanged.
- it is _description oriented_. The details that are public and important for
  the use of the service are exposed in a machine-processable description.
- it has a _granularity_.
- it is _platform neutral_, by means of standards and interfaces.

According to XML.com, as a result of a service, the state of the consumer may
change, but also the state of the provider, or both.

### Modelling languages and tools

IBM defined a layered model for **service oriented modeled architecture**.

1. Operational system layer, where we describe which legacy systems - existing
   custom built applications - are already inside the organization.
2. Enterprise components layer, where we describe the quality of service of the
   exposed services.
3. Services layer, what will be actually exposed to the consumers.
4. Business process composition layer, how to orchestrate the different services
   in order to act as a single application.
5. Access or presentation layer, the interface through which interact with the
   lower layers.
6. Integration layer, that allows and supports the integration of different
   services that uses different technologies.
7. Quality of service layer, that provides the capabilities to monitor, manage
   and maintain aspects such as security, performance and availability.

![soma](./assets/images/soma.png)

**Service Oriented Architecture Modeling Language** is an extension of UML that
provides attributes and interfaces to describe the service oriented
architecture.

### Core concepts

Each **service** has a unique service name, a version, an execution type, a
logging mode, configuration and structure of incoming and outgoing messages, and
a visibility cone. Each service can be bound with one or more server objects,
depending on the application container and the integration implementation.

![service model](./assets/images/service_model.png)

A **service catalogue/repository** is a list of all available services along
with the list of incoming messages to be used to configure a service execution.

The **service queue** is the list of all service execution, of which it keeps
trace, working as a single point of monitoring. In practice, it is usually a SQL
table.

The **enterprise service bus** is the communication system that allows the
services to interact with legacy systems. It can be used to avoid the
system-to-system integration. it sees services as abstractions that can expose
simple or complex functions.

The **service broker** implements the service broker pattern, and therefore it's
in charge of submitting services to the correct application server, managing the
service queue, updating services statuses and messages. It can be seen as
somewhat of a load balancer.

![service broker](./assets/images/service_broker.png)

### Available communication models

In **synchronous service calls** the client compiles and sends a message. The
execution and business logic marshals the request, the service broker submits it
to the specific application servers which executes the logic and returns a
message. Each execution has a status - todo, wip, done, error - and a trace log.
Technical execution information is traced inside a service queue.

![synchronous service call](./assets/images/synchronous_service_call.png)

In **asynchronous service calls** the client still compiles and sends a message.
The service broker returns immediately a communication id and adds the service
to the service queue. When the time of execution comes, the service broker
submits the execution to the right application server. The client can check the
execution status at any time, using the communication id.

![asynchronous service call](./assets/images/asynchronous_service_call.png)

**Stream outbound file reference** model is a slight variation of the
asynchronous model, in which instead of returning the response inside the
outgoing message, the service broker returns immediately a reference id - to
some storage location, e.g. a data lake -, where the client knows it will find
the response. This is particularly useful for huge and hard to transfer
responses. This also allows the client to start using the result even if it is
not complete yet.

![stream outbound file
reference](./assets/images/stream_outbound_file_reference.png)

Based on the pub/sub pattern[^pubsub], the **pub/sub service callback** model
allows client to subscribe to a topic and to get a callback when new event
happens on that topic. The ESB hides the complexity of the message bus and just
provides an object id to the client each time a new event happens.

[^pubsub]:
    A pattern based on the capabilities to publish, subscribe to, store
    and process stream of events. An _event_ is a record of something happening.
    _Producers_ publish events on a message bus, whereas _consumers_ read those
    events. Producers and consumers are fully decoupled and agnostic of each
    other. A _topic_ is an index on the events that allows to organize them.
    Topics can be used recursively.

![pub sub service callback](./assets/images/pub_sub_service_callback.png)

**Batch sync data process** uses a CDC job to allow incremental batch data
extraction from a legacy system. Fresh extracted rows are saved on a data lake.
It uses a gateway service - a general purpose, highly configurable service - to
forward these incremental extractions on the target systems.

![batch sync data process](./assets/images/batch_sync_data_process.png)

### Services vs µServices

The great advantage of µServices is the possibility to confine them into a
container, in which total freedom is granted. This may become a liability when
needing to maintain the service that someone else has developed without having
to respect any constraint. Reusability and aggregation are difficult to provide
with a µService. The idea of black-box used to implement µServices makes them
difficult to reuse. µServices are usually more optimized towards a specific
need. They also don't require any abstraction, which is usually something
difficult to do for the human mind. On the other hand, having to re-implement
everything from scratch every time is very effort demanding, often not in the
implementation phase, but in the maintenance phase, which is the real burden of
µServices. Another architectural pillar that falls is centralization. It is also
very difficult to enforce governance, given the high freedom left to the
development. While with µServices the idea is to create a container that
contains all the technological stack, with a service oriented architecture the
idea is to have a service broker that is able to submit the execution to any
application server, which is prepared and configured by the architect and its
time, in a very controlled environment.

## From big data to big money

### Projects

We can distinguish three types of projects:

- _Turnkey_ project, in which the project has a clear scope, and the supplier
  will get paid by the customer when it achieves what is stated in the contract.
- _Time and material_, where a set of highly skill-intensive activities are
  delegated to an external supplier which possess the skills and the time to
  work on it.
- _Body rental_, where time is the real asset provided by the supplier, even
  with low amount of skill.

### Costs

For each project _benefits_ and _costs_ are huge factors. The are standard ways
to compute these factors - e.g. return on investment, the rate of return on
money invested in an economic entity, used to decide whether or not to undertake
an investment. Monetizing costs is easier than monetizing benefits, because the
latter depend on the fact that the benefits will then be used, which is not
something that can be anticipated with certainty. ICT is more suited to monetize
costs, based on its knowledge of how to implement something, when it will be
ready, where it should be developed and who is going to work on it. Business is
more suited to monetize benefits, because it knows what needs to be done, and
why.

To evaluate **costs** it helps distinguishing between different kind of costs. A
very easy distinction is between _CAPEX_ - capital expenditure, money spent to
buy or improve fixed assets, such as buildings, equipment and software - and
_OPEX_ - operating expense, ongoing costs for running a product, such as
maintenance and licenses fees. The distinction is important because OPEX costs
will have to be paid indefinitely, whereas CAPEX costs happen in just one
instance. In this context, costs can also be seen as _one-shot_ or _running_.
Investing in one-shot costs may lower running costs considerably, and vice
versa. The best configuration depends on the context. We can also distinguish
between _variable_ costs, that change in proportion to the production output,
and _fixed_ costs, that have to be paid independently of any specific business
activities.

![capex vs opex](./assets/images/capex_vs_opex.png)

### AAS vs bare metal

_X_ **as a service** refers to something offered hiding all the internal
complexity. This approach can be employed for _infrastructures_, _softwares_,
_platforms_. This approach requires a _cloud_ provider. The opposite of this is
the concept of **bare metal**, that involves owning and controlling everything.
A real comparison between the two in terms of CAPEX and OPEX is hard to perform,
because the hidden nature of the service makes it difficult to perceive the real
complexity. The worst scenario is a fake _X_ as a service, where infrastructure
as a service is employed in a bare metal scenario, suffering the worst of both
worlds.

![aas vs bare metal](./assets/images/aas_vs_bare_metal.png)

Things are different when developing a good capacity plan.

![aas vs bare metal planning](./assets/images/aas_vs_bare_metal_planning.png)

### Project management

The process of leading the work of a team to achieve goals and meet success
criteria at a specified time. The primary challenge is to achieve the project
goals within the given constraints.

An usual tool when managing a project is the **GANTT chart**. Activities are
listed on the rows, whereas the columns represent time-frames - days, weeks,
sprints. This chart allows us to answer three very important business questions:
how much the project will cost, when the project will be completed and the
required time size to achieve this time goal. A useful additional column is the
_effort_ required by each activity, in terms, for example, of FTEs[^fte].
Another nice addition is the _requirements_ column, in which we define the
required activities that the activity represented by that row depends on.

[^fte]: The effort provided by a full-time employed person.

![gantt chart](./assets/images/gantt_chart.png)

The GANTT chart is commonly associated to the **waterfall** model. This is a
pretty risky approach, since it doesn't allow to change requirements until the
end of the project.

![waterfall](./assets/images/waterfall.png)

An alternative approach is the **Deming cycle**, which in the context on
software development is employed in Agile methodologies.

![deming cycle](./assets/images/deming_cycle.png)

## ELK

The most important reason for searching is the need to find something among a
lot of documents. The speed and scalability of **Elasticsearch** and its ability
to index many types of content mean that it can be used for many use cases:
application search, website search, enterprise search, logging and log
analytics, infrastructure metrics and container monitoring, application
performance monitoring, geospatial data analysis and visualization, security
analytics, business analytics. Elasticsearch is a distributed, open-source,
search and analytics engine for all types of data. It's based on the object
storage model[^osm] and built on _Apache Lucene_, therefore it excels in
full-text search. It has a simple REST API and it's usually part of the ELK
stack - Elasticsearch, Logstash, Kibana.

[^osm]:
    A computer data storage architecture to manage data as objects. Each
    _object_ contains data, contextual information and technical information.
    It's based on a shared naming convention and a shared serialization
    strategy. It allows to distribute data over several nodes and it was created
    to allow retention of massive amount of unstructured data.

Elasticsearch allows to push any kind of data and make it searchable, as long as
it's allowed to build its own structure for data. Data ingestion has a big role
in this process. Raw data flaws into Elasticsearch, but needs to be parsed,
normalized and enriched before Elasticsearch can index it. The final user can
run complex queries against data, using aggregators and interacting with
objects.

![elasticsearch high level
architecture](./assets/images/elasticsearch_high_level_architecture.png)

An Elasticsearch index is a collection of documents that we would like to
aggregate together. Every document in the index is a JSON document. Each
documents has a set of keys with their corresponding values, taken from a pretty
narrow set of types: strings, numbers, booleans, null, dates, complex - lists,
objects. Documents have _mappings_, with which we can define a schema for the
document, along with some metadata. Elasticsearch tries to imply dynamically the
json field type. It also tries to cast incoherent data to the schema. The type
can be define with the _type_ key, along with an _index_, which determines the
operations that will be allowed on the field. The index field can be valorized
with: `ANALYZED`, if we want to process the value with natural language
analyzers before storing it; `NOT_ANALYZED`, if we want to store the value as it
is; `NO`, if we do not want to index this field at all - useful to control data
visibility. The _analyzer_ field controls which natural language analyzer is
used - e.g. english, japanese, custom. Nested items must be typed as well. A dot
notation is available for flattening nested items.

![elasticsearch document](./assets/images/elasticsearch_document.png)

![elasticsearch document
flattening](./assets/images/elasticsearch_document_flattening.png)

Queries are in JSON format as well. They are written in a dedicated DSL and they
can be direct or compound, in which we can combine multiple clauses into a
single query. Queries can be used for two purposes:

- _Query context_, assign to each document a score and use that score to sort
  document as output
- _Filter context_ - non-scoring queries -, completely ignore some documents
  from results, no need to compute the score, which makes these queries very
  fast.

There are different query types:

- `match_all`, returns all the results, typically used after a filter.
- `match`, query against a specific field. Uses the analyzer in case of
  `ANALYZED` fields, or looks for exact match otherwise.
- `multi_match`, applies the same query against multiple fields.
- `range`, look for values in a certain range.
- `term`/`terms`, looks for the exact value even for `ANALYZED` fields.
- `existing`/`missing`.

_Bool_ queries combine partial scores using some _verbs_: _must_, _must not_,
_should_ - which contributes to the score only in case of a match -, _filter_ -
which filters but doesn't contribute to the score. _Constant_ queries assign a
constant value to each query that match. It's quicker than bool and it's used
for filter only queries. Elasticsearch can also provide details on how the final
score was computed, distinguishing between all the different contributions.
Results can also be sorted, using sorting algorithms. Aggregation queries can
also be performed: min, max, count, average, sum.

![elasticsearch query](./assets/images/elasticsearch_query.png)

![elasticsearch query
compound](./assets/images/elasticsearch_query_compound.png)

Every time a query is executed, a relevance score is computed for each document,
to determine how much the document matches the query. The TF-iDF algorithm is
used. it is very efficient, because it exploits the properties of the logarithm
to make so very common words are almost ignored - `log(~1) = 0` -, whereas very
rare words are pushed, because they carry the true content of the document.

![td idf](./assets/images/tf_idf.png)

Elasticsearch uses a data structure called _inverted index_[^inverted_index]. To
solve typical problems of natural processing languages, it employees a synonym
dictionary at batch time, to solve the synonym problem, and it performs an
analysis - using analyzers - to tackle the stemming/lemming problem and the
spelling problem. An **analyzer** performs a tokenization into individual terms
suitable for a inverted index, followed by a normalization into standard forms.
The analysis can be performed using different strategies for these three
fundamental functions: character filters, which tidy up the string; tokenizer;
token filters, which change, remove and add words - stopwords, synonyms. For
this process to work, the analyzer must be run also on the query string.

[^inverted_index]:
    An index to search documents that builds a _bag of documents_
    by inverting rows and columns in a bag of words.

![elasticsearch analysis](./assets/images/elasticsearch_analysis.png)

The indexing is near real-time, but there is no guarantee on the indexing of the
document. If the indexing takes more than a specified amount of time, the
document is rejected. The documents are distributed across different containers,
known as _shards_ - replicas of part of the indexes.

This, along with the object storage model, grants Elasticsearch horizontal
scalability. It has a similar architecture to Hadoop's architecture, with some
differences. There is not a pre-defined master node. The master node is elected
automatically and is in charge of general operations such as creating indexes
and managing resources. Every node knows where each document lives, as if every
Hadoop nodes had a name node of its own. Everything is arranged in shards,
similar to how Hadoop data nodes work. Each shard can be primary or a replica
and is a fully fledged search engine on its own. Replicas provide reliability as
well as performance - through parallelization of query computation.

![elasticsearch nodes](./assets/images/elasticsearch_nodes.png)

## Appendix

### Apache Kafka

[Apache Kafka](https://kafka.apache.org/documentation/) is a very effective
distributed system to exchange data via a high-performance TCP protocol, based
on the pub/sub pattern.
