# Questions

Questions for the theoretical part of the exam. Questions in _italic_ are
supposed to have a broader spectrum and be the only question of the exam
session.

## Enterprise Architecture

- Describe the link between building architecture and software architecture?
  What are common concepts between these two subjects?
- Define what “Software Architecture” mean
- The Project Execution pyramid (slide 23 - Lesson 002 – Architecture 101)
- What is an abstraction? How an abstraction is created? How can we use this
  concept when we try to design an Architecture?
- _What are the main pillars of any Software Architectures? Describe each of
  them with practical examples to show the added value_
- What is a design pattern?
- ETL pattern and evolutions based on Data Lake
- _How can we implement the CDC pattern? Pro(s) and Con(s) of each approach_
- CDC, stateful/stateless and transactionality
- Differences between log and registry when we try to implement the CDC pattern
- What could be used the wrapper pattern for? Why?

## Distributed Computation

- What is Map Reduce? When was it first formalized?
- Hadoop Architecture
- HDFS Architecture
- Apache Spark main data types, laziness, and Shared Variables
- _How does Apache Spark use Hadoop elements? Apache Spark Architecture_
- Assumption(s) on a transformation distributed over Apache Spark
- _Describe a Spark Job end to end_
- What is a transaction? What is an Index? How is SQL physically organized?
- Describe what happens when you try to execute a SQL Statements
- _How to write from Apache Spark? Pro and Con(s)_
- Spark Dataframe

## Distribute Processes

- What is a SOA? How can you map SOA with Architectural Principles?
- _During the class we discussed several times about differences between
  Services and µServices: can you give an example to show differences?_
- Service Definition, Service Queue, Service Broker
- _Describe all Communication Models we have seen during the class_

## Project Management

- CAPEX vs OPEX
- As a Service pattern
- Make or Buy
- GANTT vs Agile
