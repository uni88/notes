# Artificial Intelligence for Videogames

## Table of Contents

1. Designing games AI
   1. Designing AI for a videogame
      - Goals
      - Tasks
      - Steps
   2. Evaluating characters behavior
      - Movement
      - Decision making
      - Tactics and strategies
   3. Selecting techniques
   4. AI Model for a videogame
      - Movement
      - Decision making
      - Tactics and strategies
   5. AI for different genres
      - Shooter games
        - Movement
        - Firing
        - Decision making
        - Perception
        - Pathfinding
        - Related genres
      - Driving games
        - Racing lines
        - AI drives the car
        - Urban driving games
        - Related genres
      - Real time strategy games
        - Pathfinding
        - Group movement
        - Influence mapping
        - Decision making
      - Turn based strategy games
        - Assistive AI
      - Sport games
        - Playbooks
        - Physic prediction
2. What is a game engine
   1. History
   2. The game engine
      - Black box, core, and evolution rules
      - Tool set
      - Runtime
   3. Assets and rules
      - Built-in vs user-provided
      - Scripts
   4. Runtime Architecture
      - Hardware
      - Device drivers
      - OS
      - Third parties's SDKs and middleware
      - Platform independence
      - Resource manager
      - Virtual execution environment
      - Game-specific subsystem
   5. Graphic assets
      - Editing and importing
      - Rendering
3. AI in the game engine
   1. Introduction
      - Game AI
      - Techniques
      - Inclusion
   2. AI engine architecture
      - Core technologies
      - Character control systems
      - Goals settings and decision making
   3. Performances
      - Resource management
      - Believability vs accuracy
      - Game loop
4. Pathfinding basics
   1. Introduction
      - Planning
      - Representation
      - Best way
   2. Graphs
      - Kinds of graphs
      - Environment map
      - Constraints
   3. Dijkstra
      - Steps
      - Millington's version
   4. Uneven terrain and slopes

## Designing Games AI

### Designing AI for a Videogame

Designing AI for videogames is about _believability_, not optimization. We have
to convince the players that they are dealing with a real intelligence, without
actually creating one.

The **goals** of AI in videogames are mainly two: assisting the game play, and
enhancing the immersion. There are many different **types** and **uses** of AI.
In general, AI can be a great supporting tool for game design, by means of
genetic algorithms, procedural content generation, neural networks, machine
learning, etc...

When designing agents/AI, the game designer defines:

- The psychology of the creatures
- Where to place them
- How to make their behavior readable
- How to avoid predictability

Ideally, this would be done following these **steps**:

1. Work out the desired behaviors, starting from the game design document
2. Select the simplest set of techniques that supports these behaviors
   1. Plan how to integrate those techniques
   2. Plan how to integrate the AI and the game engine
   3. Put placeholders for character behaviors
3. Start to work

Unfortunately, many things step in the way of this process, like publisher
milestones or quick and dirty code that ends up making it in the final version.

#### Evaluating Characters Behavior

Evaluating a characters behavior starts from the description made by the game
designer. Of course, behaviors are not set in stone, and they typically evolve
during the game development. Given this, it's important to develop with
_flexibility_ in mind, and optimize code only at the end.

While evaluating characters's behaviors, there are 3 main groups of questions to
answer:

1. **Movement**
   - Do the characters move individually or in groups?
   - What is the degree of realism?
   - Is any physical simulation needed? If so, how realistic should it be?
   - Is any pathfinding needed?
   - Is characters's motion influenced by other characters?
2. **Decision making**
   - What is the full range of different possible actions?
   - How many distinct states does each character have? How will the actions be
     grouped accordingly?
   - When and why will the characters change their behavior?
   - Do characters need to look ahead to make their decisions?
   - Are characters's decisions influenced by the players actions?
3. **Tactics and strategies**
   - Do characters need to understand the game on a larger scale?
   - Do characters work together?
   - Do characters both think for themselves and still display a certain pre
     defined group behavior?

#### Selecting Techniques

Once the behaviors are set, some candidate techniques must be selected to
implement them. A critical point in this process is which decision making system
is adopted. A good approach is to try the simplest approaches first (behavior
trees, state machines,...) and use exotic, complex techniques only if really
necessary.

#### AI Model for Videogames

![AI model for videogames](./assets/images/ai_model_for_video_games.png)

**Strategy** coordinates a whole team. Each character has its own decision and
movement algorithms, but their decision making will be influenced and guided by
a group strategy.

**Decision making** allows a character to work out what to so next. Each
character has a range of possible behaviors between which to choose. Decision
making algorithms may be very simple, or very complex - possibly including a
chain of intermediate actions with a specific goal. Once the behavior has been
chosen, it can be executed by animation and movement techniques. The decision
may also have no visual feedback.

**Movement** algorithms turn decisions into some kind of motion.

Of course, to build AI for a game, algorithms alone are not enough. Movement
requires animation and physics simulation. AI needs information about the game
world, obtained through perception. AI must also be managed to optimize CPU and
memory.

### AI for Different Genres

#### Shooter games

![AI architecture for
shooters](./assets/images/ai_architecture_for_shooters.png)

- to control enemies's movement
- to make enemies fire accurately
- to perceive who to shoot and where they are
- to plan NPCs routes
- to make NPCs find safe positions

**Movement** is the most visible part of the characters's behavior. Shooters
have the most complex sets of animations. Making characters move around the
level is a challenge. Characters have to find a way in constrained settings
while reacting to other characters/players move. AI needs to work out the route
and break the necessary movement into animations. To do so, the main approaches
are:

1. AI splits the tasks in two parts: the pathfinding and the animation
   management.
2. To control the NPCs, the game uses the same controls as the players. AI needs
   to specify only direction, velocity, weapon changes,...

When it comes to **firing**, it's important to guarantee a good trade-off
between _accuracy_ and _fun_.

**Decision making** is usually carried out using _finite state machines_ or
_behavior trees_. The main difference is that in behavior trees any decision can
be reached at any moment, while in fsm only the actions connected to the current
state can be reached. A common approach is to use scripts that poll the current
game state and ask actions to be executed. A nice side effect of decision making
may be _emergent behavior_. Adding a bit of randomness may add _credibility_ to
the AI.

![FSM example](./assets/images/fsm_example.png)

**Perception** is important to provide the AI with the input on which to make
decisions. Perception may be faked by putting a _radius_ around the characters,
inside which they are aware of the player. More sophisticated approaches may
require an actual _sense management system_. Cone of sights and sound models
might be also part of such a system.

Developers use a wide range o representations to tackle the problem of
**pathfinding**:

- **Navigation mesh**es are graphs based on polygons. Each polygon is a node
  that may (or may not) be interconnected to its neighboring ones. Usually,
  these polygons are triangles or quadrilateral, with 3/4 interconnections at
  most.
- **Embedded navigation** links the pathfinding to the type of action needed to
  traverse the interconnections between polygons. This gives the illusion of the
  agents reacting to the type of terrain they are traversing.
- **Waypoints**

Other genres have similar characteristics to shooters. **Platform** games are
focused on making enemies interesting but fairly predictable. Movement has to
deal with flying enemies. **Adventure** games try to turn enemies into puzzles
to solve. Pathfinding is usually not necessary and decision making is also bound
to an unaware/aware of the player status. In **MMO**s and **MOBA**s there is
marginal need for AI, since the challenge is represented by other players. AI is
still an aspect to be tackled, since the environment is very large and the
agents are numerous. Pathfinding and perception scalability issues are something
to be aware of.

#### Driving games

Ai for driving games is one of the most specialized, genre-specific AI. The
crucial tasks are focused on movement. The player will judge the AI on the basis
of how well it drives the car. Generally, realistic behaviors and clever
tactical reasoning is not necessary.

![AI architecture for driving
games](./assets/images/ai_architecture_for_race_driving_games.png)

An old approach for **racing** AI is to use **racing lines**. The level designer
defines the _spline_[^spline] that the vehicle should follow and additional
data, if needed - speed, steering to get back to the line.

[^spline]:
    A spline is a mathematical function composed of multiple pieces, each
    one defined by a _polynomial function_. The different polynomial functions
    connect in points called _knots_, which possess a high degree of smoothness.

A more recent approach is to have the **AI drive the car**. The movement can be
more or less realistic, according to the player's expectations. It's critical
that physic's laws are the same for the AI and the player. This may also be
coupled with splines. In this case, AI tries to follow the line, but is not on
rail.

In **urban** driving games the big difference is there are no predefined, fixed
racetracks. Depending on the situation, AI should follow paths for escaping, or
perform homing-in algorithms for catching the player. Some pathfinding and
tactical analysis are involved in both those tasks. Usually, cars are created
only in the surrounding block, where the player is. The same approaches used for
driving games can be applied to **extreme sports** games and **futuristic
racers**.

#### Real Time Strategy games

![AI architecture for real time strategy
games](./assets/images/ai_architecture_for_real_time_strategy_games.png)

Early strategy games were based on huge tiled grids. Because of this, the focus
was on efficient and quick **pathfinding**. More recent strategy games use
arrays of heights - _heightfield_[^heightfield] - to render the landscape. Those
are also used for pathfinding. Pathfinding difficulty increases in case of
deformable terrain.

[^heightfield]:
    Also called _heightmap_. It's a raster image, used to store
    values about surface elevation data, bump mapping, and displacement mapping.
    Widely used in terrain rendering softwares and videogames, it's the ideal
    way to store digital terrain elevations, since it requires substantially
    less memory than a regular polygon mesh.

Strategy games also have to tackle the hard task of **group movement**. In older
Strategy games this was done by grouping the units into _formations_ - with size
limit - and moving them as a whole. In later times size limit was eventually
removed. This posed the issue of scalability, which was tackled by using
different slots, according to the number of units. In recent Strategy games the
formation depends also on the features of the level. It may also be that the
player has only indirect control on the group, with units moving independently
and formation pattern decided by AI.

Strategy games introduced the concept of **influence mapping**. This is about
using the output of tactic/strategic AI for: guiding pathfinding, i.e. taking
into account the type of terrain; selecting locations for constructions, i.e. to
put walls between valuable areas and enemies; maneuvering large scale troops.

**Decision making** has to be tackled at different levels: an individual unit, a
single group, a whole side. For each of this level, an AI module is created. The
modules can be dependent on each other or independent from each other. The main
technologies adopted in this context are: FMSs, decision trees, probabilistic
methods and sets of rules.

#### Turn Based Strategy games

![AI architecture for turn based strategy
games](./assets/images/ai_architecture_for_turn_based_strategy_games.png)

For older games - replicas or small variants of existing board games,
_Minimax_[^minimax] techniques were often enough. In more recent games, the
number of possible moves is almost unlimited. This makes AI needs very similar
to the ones of Real Time Strategy games. The advantage, in this case, is that
the movement algorithms can be very simple. AI has to compete with a higher
level of human thinking. The game/level designer must try to simplify AI job as
much as possible, by creating levels that are easy to tactically analyze,
research trees that are easy to search. The full range of decisions to take is a
big concern.

The game may help the player automate boring tasks, by assigning them to an
**assistive AI**. This type of AI should have decision tools which have no needs
for strategic input from higher levels. It should also understand the player
strategy.

[^minimax]:
    a decision rule for minimizing the possible loss for a worst case
    scenario. When dealing with gains, it is referred to as _maximin_ - to
    maximize the minimum gain.

#### Sport games

![AI architecture for sport
games](./assets/images/ai_architecture_for_sport_games.png)

The range of different sports to implement is just as huge as the body of
knowledge for good, ready to use strategies. Unfortunately these strategies
aren't always easy to encode. The player expects human-like competencies and
appropriate team coordination. As seen previously for Strategy games, AI for
sport games is multi-leveled. The highest level is represented by strategic
decision, followed by coordinated motion systems - play patterns - and by AI for
the single unit. To do this, AI may use _Playbooks_: sets of movement patters
used by the whole team, or a smaller group of players, in specific
circumstances.

**Physic prediction** is a crucial component of Sport games AI. The ball - or
any other involved props - movement must be predicted in order for the AI to
make a decision. For simpler physical dynamics - soccer, baseball - predicting
the trajectory could be enough. For more complex dynamics - pool, golf - the
exact result must be predicted.

## What is a Game Engine?

### History

Game engines date back to 1994, with the development of _DOOM_. The idea was to
strictly separate the core software components and data assets, and to enforce
code reusability during development. From then on, more games have been designed
with a modular architecture, allowing modding and focusing once again on code
reuse. Scripting languages - E.g. _quake C_ - started to become part of the
distribution, and the game engine became a standalone product. The customers of
the game engine are not the players, but the developers.

### The Game Engine

From a technical point of view, a game is _a real-time interactive agent-based
computer simulation_. **Real-time interaction** consists into responding to
player input in a timely-bound manner. **Agents** are independent entities that
live and interact with each other within the engine. As a **simulation**, the
game is capable to describe a model representation of a virtual world, through a
_mathematical_ description.

![game in unity](./assets/images/game_in_unity.png)

A game engine is a software made to implement such a system. Its **core** runs
the basic simulation and manages and coordinates the resources. It doesn't know
how to make the system evolve; it just applies _evolution rules_ provided with
the game. Therefore, it must be able to apply any kind of rule.

Core and basic evolution rules - E.g. physics and lightning) are bundled and
hidden from the user in a **black box**. To create a game, there is no need to
know how this basic rules work, just how to use them, typically by means of an
easy to use GUI. The GUI is usually the front-end to the tool set.

The **tool set** allows for different things, such as: compile software to work
within the game engine, help describe rules, manage assets, create content, etc.
The **runtime**, on the other hand, takes care of running the rules on the
assets and must be distributed with the game. It usually consist of a library, a
middleware, a sandbox or a virtual machine. Through the GUI, we use the tool set
to put assets inside the runtime and then ask it to create an _executable file_.
A large piece of the core will be inside each executable; therefore, we need
re-distributable licenses.

A game engine can be seen as a container for rules, so that the game developer
only needs to explain _how_ the world should evolve, and not create the code to
make it evolve. The black box will then apply the rules on the assets, changing
the box status and its assets, making the world actually evolve.

![black box and gui](./assets/images/black_box_and_gui.png)

### Assets and Rules

An **asset** is whatever can be seen, listened to or felt while playing - E.g.
textures, 3D meshes, material definitions, particles, visual effects, music.

A **rule** is the definition of a behavior, attached to an asset in order to
define how to interact with the user, the environment and other assets. Creating
a believable _NPC_ means creating the right rules based on the surrounding
context. Rules can be _built-in_ the black box, for everyday use, or they can be
provided by the _user_, making th game truly unique. There is no actual
difference between the two.

The easiest way to describe a rule is by means of a **script**. Technically, a
script is an asset. It turns into a rule when it's compiled and run inside
another asset.

### Runtime Architecture

The game engine has a huge and complex architecture that is made of layers. From
bottom to top:

1. The lowest layer is the _hardware_.
2. _Device drivers_ provide uniform and manageable interfaces to device
   controllers.
3. The _operating system_ - os - allows the application to use the hardware via
   the device drivers.
4. The os also hosts _third parties's SDKs and middleware_ to use the os in a
   way that is different and convenient for the game. This includes implementing
   data structures and logics that are extremely optimized for the current
   platform. From this layer on, a subset of everything that will be defined
   will be bundled with the game executable. This specific layer will be
   different depending on the system for which the build is targeted.
5. Since the middleware are architecture-specific and we don't want to ask the
   developer to create architecture-specific code, we need a dedicated layer to
   provide _platform independence_ in accessing the game engine inner
   functionalities. This replicates the data structures and logics by means of a
   standard API. This is where built-in rules lie, along with some executors for
   user-defined rules.
6. Then, a platform independent _core system_ is built. We could achieve better
   performances by moving this to a lower layer, but we would have to sacrifice
   platform independence, introducing more efforts to maintain it.
7. A _resource manager_ is in charge to coordinate resources and feed them to
   the core, also guaranteeing consistency for their data structures.
8. Every layer listed until now provides a _virtual execution environment_ where
   the game will run and evolve. Inside this VM, all the tools used to manage
   the game engine are implemented. This is the tool set of the game engine. It
   may be seen as the lid of the black box.
9. Using the tool set, we implement the _game-specific subsystem_ from outside
   the black box. This is where user-provided rules are defined. This layer is
   separated from the lower ones by the _editor_.

![runtime architecture](./assets/images/runtime_architecture.png)

The game rules defined through the editor are compiled using the tool set and
stored in platform-dependent data structure. These data structures are fed to
the executor, to run the game rules together with built-in rules. The result of
the execution is propagated to the tool set - for output and communication - and
to the core system - to update the game configuration.

![runtime execution](./assets/images/runtime_execution.png)

### Graphics Assets

We use editors to create graphic content in industry-standard formats. These
editors are completely unaware of the fact that we will be using its exports
inside a game engine. On importing those contents, the resource manager will
convert them - using the tool set - into a platform-independent format. Then,
this platform-independent format is converted into an engine-specific format, to
boost storage and usage performance.

When running the game, graphic will be rendered using a library installed in the
system. Therefore, we myst convert everything in a format that can be used by
said library. Unfortunately, this library is usually unknown at build time, so
we must perform the conversion on the fly during the execution. This is one of
the reasons behind loading times. The library will then take care of converting
everything in a format that is understandable for the os and the hardware, but
this is beyond the game engine control.

## AI in the Game Engine

A collection of tools - a _module_ - to help produce the illusion of intelligent
behavior is located inside the AI dedicate section of the black box. The term
**game AI** is often used to refer to a broad set of algorithms, which includes
techniques from control theory, robotics, computer graphics and computer science
in general. As already seen, a generic engine architecture for AI might look
like this:

![AI model for videogames](./assets/images/ai_model_for_video_games.png)

At the core, there are basic **techniques** for _planning_ and _decision
making_, like: _pathfinding_, _A\*_, _decision_/_behavior trees_, _finite state
machines_. There are also techniques on _perception_: _line of sight_, _cone of
vision_ and _knowledge of the environment_. Possibly, also some form of
_memory_.

At the beginning, AI was not considered as a rightful part of the game engine.
Only recently, companies understood that there are AI patterns that come up in
many games. Therefore, these core technologies are slowly being integrated in
game engines - this is true for Unity and Unreal.

### AI Engine Architecture

Again, a layered structure is employed. Layer distinction is blurry, and we
might end up mixing some layers, or even completely skip others.

1. At the lowest level, we find the _core technologies_, basic functionalities
   provided by the engine core.
2. On top of the core technologies, we can build _character control systems_,
   including locomotion, navigation, driving and usage of weapons, along with
   everything else needed to define rules in our AI. These can be seen as
   general services that might be useful for more than one game.
3. The next layer takes care of _goals settings and decisions making_. These
   includes handling emotional states, group behaviors - coordination,
   cooperation, flocking - and expert systems. These can be game-specific.

### Performances

It's very important to use computational resources in an economic way. CPU,
memory and energy are budgeted - with a very small budget, if the machine is
portable - and we must update the whole world in each frame. Therefore, complex
or slow algorithms should not be employed, and cutting edge technologies should
be adopted, when possible. Cloud, GPUs and parallel CPUs are excellent ways to
decentralize data processing, and new technologies are continuously being
developed to improve the situation. Nonetheless, low-end machines will always
ask for compromises.

It is important to notice that the goal is not accuracy or optimality. The goal
is to have a believable an fair behavior, so workarounds and cheats aimed at
improving the performances are acceptable, unlike when dealing with traditional
AI.

The situation is made a bit better by the fact that AI doesn't have to be
synchronized with the rendering loop. Graphics needs to be recomputed 30/60
times per second - or more -, whereas AI might need to be serviced by the _game
loop_[^game_loop] just once or twice per second.

[^game_loop]:
    The game loop is the scheduler in charge to update the system
    status inside the game engine.

## Pathfinding Basics

Pathfinding is a special case of _planning_ aimed at making NPCs stroll around.
It has to be believable, but it doesn't need to be perfect. The two fundamental
steps are: making a **representation** of the space, and run an algorithm to
understand the **best way** to go from point A to point B. Representation is
done by means of a _graph_. A definition of "best" is also needed.

### Graphs

Many different kinds of graphs are employed in videogames:

- _simple_/_undirected_, in open worlds or labyrinths.
- _directed_, when driving cars in a city.
- _oriented_, in rail shooters or infinite runners.
- _mixed_, when moving freely in a level, with a one-way connection to te next
  one.
- _multi-graph_, when many ways to cover a path are available.
- _weighted_, when not all connections have the same importance/cost.

The starting point is the **environment map**, with the goal of understanding
where an NPC might be interested to move. We recognize areas and passages
between them. Areas can be associated to _nodes_, and passages to _edges_. A
_weight_ can then be associated to each edge, to specify the cost to move from
one node to another using that edge. The AI only needs the graph that we created
in this way.

![map for a path](./assets/images/map_for_a_path.png)

![graph for a path](./assets/images/graph_for_a_path.png)

Our implementation won't allow negative weights. This is because algorithms that
take those into account work poorly and perform well only with complicated
implementations. We also won't allow undirected edges, since we do have one-way
edges, occasionally. What we are left with are **positive-weighted oriented
graphs**, which can behave like undirected graphs by using two one-way edges. If
we have no use for wights, we can just sey each weight to 0.

#### Unity Implementation

```C#:./assets/code/pathfinding/graph/Node.cs

```

All we care about a node, at the moment, is to give it a name, in order to let
te user tell it apart from the others.

```C#:./assets/code/pathfinding/graph/Edge.cs

```

```C#:./assets/code/pathfinding/graph/Graph.cs

```

A graph is a collection - a dictionary - of nodes, linking to each node the list
of outgoing edges. Adding an edge means adding the linked nodes and then
assigning the link inside the dictionary. Adding a node will just create a new
entry in the dictionary. Tho explore the graph, it is useful to get all outgoing
edges from a given node.

### Dijkstra

Dijkstra is a shortest-path algorithm for graphs, but if we associate a map to
the graph, it can act as a pathfinding algorithm. To be precise, Dijkstra's
algorithm calculates shortest-paths from a starting point to anywhere in the
graph; this means that we have a super-set of the result we were looking for.
Hence, resources will be wasted and the algorithm itself will be inefficient.

As input, the algorithm takes a graph, a starting node and a goal node. The
output is a list of edges to go from start to goal with minimal weight. The
result may not be unique. The **steps** are the following:

1. Define two sets of nodes: _visited_ and _unvisited_.
   - Set the unvisited set as the complete list of nodes and the visited set as
     empty.
2. For every node, define a _tentative distance_ from the start, and a
   predecessor node.
   - Set 0 for the starting node, and infinite for all other nodes.
3. Select a _current_ visited node as the one in the unvisited set with the
   smallest tentative distance.
4. _Process_ the current node:
   - Assign a tentative distance and a predecessor to all its neighbors, only
     where the new tentative weight is lower than the current.
   - The neighbors's value will be the sum of the local tentative distance and
     the weight of the edge leading to that neighbor.
5. Mark the current node as _visited_.
6. If the unvisited list is _not empty_, repeat from step 3.
7. Return the minimal path by backtracking from the goal node.

#### Millington's Version

To avoid dealing with infinite as a value - for performance reasons -, instead
of using visited and unvisited sets, the author defines tree sets:

1. _Unvisited_ nodes: unvisited nodes with infinite tentative distance.
2. _Open_ nodes: unvisited nodes with a non infinite distance - the _border_ of
   the visited nodes set.
3. _Closed_ nodes: visited nodes.

#### Unity Implementation

```C#:./assets/code/pathfinding/dijkstra/DijkstraSolver.cs

```

It is technically redundant to keep track of the distance in the node extension,
since we could re-compute te path each time. This would be better space-wise,
but also a lot worse performance-wise. This code also contains a potential bug.
It assumes that there will never be a path that is longer than `float.MaxValue`.
The output list of edges is returned by traversing the predecessors, starting
from the goal. This list is then reversed and converted into an array.

### Uneven Terrain and Slopes

Not all battlefields are flat, of course. This complicates pathfinding - E.g. An
NPC has to choose between traversing a mountain by climbing it, or going around
it. To achieve this, we must consider the geometrical features of our objects.
This might be done by tweaking the weights of te edges - E.g. the distance is 3
when climbing up, 1 otherwise, so that the agent climbs up only if it's actually
a shortcut.
