# New Generation Data Models and DBMSs

NoSQL systems try to overcome the limits of relational systems:

1. Relational models don't allow to introduce data that doesn't adhere to the
   schema. This is a problem when collecting data without knowing its structure
   yet, or data produced by different sources.
2. When data is stored in clusters of machines, it's difficult to ensure
   consistency.
3. Queries rely on the `JOIN` operation, which is too costly when data size
   increases, especially for clusters of machines. To avoid using the `JOIN`
   operation, data is not normalized anymore, and data aggregation is used a
   lot, thus causing data duplication, the price to pay to have efficiency.

## Table of contents

- [New Generation Data Models and DBMSs](#new-generation-data-models-and-dbmss)
  - [Table of contents](#table-of-contents)
  - [Introduction to NoSQL systems](#introduction-to-nosql-systems)
    - [Limitations of the relational model](#limitations-of-the-relational-model)
    - [The birth of NoSQL systems](#the-birth-of-nosql-systems)
    - [Data partitioning and sharding](#data-partitioning-and-sharding)
      - [Range based](#range-based)
      - [Simple hashing](#simple-hashing)
      - [Consistent hashing](#consistent-hashing)
      - [Other key-oriented schemes](#other-key-oriented-schemes)
    - [Data replication](#data-replication)
      - [Master/slave](#masterslave)
      - [P2P](#p2p)
    - [The CAP theorem](#the-cap-theorem)
    - [Consistency](#consistency)
    - [Storage layout](#storage-layout)
      - [Strategies](#strategies)
      - [LSM](#lsm)
    - [Use cases](#use-cases)
    - [Data models](#data-models)
      - [Key-value](#key-value)
      - [Column family](#column-family)
      - [Document based](#document-based)
      - [Graph data](#graph-data)
    - [MapReduce](#mapreduce)
  - [Database design](#database-design)
    - [UML class diagram notation](#uml-class-diagram-notation)
  - [MongoDB](#mongodb)
    - [Data model](#data-model)
    - [Architecture](#architecture)
    - [Designing with MongoDB](#designing-with-mongodb)
      - [JSON schema](#json-schema)
      - [Design a JSON schema](#design-a-json-schema)
      - [From UML to JSON schema](#from-uml-to-json-schema)
    - [Design patterns](#design-patterns)
      - [Attribute pattern](#attribute-pattern)
      - [Extended reference pattern](#extended-reference-pattern)
      - [Subset pattern](#subset-pattern)
      - [Computed pattern](#computed-pattern)
      - [Schema versioning pattern](#schema-versioning-pattern)
  - [Graph databases and Neo4j](#graph-databases-and-neo4j)
    - [Graphs](#graphs)
    - [Graph databases](#graph-databases)
    - [Designing a graph database](#designing-a-graph-database)
    - [Neo4j](#neo4j)
    - [Cypher](#cypher)
    - [PGQL](#pgql)
  - [Cassandra](#cassandra)
    - [Architecture](#architecture-1)
      - [Clusters](#clusters)
      - [Data centers](#data-centers)
      - [Partitioning](#partitioning)
      - [Consistency](#consistency-1)
      - [Time to live](#time-to-live)
    - [Data model](#data-model-1)
    - [CQL](#cql)

## Introduction to NoSQL systems

In the past few years we have witnessed an explosion in the quantity of data
that humans produce. The need to efficiently maintain, store and retrieve
information arose. Data is also more and more interconnected - directly or
indirectly. Machines are also more and more organized in a P2P system. This
allows to obtain high computing power for a low amount of money. This is also
more flexible than having a unique machine. The requirements have also strongly
changed over time. The number of users of web applications has increased
enormously. Many of these web applications have to deal with concurrent access.
Data is also much more diverse and doesn't always conform to the same schema.
All of this often needs to happen in cloud.

A big reason why relational models are not best suited for modern applications
that handle complex data is performance. In the relational model, maintaining
relationships between data is very costly - it requires the `JOIN` operation,
which is the most costly operation, feasible for small amounts of data,
unfeasible for big data.

### Limitations of the relational model

- Tabular and rigid representation of data
- Data is spread
- Non application specific
- SQL is not complete
- Not scalable
  - Solutions

The relational model relies on a tabular representation of data, whereas there
are a lot of types of data which are not suited for a tabular representation -
i.e. a web page, how do you represent the content? This schema is also very
rigid. What if the schema of the data changes over time? What if we have to
merge two datasets that had different schemas? This operations would create a
lot of null data.

In the relational model, to maintain normalized data, information is spread
across multiple tables. This can make retrieving complex/aggregated data very
tricky. I might have to query different tables. What if the three tables are on
three different machines?

The relational model doesn't care about the type of application it's supporting,
and its goals. What we should do, tho, is focus the structure of data in a way
that is more efficient for the application that will use it.

SQL is not a complete language. There are some queries which cannot be
implemented in SQL - i.e. recursion -, or at least their implementation is not
easy. Dealing with interconnected data asks for operations that are not
implemented in SQL - i.e. shortest path.

The relational model is scalable only vertically, but not horizontally. This
means that it's scalable by making the machine more powerful, but it's not
scalable by adding new machines to the cluster. The issues with horizontal
scalability are due to the management of constraints, the satisfaction of ACID
properties, the new 1:1 ratio of read:write operations, the replication of data.
This can be tackled by using a master/slave operation. Write operations are only
executed on the master, whereas read operations are only performed from the
slaves. This can cause inconsistencies in reading operations, in case writes
didn't have time to propagate yet. Also, the master could be a bottleneck for
the system. Another approach is sharding. Data is physically separated in
different servers. Each server is responsible for reading/writing its own data.
This scales better performance-wise, but doesn't support relationships across
partitions.

### The birth of NoSQL systems

- Data modeling strategies
  - Aggregate organization
  - Detail organization
- Querying languages
- Distribution
  - Filesystem strategies

At the beginning of 2000, many companies tried to overcome these limitations by
developing new models that were suited to their needs. These systems were
designed to work with a huge amount of data, they used simple APIs to access
data, and they were able to easily scale.

Initially, these systems were not-relational - they didn't even have a schema,
in some cases, and didn't use SQL -, distributed and horizontally scalable -
they used a cluster of message-exchanging machines -, open-source, not fully
supporting consistency and ACID properties.

Even tho it's not required to define a schema in advance, it's still a good
practice to try and design a schema for data. This could improve performance.
Data can be organized using two families of strategies:

- Aggregate organization: different pieces of information are put together in
  order to reduce the number of data accesses. This works well with key-value or
  document-based data models. The advantage is faster retrieval of data. The
  drawback is data duplication and the lack of flexibility in changing access
  patterns.
- Detail organization: information is broken into smaller units. This works well
  with graph data model. The advantage is that data is not duplicated, and
  relationships can be easily added or removed. The drawback is that
  partitioning is not easy to achieve. Also, some type of data do not have
  connections.

NoSQL systems usually don't support SQL. They don't provide such an expressive
language, but rather a set of APIs, or a declarative language which reminds of
SQL, but still doesn't have the same expressive power. This is mainly because
the `JOIN` operation cannot be supported.

The distribution is achieved by separating servers in two categories:

- frontend servers, which are used for collecting application requests and
  sending them to the clusters of nodes.
- backend servers, which are the above mentioned data storage nodes and can be
  organized according to different models.

Distributed systems are either shared-nothing or shared-disk. Shared-disk
systems may have different nodes that access the same file system, whereas in
shared-nothing systems each node is responsible for a single portion of the
data.

### Data partitioning and sharding

- Definition and goals
- Partitioning strategies
- Partitioning graphs
- Static vs workload aware partitioning

We are in the context of shared-nothing architectures, to achieve horizontal
scalability. Data has to be partitioned, for efficiency reasons, and replicated
on different nodes, to ensure fault tolerance. The workload must consist of
simple operations that can be distributed on the nodes. The nodes can be added
or removed without blocking the processes.

Data partitioning is the process of splitting data into disjointed partitions
and spread them over the cluster nodes. The process should contact the least
amount possible of nodes. Another goal is to reduce the amount of data
transferred over the network, when querying the cluster. Third, the performance
of the system should be increased by processing data in parallel. The process
should also easily adapt to changes in the cluster - i.e. adding/removing a
node, malfunctioning of an existing node.

Different approaches can be divided into vertical partitioning - data is
partitioned by "columns" -, and horizontal partitioning - data is partitioned by
"rows".

Each partition has an identifier, called shard key. This key is used in lookup
operations inside the cluster. This is called key-oriented sharding of data.

When working with graphs, partitioning means identifying the set of nodes and
relationship that should be in the same partition - transversal-oriented
sharding. The problem is how to decide the best way to partition. We could end
up in a situation where a node is much busier then the others - hot-spot node -.
We can try to evenly distribute objects among nodes, but we may often need to
access multiple nodes to explore the graph to answer a query. We can try to
minimize the number of edges that start in one node and end in another one. The
optimal partitioning of the graph is known to be an NP-hard problem - there is
no optimal solution that fits every case. Currently, very few - academic -
systems are able to optimize the partitioning of a graph into a cluster.

![transversal oriented](./assets/images/transversal_oriented.png)

Range based sharding and hash based sharding are part of the key-oriented static
sharding, which only considers static information about the network.
Key-oriented workload-aware sharding uses techniques from the static approach,
but also uses information about the current workload of the network.
Specifically, it considers network hot-spots: nodes that are very busy at a
given moment in time. The goal is to identify them and re-distribute the
workload, in order to avoid ping-pong effects and continuous relocation of data
across the nodes.

#### Range based

- Definition
- Pros and cons

In range-based approaches, data items are ranked using the shard key. Then,
contiguous piece of data are assigned to the same cluster. Queries on contiguous
data are very efficient. On the other hand, nodes with popular keys have a
higher workload. Also, nonuniform key distribution may lead to unbalanced data
distribution. Lastly, to use this structure we have to maintain a lookup table
somewhere in the network. Application that need to access information relying on
specific keys or lists of ordered keys can benefit a lot from this structure -
i.e. data warehouses, online games. If a node is added or removed, the range of
shard keys and the data must be redistributed evenly and ordered.

![range based](./assets/images/range_based.png)

#### Simple hashing

- Definition
- Pros and cons

In simple hashing approaches, we can get rid of the lookup table by using a
simple hashing function - i.e. modulo hashing - which allow us to get the
hosting node starting from the key. Data lookup is very quick. With this
approach we also need to redistribute the data when nodes are added/removed. We
also cannot group related data on the same node, as the node is chosen only
depending on the key - the hashing function is not aware of the semantic of the
data.

![simple hashing](./assets/images/simple_hashing.png)

#### Consistent hashing

- Definition
- Node addition/removal
- Pros and cons

In consistent hashing, the same hashing function is applied to both the data and
the nodes. The nodes are allocated in different points on a virtual circle,
based on the result of the hashing function. Each node is responsible for every
shard key which is contained in the section of the circle between it and the
previous shard.

When a node crushes, its range of key shard becomes responsibility of the next
node on the circle - the data which was contained in the crushed node is still
lost. Because of this mechanism being in place, data is replicated in the nodes
that follow, so that requests can be still satisfied if the original node
crushes.

![consistent hashing](./assets/images/consistent_hashing.png)

When a new node is introduced in the cluster, it has to get in touch with the
nodes that will replicate its data, and with the nodes which data will be
replicated by it. The triggered changes will be asynchronously propagated - same
goes for node removal.

![consistent hashing new node](./assets/images/consistent_hashing_new_node.png)

The big advantage with respect to previous methods is that not all nodes are
involved in the changes triggered by the addition/removal of a node. On the
other hand, there is still poor data locality, the scheme doesn't take into
account the resources made available by each node, and when nodes join or leave
the network, data redistribution may overwhelm the load capacity of its
successor.

#### Other key-oriented schemes

- Virtual nodes
- Hybrid approaches
- DHT

Some approaches use the concept of virtual nodes, instead of partitioning the
key-range depending only on the position of the nodes. Each node may have a
different number of virtual nodes, depending on its resources.

This doesn't solve the issue of poor data locality. Combining consistent hashing
and range-based strategy leads to better results. For example, consistent
hashing may be used for inter-cluster partitioning, and range-based can be used
for intra-cluster partitioning.

Distributed hash tables - DHT - extend consistent hashing to obtain a totally
decentralized approach - consistent hashing still asks for a kind of lookup
table to keep track of which node is in charge of which range of keys.

### Data replication

The same partition can be mirrored across several nodes of the cluster. The
advantage is that we obtain an increased throughput, fault tolerance and high
availability. An important variable is the replication factor: the number of
times the same partition is replicated. This also determines the fault tolerance
of the system. A replication factor of 3 is usually considered optimal.
Replication is usually tackled by either using a master-slave approach or a P2P
approach.

#### Master/slave

- Definition
- Pros and cons

In the master-slave approach, a single master node coordinates one or more
slaves. Update operations are always taken care of by the master. The slaves
only answer reading requests. Each update to the master has to be propagated to
the slaves. In the pull mode, the slaves just wait for updates from the master.
In the push mode, the slaves ping the master, asking for updates. This process
can be synchronous - any change is immediately propagated - or asynchronous -
more common, changes are propagated in batches. The master can be manually
chosen during the setup of the master. If the master fails, the system is left
without a master, tho. To address this, the master can be elected automatically
by the cluster. This approach is best-suited for read-intensive applications,
since the master represents a bottleneck for the writing operations. Also, if
the master fails, write operations will be lost until a new master is elected.

#### P2P

- Definition
- Replica convergence strategies
  - Last write wins
  - Vector clock detection
- Conflict correction strategies

In the P2P approach, write operations are allowed on every node. Any write
operation has to be propagated to every other node. The single point of failure
of the master-slave approach is thus solved. Performance increases, since write
operations are distributed as well. The main drawback is inconsistency, since
two nodes might receive a write request at the same time. This problem increases
with high number of write requests and with slow propagation. In case of
conflicts, replica convergence must be performed. The simplest approach is to
consider the timestamp of the concurrent updates. Last write wins. Clocks from
different machines might not be comparable, tho. Another problem of this
approach is that updates might happen on different properties of the same
object. Information might get lost. To tackle this, the timestamp might be
applied at finer granularity. Another approach is the so-called vector clock
detection. Other than determining a partial order in the operations, the goal is
to also detect casualty relationship between events. If an order between the
events is found, the latest version of the data is returned to the client.
Otherwise, there might be a conflict, which has to be resolved manually. Each
vector timestamp represents the number of operations that have been done on a
certain datum for each node of the network - sending/receiving updates count as
an operation.

![vector clocks](./assets/images/vector_clocks.png)

![vector clocks ordering](./assets/images/vector_clocks_ordering.png)

If `V = V'`, the two replicas hold the same value. If `V < V'`, the first is a
stale version of the value. If `V || V'`, there might be a conflict, and the
user needs to solve it. We also need to establish when to solve this conflict.
If the correction is done when the first read occurs, we have a read repair
approach. If the correction takes place during the first write operation, we
have a write repair approach. Both of this approaches slow down the respective
operation, which impact responsiveness, which is a key feature of the system. In
the asynchronous repair approach, the correction is not part of neither a read
or a write operation.

### The CAP theorem

- Definition
- RDBMS approach
- Consistency vs Availability

CAP is an acronym for:

- Consistency. All copies of the data have the same values. If the system is not
  able to figure out the correct answer, it doesn't give any answer.
- Availability. Reads and writes always succeed. Particularly important in any
  system for which a quick answer is more important than a correct answer.
- Partition tolerance. Whenever a failure occurs in the network - thus
  generating a partition, which means that two machines are not able to
  communicate -, the system is still able to maintain its properties.

The CAP theorem says that any system cannot guarantee these three properties
simultaneously. Any system can guarantee two of these properties at most. Given
that large systems will partition at some point, we are left with either
consistency or availability. Traditional DBMS choose consistency over
availability, whereas many new systems work the other way around. If one is
willing to wait a few instants, all three of these properties can be guaranteed.
If working on a centralized machine, thus not requiring partition-tolerance, we
obtain a system which can be consistent and available - i.e. a traditional
RDBMS.

In case of a partition happening, the drawback of the consistent approach is
that requests will have to wait. The drawback of the available system is that
the responses might be inconsistent. Modern systems tend to lean towards
availability, but they don't completely give up consistency.

### Consistency

- Levels of consistency
  - Types of eventual consistency
- BASE properties

Different level of consistency can be granted:

- Strong consistency. At the end of an update operation, any subsequent access
  will return the updated value - this is known as linearity in traditional
  databases.
- Weak consistency. At the end of an update operation, there are no guarantees
  that any subsequent access will return the updated value.
- Eventual consistency. A kind of weak consistency that, in absence of further
  updates, guarantee that, eventually, all accesses will return the value
  consistent will the last updated. This can be implemented in different ways:

  - Causal consistency guarantees that processes that have a causal
    relationships will access consistent data.
  - Read-your-write consistency guarantees that a process that has performed an
    update will never access the old value.
  - Sessions consistency guarantees read-your-write consistency as long as the
    session is active.
  - Monotonic read consistency guarantees that if a process acquired a certain
    value, each subsequent access cannot return previous values.
  - Monotonic write consistency guarantees the serialization of write operations
    by the same process.

Multiple of this properties can be guaranteed by a certain system.

When it comes to traditional distributed systems, the most recommended approach
is the two-faced commitment protocol. This protocol allows to maintain
consistency, but doesn't guarantee availability. Different systems were proposed
in order to provide availability, at the price of sacrificing some consistency.
This is done because in many practical cases strong consistency is not that
crucial. Even if real-time requirements are in place, if conflicts occur, other
data correction systems can be applied on top of a NoSQL system.

NoSQL systems provide BASE properties, instead of ACID properties:

- Basically available;
- Soft state;
- Eventually consistent.

This systems stay in a soft state for a very brief period of time - often less
then a second.

### Storage layout

#### Strategies

- Row based storage
- Column based storage
- Column based storage with locality groups

When it comes to relational model's tables, they can be stored as rows or as
columns. This choice may lead to different performance, depending on data access
patterns. This also depends on the ratio of read and write operations, and the
size of the subset of rows that it's usually accessed.

Row-based storage provides good locality of access for different columns of the
same row. Reading and writing a single row is a single I/O operation. On the
other hand, accessing all values for a single column means accessing a huge
number of files.

![row based storage](./assets/images/row_based_storage.png)

Column-based storage has mirrored pros and cons.

![column based storage](./assets/images/column_based_storage.png)

Column storage with locality group organizes columns into families - locality
groups - of columns that need to be accessed together. Within a single group,
this grants the advantages of both the row based storage and the column based
storage.

![locality groups storage](./assets/images/locality_groups_storage.png)

#### LSM

- Definition
- SSTables
- Reading and writing
- Compaction

To improve performances of write operations, log structured merge trees are
used. Random I/O writes are very bad, performance-wise. LSM trees allow to
convert random writes to sequential writes. We maintain a memtable - a data
structure in memory - on which we perform write operations, and a - sequentially
accessed on disk - commit log file. The memtable is occasionally flushed as it
is to disk to the SSTable. A memtable can be seen as a materialized view of the
requested changes. Data is persistently stored in immutable SSTables. The
memtable holds data which has not been persisted in the SSTables yet. The commit
log file is an append-only log of every write operation.

SSTables are disk-resident ordered immutable data structures. It's the snapshot
of the operations over a period of time and it's created by the flush process
from the memtable. It is composed by:

- The data block, a sequence of ordered key-values pairs.
- The index block, keys mapped to data-block pointers, pointing to where the
  actual record is located.

Every value has a timestamp associated to it, which determine if the value is
alive or if it's been - logically, not physically - deleted. When writing, we
write to the memtable. When reading, we merge the contents of the memtable - so
that we are sure to get the latest value - to the content of the different
SSTables - so that we are sure that the whole dataset is present, as only a part
of it could be in the memtable. By using bloom filters[^bloom_filter], the read
operation can be executed very quickly. Read and write operations are very fast
in this kind of systems.

When flushing the memtable, we create a new SSTable and we update the commit log
on disk. The commit log in memory can be destroyed, and the memtable can be
emptied. When the number of SSTables increases, different SSTables will be
compacted into a single one.

Compaction includes:

- Delete records which are not alive anymore. They are not removed physically. A
  timestamp - called tombstone - is added to the record.
- Merge multiple updates into one.

Many real system do not implement compaction, since it's not considered
essential.

![compaction](./assets/images/compaction.png)

[^bloom_filter]:
    A probabilistic data structure, used to check a value
    membership in a set. Positive answers are always true; negative answers may
    be false negatives. They use a single bitmap key data structure, which
    usually takes up $10n$ bits for $n$ elements. To decrease the number of
    false positives, due to conflicts in the usage of hash functions, different
    hash functions are used.

### Use cases

- Use cases
- Polyglot persistence

Whenever: the amount of data is huge - to the point where it's not storable on
the same machine -, the schema must be flexible, the number of relations is very
high compared to the number of objects - especially in systems that don't
implement the `JOIN` operations -, the goal is storing events or temporal data,
the goal is data analysis - which enjoys the already aggregated data.

Many data models depend on the nature of data. Polyglot persistence aims at
always using the best data store depending on the nature of data.

### Data models

The 4 main families of NoSQL systems depend on the data model used for data
organization.

1. Key-value data model.
2. Document data model. Which is also based on a key-value structure.
3. Graph data model.
4. Column family data model.

Different data models imply the usage of different query languages.

#### Key-value

- Definition
- Pros and cons
- Data access pattern
- Examples

The key-value data model is the simplest model. The main idea is to use a hash
tables. The basic insert - put - operation takes a key and the value to
associate to it, whereas the fetch - get - operation takes only a key, and
returns the value associated to it. In many systems, the insert and the update
operations are the same operations. The system is agnostic of the nature of the
values. It is on the application to know how to handle the values.

It's a very fast and scalable system, which offers eventual consistency and
fault-tolerance. In return, the system is agnostic of the nature of the value
and cannot model more complex data structure - such as objects. Also, it is
necessary to know the key in order to access a specific entry of the data.

When using a key-value data model, one has to choose which pattern to use when
accessing the data - i.e. if handling readers and books, I have to decide if
aggregating readers into books, or the other way around. This has to be chosen
based upon the workload that the system will be exposed to. Depending on this
decision, some operation will not be doable at all. We can create more than one
collection, to make the system more flexible. We can also store the same
information in multiple structure, thus creating a lot of data duplication, but
being able to provide quick operations.

Some examples of this data model are SimpleDB, Redis and Dynamo. Redis is the
only one that has some business applications.

![key value example](./assets/images/key_value_example.png)

#### Column family

- Definition
- Examples

The column is the smallest instance of data. It is a tuple which contains a
name, a value and a timestamp. The key is associated a group of column-value
pairs. The timestamp tells if the value is still valid, or not. Columns are then
organized in different families, to represent different associations of data.

Some examples of this data model are BigTable and CASSANDRA. CASSANDRA offers a
declarative query language, similar to SQL, which only provides a syntax.

![column family example](./assets/images/column_family_example.png)

#### Document based

- Definition
- Indices and joins
- Examples

To each key, we associate a document, which in turn will associate keys to
values - XML, JSON - which can also be array or even other documents, thus
creating a tree structure. Unlike relational tables, each document can have
different structure, so if a value is missing for a certain key, the key is not
even specified.

We can use index structures, so that data can be accessed not only using keys,
but also indexed properties. This kind of system was also able to implement a
sort of `JOIN` operation, which doesn't provide all of the functionalities of
the traditional one.

Some examples of this data model are MongoDB and Couchbase.

![key document example](./assets/images/key_document_example.png)

#### Graph data

- Definition
- Scalability and clustering
- Differences with other approaches

Based on graph theory. We use what is called a property graph, where nodes and
edges have a type. We can associate additional information to nodes and edges,
as well as creating more than one edge between two nodes - as long as the edges
type is different.

Usually, systems implementing this data model run on a single machine.
Therefore, they scale very well vertically, and guarantee transactions and ACID
properties. New systems like neo4j can work on a cluster of machines, giving up
transactions and ACID properties.

Unlike the previous ones, the graph data model is not an aggregate data model,
but a detail data model. There is still the possibility to create index
structure, so that lookups are possible also on non-key properties. This model
is useful only when it's possible to identify relationships between different
nodes.

![graph example](./assets/images/graph_example.png)

### MapReduce

- Definition
- MapReduce and nodes
- Workflow
- Handling worker failure
- Apache Hadoop

NoSQL systems try to use the map reduce paradigm. A map reduce job takes as
input a collection of pairs and produces a collection of pairs. The big
advantage is that the computation can be distributed over different machines in
the same cluster, when parallel - which is always true for map operations. To
perform reduce tasks, the key space is partitioned. Each partition becomes a
separate reduce task.

Some nodes of the P2P system will be in charge of map functions, some other will
be in charge of reduce functions. This system is fault-tolerant, as anytime a
node fails its workload is passed on to another node. This management is done by
the system and has no impact on the client. The file system has to be
distributed. Each file is divided into blocks of fixed size, and each block is
stored on a data node. There needs to be a main node - name node -, who has the
knowledge about where all other nodes are, and their status. Fault-tolerance is
guaranteed by replicating each block on an established number of data nodes.

Each map worker is in charge of map tasks related to blocks that are replicated
on that machine. The result of a map task is written on the worker's local file
system. Reduces workers read the content of each map worker. The result of a
reduce task is written on the distributed file system - after being sorted by
key. Reduce workers should start only when map workers are finished. This is a
key synchronization problem.

When a worker fails, map tasks are rerun, because their output is lost, whereas
reduce tasks are not rerun, since their output is written in the distributed
file system.

![map reduce example](./assets/images/map_reduce_example.png)

Google implementation of map reduce is proprietary, whereas Apache Hadoop is
freely available. For this reason, many systems have been built on top of the
latter.

Hadoop is based on the Hadoop distributed file system - HDFS. This distributed
file system is able to handle hardware failure. It's able to work with streams
of data and large data sets. Nodes are divided into a name node, and one or more
data nodes, which may be grouped in racks. The name node is always aware of the
location and the status of data nodes.

![hdfs architecture](./assets/images/hdfs_architecture.png)

## Database design

### UML class diagram notation

- Description
- Elements
- Relationships
  - Association
  - Generalization
  - Aggregation/composition
- Improvements for NoSQL
- Workflow

Used to describe the structure of an application in terms of classes and objects
that exist in its domain. The primary purpose is to create a vocabulary that is
used by both the analyst and the users.

A class is a template that we use to create specific instances of objects in the
application domain. They specify attributes and behaviors of a set of objects.
An object is a specific instance of a class. Each object has a state and a
behavior - the operations that the object can execute. Each object has a unique
id, but we take it for granted, so it's not necessary to include it in the
diagram. Attributes of a class are the information about the object that we want
to capture. The operations that are part of the behavior can be classified in
constructor, queries and updates.

![uml class](./assets/images/uml_class.png)

Classes may have relationships among one another. Relationships have a
multiplicity - if not specified, the multiplicity is 1. They also may have an
arrow, which indicate the direction the relationship should be navigated with.
Relationships belong to 3 basic categories:

- Association relationships. Bidirectional semantic connections between two
  classes. The relationship needs a name and a role for each class which is part
  of it. Roles are particularly important when the relationship is between a
  class and itself. I may also specify the minimum or maximum number of
  instances that can be involved in the relationship. The cardinality may often
  represent the average number of occurrences of the instance in the
  relationship. The relationship may be represented by another class, with
  properties of its own.
- Generalization relationships allow to create classes that inherit attributes
  and operations of other classes. Subclasses may have additional properties and
  also override properties or operations of the superclass.
- Aggregation relationships are a specialized form of association, in which a
  whole is related to its parts.

![uml relationships](./assets/images/uml_relationships.png)

To improve how UML applies to NoSQL system, we can extend it by providing some
optional information:

- An expected minimum, average and maximum number of instances, for every class.
- The minimum and maximum number of allowed occurrences of an attribute.
- The subset of attributes that represent the key of the class.
- Identify weak classes: classes which are not able to uniquely identify their
  instances without relying on another class. Weak classes can be represented by
  using a dotted line instead of a full line, and by adding the name of the
  supporting entity/ies - in parenthesis - as attributes.
- Attribute types. Basic types, or aggregate types - i.e. records, arrays.

When using an aggregated data model, it is very important to determine the path
among the different classes that we have to follow to provide the usual workload
of the application. This can be done by analyzing the most common queries and
represent the path on top of the UML diagram.

## MongoDB

A document-based NoSQL system, focused on JSON documents.

![mongodb vs relational](./assets/images/mongodb_vs_relational.png)

### Data model

- JSON
- BSON
- Collections
- MongoDB shell
- Aggregation
- Joins
- Indices

JSON documents are the key concept of MongoDB - and the main data type. A JSON
document is a collection of key-value pairs, or an ordered list of values. The
format is simultaneously human readable and machine readable. It supports
Unicode, allowing almost any information in any human language. It's a
self-documented format, thanks to the semantic meaning of the keys. It's always
possible to convert JSON documents from and to XML documents. The strict syntax
leads to pretty simple, consistent and efficient parsing algorithms. Each
document needs to have an `_id` attribute, which will be used as key.

![row vs json](./assets/images/row_vs_json.png)

For ease and efficiency of storage, JSON documents are stored in BSON format -
binary JSON. Storing and comparing binary keys is much more efficient than
storing and comparing string keys. BSON encodes type and length information as
well, to make handling data even more efficient. The BSON format is no longer
human readable, but this doesn't affect the frontend of the system in any way.
BSON allows to store more data types than JSON - i.e. dates, specific numeric
types.

JSON documents are grouped in schema-free collections. The same collection may
hold documents which have completely different structures from one another. It's
still useful to keep similar documents in the same collection, for performance
purposes.

By default, MongoDB runs on port `27017`, and provides a JavaScript based shell.

![mongodb shell](./assets/images/mongodb_shell.png)

Inside the shell, the `db` variable always refers to the database currently in
use. The `find` method is by far the more important and provides a similar
behavior to the relational `SELECT FROM WHERE`. It's only possible to perform
queries within the same collection, not among different collections. It takes
the query to perform, and projection of the desired attributes. The `_id`
attribute is always included in the returned attributes, unless explicitly
omitted. The query is always performed before the projection.

MongoDB provides three ways of aggregating data:

- Single purpose aggregation - i.e. `count`, `group`, `distinct`.
- Aggregation pipeline: a sequence of operators each of which generates a new
  intermediate document. This is done by using the `aggregate` JS function. Each
  stage can use up to 100MB of RAM, and generates an error otherwise. The output
  document shouldn't exceed 16MB of size. Using the `$match` stage at the
  beginning of the pipeline reduces the documents to process and improves
  performance. The `$project` stage returns one document for each document,
  adding or removing fields. The `$group` stage separates documents into groups
  according to a group key and one or more accumulator operators - i.e. avg,
  sum, count, min, max. The `$unwind` stage allows to unfold a document with
  respect to a specific array field, generating multiple copies of the same
  documents: one for each element of the array. This is particularly useful,
  because the `$group` operator is not able to work on array fields. The `$out`
  stage occurs at the end of the pipeline and it allows to write the resulting
  document into a collection.

  ![aggregation pipeline](./assets/images/aggregation_pipeline.png)

- MapReduce, using the `mapReduce` JS function.

For performance purposes, MongoDB doesn't provide the join operation as the
relational model intends it. However, the `$lookup` operator allows to perform a
left outer join inside an aggregation pipeline.

It is possible to define indices: data structures that store a small portion of
the documents in an easy to traverse form. They store ordered values of a
specific fields, to efficiently support equality, sorting and range-based
queries. Different types of index are available - i.e. single field, compound
fields, geospatial. The goal is usually to create single field indices since
they are very easy to maintain and performant. Compound indices are useful only
when all the fields that compose them are accessed. The order of the fields in a
compound index matters. Text indices are used when searching for strings. They
only store root words; no stop words or stem. The index system uses a B-tree
data structure. An index is always created on the `_id` field. The index can
have its own properties - i.e. uniqueness.

### Architecture

![mongodb architecture](./assets/images/mongodb_architecture.png)

![mongodb cluster](./assets/images/mongodb_cluster.png)

- Architecture
- Sharding
- Replication

The architecture has to support storing data in a distributed setting. The Mongo
service is in charge of handling requests to the system. Each shard is in charge
of handling a part of the data. The client can issue requests to the system
using the shell. The Mongo shell can check at any time in the configuration
server which shard holds which part of the data. If a shard does not answer, the
configuration server is also in charge of pointing to the shard that holds the
replicated data.

<!--- cspell: disable --->

- mongod, the core database process. Handles data requests, manages data format,
  and performs operations for background management.
- mongos, controller and query router for the cluster. Processes queries and
  determines where the requested data is located.
- mongo, the interactive shell.
- mongodump, creates a binary export of the db content.
- mongorestore, restores an exported db in another db.
- bsondump, converts BSON files into human readable formats.
- mongoimport/mongoexport, imports/exports from/to JSON/MongoDB.
- mongostat/mongotop/mongosniff, to get analytics about the system.

<!--- cspell: enable --->

Each collection needs one and only one shard key, to determine how it's going to
be distributed. The choice of the shard key cannot be changed after sharding has
happened. This choice can affect performance and scalability. MongoDB partitions
data into chunks. To achieve an even distribution of chunks across shards, a
balancer runs in the backgrounds and migrates chunks.

The partitioning can be done by using the range-based approach. This is
particularly useful when performing range queries, since most of them will be
answered by querying a single shard. Partitioning can also be done by using the
hash-based approach.

![mongodb range partitioning](./assets/images/mongodb_range_partitioning.png)

![mongodb hash partitioning](./assets/images/mongodb_hash_partitioning.png)

Replication can be done by master/slave or by replica set. In the replica set
approach, one of the members of the replica set is the primary, the others are
secondary/recovery members. The latter have different privileges - i.e.
secondary members can answer requests, recovery members cannot. Whenever the
primary member doesn't answer, one of the secondary members is elected as the
new primary. This approach is very flexible in reacting to the network's
workload. Some members may have the role of arbiters: they don't hold data, they
just take part in the network by answering to heartbeats and to election
requests. Clients read from the primary member by default, which will delegate
to secondary members. Clients can explicitly specify a secondary member to read
from. When reading, a desired level of consistency can be specified as part of
the query, since some members may not have the latest version of data.

### Designing with MongoDB

#### JSON schema

- Definition
- Basic types
- Objects
- Arrays
- Metadata
- Validations

A tool for validating the structure of JSON documents. It is not a standard yet.
For each type supported in JSON document, JSON schema allows the specification
of facets, to limit the values a JSON field can assume.

The `string` type allows to specify `minLength` and `maxLength`, regular
expressions and formats like datetime and email. The `integer`/`number` type
allows to specify inclusive and exclusive minimum and maximum, as well as
constraint on the multiplicity. The `boolean` type only matches true and false.

The `object` type deals only with sub-documents. The object size can be
constrained with the `minProperties` and `maxProperties` property. The
`properties` property allows to be more specific about the object's structure.
The specified properties's format needs to be respected, but this doesn't
prevent from adding extra properties. The order is irrelevant. If
`additionalProperties` is set to false, extra properties are not allowed. The
`required` property accepts an array of properties, which will be required.

The `array` type deals only with array data. With the `items` property, we can
constrain the content of the array - i.e. the `type`. We can define `minItems`
and `maxItems` for the array, as well as uniqueness, using `uniqueItems`. With
the `prefixItems` property, we can define the structure of the array - as if it
was a tuple -, down to every single item. We can require the array to only
`contains` a certain number of a certain type of items, using the additional
`minContains`and `maxContains` properties.

The `title` and `description` fields provide a human-readable description of the
document. The `$schema` property declares that this JSON document is a JSON
schema. It's usually used in the root schema. MongoDB supports draft 4 of JSON
schema.

We can specify for each collection which schema to use, using the `$jsonSchema`
parameter. This will make MongoDB check the schema on each document insertion.
MongoDB supports a subset of JSON schema types - i.e. `integer` is not
supported.

![json schema](./assets/images/json_schema.png)

![json schema create](./assets/images/json_schema_create.png)

We can validate fields with validation rules.

![json schema validation](./assets/images/json_schema_validation.png)

#### Design a JSON schema

- Workload analysis
- Logical design
  - Referencing
  - Embedding
- Physical design
- Goals

For a relational database, we would start from requirements analysis. This
includes documentation, analysis of applications previously developed for the
same goal, interviewing experts of the domain. After that we proceed with the
conceptual design: identifying the classes and the relationships among them. We
don't care yet about how the database is stored. Then, with the logical/physical
design, we identify the tables that will implement the entities and
relationships identified at the previous step. At this point, we need to start
taking into account the workload and the typical usage of that the application
will generate. The result is a general-purpose schema, which we can then adapt
to specific applications.

When using MongoDB/NoSQL we cannot follow this process anymore. We need to
immediately start evaluating the application workload, taking into account data
size and the list of useful operations - ranked by importance. This includes
quantifying - how often? how long should it take at most? - and qualifying -
type of access, criticality - the different queries. After that, we proceed with
the logical design: which collections will we create? Which relationships will
we setup among them? This includes creating a CRD - collection relationship
diagram, choosing between referencing[^referencing] or embedding[^embedding].
The physical design follows, during which we identify and apply relevant design
patterns that improve efficiency. At this point, we also have to decide which
indices we will use. The result is already very application specific: a list of
collections and fields, along with data sizes, queries and indices.

Despite MongoDB not mandatorily requiring a schema, it's still important to
design and define one, since this will improve performances a lot. The schema
will then be flexible, and should be designed in a flexible, incremental way.

Getting to a schema where every entity has a separate collection - like it would
be in a relational model - is always a bad solution, since this will require a
lot of `JOIN`s which perform very badly in a distributed system. It's often
possible to use a single collection, which is a very simple solution. To achieve
the simplest solution, the workload analysis should focus on the most frequent
operations. Identifying models and relationships should aim at using mostly
embedding, resulting in using very few patterns. To achieve the most performant
solution, all operations - considering both quantity and quality - should be
considered, as well as using both embedding and linking and a higher number of
patterns.

[^referencing]:
    Referencing related objects using the object id. Referencing
    requires to manually perform more queries, or to use the `JOIN` operation.
    On the other hand, it reduces redundancy. Linked items have to be removed
    manually, when removing the cascading side of the relationship. We will have
    smaller documents, reducing delays due to transfer and avoiding irrelevant
    access to data - i.e. counting the elements of an array doesn't require
    accessing related documents.

[^embedding]:
    Embed related objects directly into the document. Embedding allows
    to retrieve information with a single query, allows to avoid using the
    `JOIN` operation and updates happen on a single document - which guarantees
    respecting ACID properties. Embedding also generates overhead when it comes
    to large documents, and also risks to get close to the 16MB document size
    limit. It also produces data redundancy in representing N:N relationships,
    which introduces consistency problems.

#### From UML to JSON schema

- 1:1 relationships and inheritance
- 1:N relationships
- Aggregation and composition
- N:N relationships
- Security concerns

To represent 1:1 relationships, we can use sub-documents, since they allow us to
represent structured properties and multi-valued properties. The choice of which
to embed in which, depends on the workload or on identifying a predominant class
for the domain. Inheritance is a special case of a 1:1 relationship, so it can
be represented by embedding the fields of subclasses in the parent class, which
will be the only collection. An attribute `type` can be included to represent
the specific type of instance. It might be possible to completely remove the
superclass and only keep the subclasses. This is possible when every member of
the superclass is a member of at least one subclass, and no operation is
specific of the superclass - i.e. the superclass is just a template.

To represent 1:N relationships, different scenarios have to be handled:

- N is a very specific number, and a very low one. In this case, embedding is a
  good strategy. No `JOIN` will be needed, but the document size won't explode
  either.
- N can vary a lot, up to very big numbers. Embedding is not an option, because
  the document could become too big and also generate useless accesses to data.
  We can use the one to zillions notation - i.e. `(0,20,Z)`.
- The many side entity doesn't change frequently. Embedding in the one side
  might be a good strategy, provided the amount of information is not too high.
- The many side entity changes frequently. It might be better to represent it
  with a different collection, with a reference to the one side entity.

Usually, embedding is used in the most queried entity, whereas referencing is
usually used in the many side.

Aggregation and composition relationships are a special case of 1:N
relationships. In case of compositions, embedding on the one side is better, to
showcase the dependency between the two entities. In case of aggregation, since
every component is independent, referencing on the many side is a better
approach.

To represent N:N relationships, we usually consider only a single direction of
the relationship, thus making it a 1:N relationship. We do this by using an
array of sub-documents on any of the two sides of the relationships - depending
on the workload. Embedding or referencing is also chosen based on the workload.
Keeping both sides of the relationship is possible, has a high maintenance cost.
Information will obviously be duplicated as a result. To improve performance, we
can do some indexing on the array.

In general, keeping separate collections is a valid choice when there are
security concerns, and storing different kind of information privately is a
requirement.

### Design patterns

Patterns can improve performances and scalability of queries, but they may also
introduce duplication, data inconsistency and integrity issues. Thus, their
usage is subject to the requirements of the application.

#### Attribute pattern

To be used when a collection contains a very heterogeneous set of properties,
depending on the type of the item. Including very different products in the same
collection may have negative effects on performance. Using a different
collection for each kind of item has the advantage that every collection has the
same structure for every item, but it often requires to query different
collection to retrieve some information.

We should identify all the properties that are common to every kind of item, and
the ones that are specific to each category. Common properties are stored with a
key-value pair. Additional properties are stored in an array field, as a list of
key-value pairs. This kind of model is named polymorphic. This way, all the
items can be stored in the same collection, common characteristics can be
queried very efficiently, and additional properties can be queried as needed. I
can then create an index on the additional properties array field's key.

This pattern is also very useful to group data of the same type that we want to
query at the same time. It is also easier to handle new fields, if they fall
into some grouped set of fields which already exists. We can reuse the same
index for multiple fields, thus having less indices.

#### Extended reference pattern

By means of the extended reference pattern, parts of the information of the
referred collections are introduced in the referencing collection. This can
avoid performing additional costly lookup operations. This introduces some
duplication. For this reason, this pattern is a good choice when we select
fields that don't change often, and only those fields that are needed to avoid
the `JOIN`. After a referenced entity is updated, we have to propagate the
change. Usually this happens asynchronously, to be able to answer the original
request quickly. This implies that there is a time-frame in which the data is
inconsistent. Sometimes, duplication is a good thing - i.e. when preserving the
history of a piece of information is valuable.

#### Subset pattern

Documents sometimes contain many fields which are not useful for the typical
workload. Loading these documents fully can effect performance - worst case
scenario, page swapping. This pattern tries to split the document into two
parts: the part that is usually accessed in the main operations of the workload,
and extra information. The two documents can be related and stored in different
collections, thus reducing the document size in memory and improving
performance. The downsides are the need to sometimes perform lookup queries to
access the extra information, and slightly more disk space usage.

#### Computed pattern

In many situations, when a new individual is introduced in a collection, we need
to count the items, and group them in some way. Performing this operations every
time from scratch can be very time consuming. This pattern aims at minimizing
this effort. What we do, is creating another collection in which we maintain the
status of the original collection - i.e. the number of items contained. This
requires us to change the write operation a bit, to update both collections.

When many physical operations are needed to perform a single logical operation,
we talk about fan out reads/writes. Fan out reads mean that to return the
appropriate data the query must fetch data from different locations. Fan out
writes mean that a logical write needs to physically write to different
locations. The idea of this pattern is to turn fan out reads into fan out
writes. By doing that, we optimize read operations, which makes a big
difference, if that's the most common operation in our domain.

It may be difficult to identify which pre-computed information we need. This
pattern should also not be overused, otherwise the writing operation will soon
become a bottleneck.

#### Schema versioning pattern

Introduced for dealing with different version of the same document that is
contained in the same collection. The idea is to introduce a version number in
the document, which will tell to the system which fields to expect. This result
in the possibility to work on the same collection with different systems that
expect different versions. I can also migrate the old document to the new
version, and this migration can happen asynchronously, without downtimes, thanks
to the backward compatibility this pattern grants. This pattern helps preventing
future technical debt. A downside is that we may need multiple indices to access
the data.

## Graph databases and Neo4j

### Graphs

- Definition
- Machine representation

A graph is an abstract representation of a set of objects where some pairs are
connected by links. Mathematically, a graph is a pair of nodes and edges, where
the edges are a subset of the cartesian product of the nodes with itself.

![graphs](./assets/images/graphs.png)

![more graphs](./assets/images/more_graphs.png)

Inside a machine, a graph has to be represented differently. One way of doing so
is by using an adjacency matrix. Adjacency matrix is a very inefficient way of
storing a graph. Less dense graphs will waste most of the memory - especially if
the number of nodes grows, and undirected graphs will effectively waste half of
the space, since the matrix is symmetric.

![adjacency matrix](./assets/images/adjacency_matrix.png)

To address these problems, a matrix can be compressed, or converted into an
adjacency list.

![compressed adjacency matrix](./assets/images/compressed_adjacency_matrix.png)

### Graph databases

- Definition
- Use cases
- Small vs large graphs
- Scalability
- Pros and cons

Graph databases are effectively NoSQL systems as they share a lot of their
traits. They do not impose a fixed, pre-designed schema, which doesn't require
normalization. We may choose graphs as a logical data model because it's often
the case that data is best modeled as a graph - i.e. a social network's user
base, protein interactions. Graphs work particularly better than relational
models when it comes to N:N relationships. In a relational database, each N:N
relationship requires adding a table to the db. In a graph database, we just
need to add a new edge among already existing nodes. This means that we just
need to store this information in the source node and/or in the target node. The
decision of the node into which to store the edge depends on the direction that
will be more often traversed in the relationship.

![graph nn](./assets/images/graph_nn.png)

Property graphs are the more interesting for this purpose and need to be
distinguished in:

- Small graphs. Hundreds/thousands of nodes. Order of magnitude of MB.
- Large graphs. Millions of nodes.

The distinction is necessary because the set of available operations changes.
Some algorithms wouldn't be able to be executed on a large graph. Either way,
expressing complex queries on graphs is easier.

In a graph database, each node knows its adjacent nodes. Even if the number of
nodes increases, the cost of a local step remains the same, whereas in a
relational database the cost would depend on the size of the connecting table.
We should also maintain different indexing structures that simplify navigating
the graph on a larger scale.

The graph model is a much more powerful/expressive data model. It's fast, orders
of magnitude faster then relational databases when it comes to connected data.
On the other hand, sharding data is tricky - which node goes into which shard? -
and they are not that useful for domains where the data is not often navigated.

### Designing a graph database

When switching from a relational model to the graph model:

- Each row in an entity table is a node.
- The entity table is represented by a label on the above mentioned nodes.
- Each column becomes a property in a node.
- Join tables become relationships, and those tables columns become relationship
  properties

![graph uml](./assets/images/graph_uml.png)

Different kind of queries may be issued on a graph: node/path selection,
navigation, graph/pattern matching, reachability, shortest path,
reliability/capacity of a certain path, critical paths.

### Neo4j

- Definition
- Features

A data management system for property graph databases. Implemented in Java and
Scala. It also has a cloud version. It presents full database characteristics,
including ACID transactions, cluster support and runtime failover. It was
initially developed to be executed on a single machine. The latest versions also
support cluster execution, with the guarantee of eventual and read-your-write
consistency. Traversing single hops both in depth and in breadth takes constant
time. It's the most commonly used graph DBMS.

Nodes represent the objects of the graph, and can be labeled. It is possible to
mark a single node with multiple labels, but it's not encouraged. Relationships
relate nodes by type and direction. Self relationships are also supported. Nodes
and relationships may have properties: name-value pairs, where the name must be
a string and the value can be any primitive type or an array. Each node has an
object identifier associated with it, used within the system for its storage.
This identifier cannot be accessed or used directly.

![neo4j example](./assets/images/neo4j_example.png)

### Cypher

A pattern matching query language designed specifically for graph databases. We
don't use SQL because it's inefficient in expressing graph pattern queries,
especially when the queries are recursive and need to identify paths. Cypher is
declarative: it allows us to state only what we want, without requiring us to
describe how to do it. Each query is compiled to an execution plan that will be
run to produce the desired result. To optimize execution plans, statistical
information about the database are kept up to date. Indexes are supported on
both nodes and relationships, and they are particularly useful to keep track of
where to start navigating the graph.

Cypher is based on ASCII-ART patterns:

- `()-->()`: start from a node, navigate a relationship and get to another node.
- `(A)-->(B)`: start from a node of type `A`, navigate a relationship and get to
  a node of type `B`.
- `(A)--(B)`: same, but without specifying a direction.
- `(A)-[:LOVES]->(B)`: follow a relationship of type `LOVES`.
- `(A)-->(B)-->(C)`: navigate two relationship.
- `(A)-->(B)-->(C)<--(A)`: provide multiple, alternative paths.
- `(:Person {name:"Dan"})-[:LOVES]->(:Person {name:"Ann"})`: search for a path
  between a person of name Dan and a person of name Ann, connected by a `LOVES`
  relationship.

A query is comprised of several distinct clauses, like:

- `MATCH`: the ASCII-ART pattern to match. The most common way to retrieve data.
- `WHERE`: it is part of the `MATCH` clause, and therefore executed at the same
  time.
- `RETURN`: what to return - i.e. nodes, relationships, properties, or some
  combination of those.
- `WITH`: allows to pipe different query parts together.
- `UNWIND`: expand a list into a sequence of records. Often used to create data
  fra collection provided as a parameter.
- `MERGE/CREATE UNIQUE`: create a new node, if it doesn't exist already.
- `DELETE`: deletes a node, but only if it doesn't have any relationship in the
  graph - either delete the relationship first, or use `DETACH DELETE`.
- `SET/REMOVE`: add/delete labels and properties to/from existing nodes.
- `FOREACH`: create/modify properties in a collection of nodes.

Variables can be used to reference parts of a pattern or a query. They are local
to the clause in which they have been declared.

When needed, it's possible to import data from files - i.e. CSV. The importing
tool of Neo4J is a very useful alternative when dealing with a huge amount of
data.

### PGQL

Developed by Oracle, builds a layer on top of traditional relational database,
which allows to interact with the database as if it was a property graph. Some
tables will represent the nodes, some other tables will represent the edges. A
property graph is created in main memory, by considering data made available in
the underlying relational tables. PGQL queries are translated into SQL queries.

![pgql example](./assets/images/pgql_example.png)

![pgql query example](./assets/images/pgql_query_example.png)

## Cassandra

- Definition
- Features
- Pros and cons

A column-oriented data store. Data is stored as columns. This change of
perspective is useful for those workloads where there's no interest in fetching
all the attributes in a row. By processing only the desired columns, queries
become more efficient. The idea is to create materialized views - called column
tables - that maintain only the information needed for a specific kind of query.

Cassandra is an open source Apache project that uses a pure peer-to-peer
architecture with support for global distribution. All nodes are equal,
therefore there is no single point of failure and the system is highly available
and fault tolerant.

The data model is very flexible, with support for complex data structures. The
system is linearly scalable - data load is evenly distributed across all nodes.
Consistency level can be chosen at system level. Thanks to memtable and
SSTables, writes and reads are very efficient.

Cassandra is a good choice when:

- The write throughput is high, whereas the number of reads is low.
- The system should be linearly scalable, by adding commodity servers.
- The system has to be multi-region.
- Each record should have a time-to-live.

Cassandra is not a good choice when:

- The application requires ACID properties.
- The application needs the `JOIN` operation, as it is not supported by
  Cassandra.
- The application needs many secondary indexes, which degrade query performance.
- Dealing with temporal data, or sequences of data.
- The application needs very high read concurrency.

### Architecture

#### Clusters

A node is a single machine containing data replica. Multiple nodes can be
grouped in a cluster, using a ring format. Data is distributed in the cluster
using consistent hashing. There is no constraint on the physical location of a
node when it comes to be part of a cluster. Within each node we can have
keyspaces, collections of column families, equivalent to a relational database.
Each keyspace has a replication factor and a replication strategy. A row is a
unique key - a string with no size limit - associated with different columns. A
column is a triple of key, value and timestamp - used to know if a piece of data
is alive or dead. Each row can have a variable number of columns.

![cassandra cluster](./assets/images/cassandra_cluster.png)

#### Data centers

- Definition
- Remote data centers

A data center is a group of nodes directly connected to each other, which makes
internal communication very efficient. One of the nodes is chosen as a
coordinator and receives requests from the client. The coordinator finds the
node which has the matching token range and persists the data in that node. The
data is replicated to other nodes, according to the replication strategy.

![cassandra data center](./assets/images/cassandra_data_center.png)

When multiple data centers exist, the coordinator either identifies a local node
that has the required data, or a remote data center where to find it. This
obviously takes more time than a local query. When having more than one data
center, the number of replicas per data center has to be defined.

![cassandra multi dc](./assets/images/cassandra_multi_dc.png)

#### Partitioning

Cassandra uses a partitioner to distribute data evenly across the cluster nodes.
A partition key is used for this purpose. Three types of partitioner may be
used: Murmur3 - the default one -, Random, ByteOrdered.

#### Consistency

- Read consistency
- Write consistency
- Hinted handoff

In order to grant better performance, Cassandra provides tunable consistency to
calibrate the tradeoff between consistency and availability. The consistency
level can be defined for each read or write operation. Read consistency levels
are:

- `ONE`, returns data as soon as one replica is received. This is the lowest
  consistency level, and the default one.
- `QUORUM`, returns data as soon as a quorum of replicas - a quantity greater
  than 50% of the replicas - is received.
- `LOCAL_QUORUM`, returns data as soon as a quorum of replicas within the data
  center is received.
- `EACH_QUORUM`, returns data as soon as a quorum of replicas is received from
  each data center where replicas are specified.
- `ALL`, returns only after all replicas are received.

When multiple replicas are received, the replica with the most recent timestamp
is returned as the result of the read.

Write consistency levels, which applies to insert, update and delete operations,
are:

- `ANY`, returns as soon as a replica is written to any node or a commitlog.
- `ONE`, returns as soon as a replica is written to the correct node - the one
  that the coordinator pointed to.
- `QUORUM`, `LOCAL_QUORUM`, `EACH_QUORUM`, `ALL`, same concept as read
  consistency.

Read and write operations will fail if the specified level of consistency is not
achieved, due to node failure - i.e. the consistency level is `ALL`, and one of
the replica nodes is off.

During data writes, when one of the nodes to write to is down, the hinted
handoff strategy is used. One of the live nodes acts as the temporary write
node, writing to the commitlog and to its own filesystem. When the actual node
is up again, the temporary node will send the data to it. Temporary nodes count
towards the `ANY` consistency level. If the actual node never recovers, a new
node will be elected to store that piece of data.

#### Time to live

- Definition
- Tombstones

In Cassandra it's possible to store data only for a limited period. The number
of seconds for which data should be stored is called time-to-live. It's
specified during an update or an insert, and data is removed - set to NULL, the
row is not removed - when that period goes by.

Since distributed deletes might cause anomalies, tombstones are used to handle
deleted data. When deleting a piece of data, a value called tombstone is created
and propagated to all replicas. After all replicas are removed, tombstones still
exists and are removed only during compaction, after a grace period.

### Data model

- Column family
- Design strategy

![cassandra vs relational](./assets/images/cassandra_vs_relational.png)

A column family is a list of key-value pairs. The order matters, and should be
designed to optimize access. It's a sorted map, which makes lookups and scans
very efficient. The number of column keys is not bounded. The key can itself
hold a value. Time-to-live is associated to each column value. It is also
possible to define super column families, but they are not actually used.
Columns can be composite, which means that the column key is composed of
multiple keys separated by `|`.

![cassandra column family](./assets/images/cassandra_column_family.png)

The design strategy starts by considering the conceptual model of a given
domain. Then, by identifying the workload, we have to isolate the most frequent
and time-sensitive queries. Column families should be tailored to satisfy those
queries, in order to optimize the execution. Usually, normalization is not a
priority. De-normalizing data can improve read performance, but should be done
only when needed.

### CQL

- Definition
- Composite keys
- Collections as values
- Group by
- Insertion
- Batch statements

A declarative query language designed to describe queries and column families
for the Cassandra data model to be easily communicated. It doesn't support joins
and sub-queries. Column families improve performance in the same way
materialized views do. It supports many standard data types. A particularly
useful one is `timeuuid`: a unique identifier which depends on the moment it has
been created. This allows to both uniquely identify the items, but also to
easily order them by creation date.

![cql example](./assets/images/cql_example.png)

CQL allows to define column families with composite keys. The first column in a
composite key is still used as the partition key - the remaining columns of the
composite key are used to cluster the data: all the rows sharing the same
partition key will be sorted by the remaining components of the composite key.
When composite keys are used, queries should reflect the organization of the
composite key. Only the last column of the composite primary key can be omitted
from the `WHERE` clause, so that the selected set of rows is guaranteed to be
continuous.

![cql composite key example](./assets/images/cql_composite_key_example.png)

Sets, lists and maps can be used, but not as keys.

![cql list example](./assets/images/cql_list_example.png)

The `GROUP_BY` clause can only be used on columns that are part of the primary
key - used in the correct order. If one of the columns of the primary key is
used in an equality in the `WHERE` clause, it can be omitted from the `GROUP_BY`
clause. Aggregate functions will produce a separate value for each group. If no
`GROUP_BY` clause is specified, they will produce a value for each row. When
grouping, if no aggregate function is specified for a selected column, the first
value encountered for each group is returned.

When inserting, at least the columns that are part of the key must be specified.
Insertion doesn't check the prior existence of the row - this can be done using
the `IF NOT EXISTS` condition. If it did exist, the existing row is updated, if
not a new row is created. Time-to-live can be specified by using the `USING TTL`
clause.

`BATCH` statements can be used to group multiple operations that concern the
same piece of data. These operations should either all happen, or none. No ACID
property is guaranteed anyway.
