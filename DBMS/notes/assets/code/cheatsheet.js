// Insert a new document in the `users` collection - create it, if not existing
// yet.
db.users.insertOne({ age: 29, name: 'Simone' });

// Insert a new document and manually define the `_id`.
const customer_id = ObjectId();
db.users.insertOne({ _id: customer_id });

// Manually link a collection to another.
db.orders.insertOne({ customer_id: customer_id });

// Insert multiple new documents in the `users` collection.
db.users.insertMany([
  { age: 29, name: 'Simone' },
  { age: 25, name: 'Elisa' },
]);

// Return all attributes of all documents inside the users collection.
db.users.find();

// Fetch all female users, older than 32. Return only the information about the
// name. Sort by age, and limit to the first result.
db.users
  .find({ gender: 'female', age: { $gt: 32 } }, { name: 1 })
  .sort({ age: -1 })
  .limit(1);

// Count the documents inside the users collection.
db.users.find().count();

// Return the users which are either not female or aged between 32 and 33.
db.users.find({
  $or: [{ gender: { $ne: 'female' } }, { age: { $in: [32, 33] } }],
});

// Update the root attribute of all users of age 32.
db.users.update({ age: 32 }, { $set: { role: 'root' } }, { multi: true });

// Delete all users of age 32
db.users.remove({ age: 32 });

// Currently in use database is dropped.
db.dropDatabase();

// Return distinct values for the name attribute. This is an example of single
// purpose aggregation.
db.users.distinct('name');

// Return the sum of the amounts of all orders in `PURCHASED` status, grouped by
// `customer_id`. This is an example of an aggregation pipeline.
db.orders.aggregate([
  { $match: { status: 'PURCHASED' } },
  { $group: { _id: '$customer_id', total: { $sum: '$amount' } } },
]);

// Same, but using the MapReduce paradigm.
db.orders.mapReduce(
  (document) => {
    emit(document.customer_id, document.amount);
  },
  (key, values) => Array.sum(values),
  { query: { status: 'PURCHASED' } }
);

// Left outer equality join from orders to users.
db.users.aggregate({
  $lookup: {
    from: 'orders',
    localField: '_id',
    foreignField: 'customer_id',
    as: 'customer_orders',
  },
});

// Left outer join from orders to users, only for order with an amount higher
// then 50. The user name is omitted.
db.users.aggregate({
  $lookup: {
    from: 'orders',
    let: { id: '_id' },
    pipeline: [
      {
        $match: {
          $expr: {
            $and: [
              { $eq: ['$customer_id', '$_id'] },
              { $gte: ['$amount', 50] },
            ],
          },
        },
      },
      { $project: { name: 0 } },
    ],
    as: 'big_orders',
  },
});

// Create a single field index.
db.users.createIndex({ lastName: 1 });

// Create a compound field index.
db.users.createIndex({ firstName: 1, lastName: 1 });

// Create a text index on the `firstName` field.
db.users.createIndex({ firstName: 'text' });

// Create a text index on every attribute of the `users` collection.
db.users.createIndex({ '$**': 'text' });

// Match an attribute of a sub-document. Also works for array fields.
db.users.find({ 'address.city': 'Basel' });
db.users.find({ 'addresses.city': 'Basel' });

// Match multiple conditions disjunctively on the elements of an array field.
db.users.find({ 'addresses.city': 'Basel', 'addresses.city': 'Zurich' });

// Match multiple conditions at the same time on the elements of an array field.
db.users.find({
  addresses: { $elemMatch: { civicNumber: { $gt: 20, $lt: 30 } } },
});

// Check the existence of a document field. Field order is relevant.
db.users.find({ 'address.city': { exists: true } });

// Check a specific index in an array.
db.users.find({ 'addresses.1.city': 'Zurich' });

// Query a date attribute.
db.users.find({ birthday: ISODate('1993-12-30T00:00:00Z') });

// Query an array field by length.
db.users.find({ addresses: { $size: 3 } });

// Examples of projecting fields in an aggregation pipeline.
db.users.aggregate([
  {
    $project: {
      _id: 0,
      'address.city': 1,
      name: '$firstName',
      old: { $cond: { if: { $gt: [18, $age] }, then: true, else: false } },
    },
  },
]);

// Count the number of users for each age.
db.users.aggregate([{ $group: { _id: $age, count: { $count: {} } } }]);
db.users.aggregate([{ $group: { _id: $age, count: { $sum: 1 } } }]);

// List the last names for each first name.
db.users.aggregate([
  { $group: { _id: $firstName, names: { $push: $lastName } } },
]);

// Count the number of users for each age, and only keep the age groups with
// more than three users.
db.users.aggregate([
  { $group: { _id: $age, count: { $sum: 1 } } },
  { $match: { count: { $gte: 3 } } },
]);

// Count the number of addresses each users has.
db.users.aggregate([
  {
    $unwind: {
      path: $addresses,
      includeArrayIndex: 'string',
      preserveNullAndEmptyArrays: false,
    },
  },
  { $group: { count: { $sum: 1 } } },
]);

// Create and use an index on the additional properties of an attribute pattern.
db.users.createIndex({ 'career.degree': 1, 'career.avg': 1 });
db.users.find({
  career: { $elemMatch: { degree: 'CS', avg: { $gt: 27 } } },
});

// Query a group of dates in one single query. `examDates` looks something like
// `[{k: 'DBMS', v: '2023-01-22'}, {k: 'PPS', v: '2023-02-23'}, ...]`
db.users.find({ 'examDates.v': { $gte: '2022-01-01', $lte: '2022-12-31' } });
