// Find and return all nodes in the graph.
MATCH (n)
RETURN n;

// Find and return all Person nodes.
MATCH (n:Person)
RETURN n;

// Find and return all nodes with a name property with value Jane Doe.
MATCH (n { name: "Jane Doe" })
RETURN n;

// Find and return all Person nodes with age in the range [12,19).
MATCH (n:Person)
WHERE 12 <= n.age AND n.age < 19
RETURN n;

// Find all Students and all Instructor, and return their cartesian product.
MATCH (n:Student), (m:Instructor)
RETURN n, m;

// Find and return two specific node, assuming ids are unique.
MATCH (n:Student { id: 121 }), (m:Instructor {id: 552})
RETURN n, m;

// Find instructors and the students they advise. Return pairs of their names.
MATCH (n:Instructor)-[:Advises]->(m:Student)
RETURN n.name, m.name;

// Same, but group by instructor.
MATCH (n:Instructor)-[:Advises]->(m:Student)
RETURN n.name AS instructor,
collect(m.name) AS students;

// Find teachers and students for all courses. Return triples of instructor,
// course and student.
MATCH (i:Instructor)-[:Teaches]->(s:Section)<-[:Takes]-(st:Student)
RETURN i.name AS instructor,
s.sectionId AS section,
st.name AS student;

// Match more then one type of relationships.
MATCH (wallStreet { title: 'Wall Street' })<-[:ACTED_IN|:DIRECTED]-(person)
RETURN person.name;

// Match all movies related to Charlie Sheen by 1 to 3 hoops.
MATCH (martin { name: 'Charlie Sheen' })-[:ACTED_IN*1..3]->(movie:Movie)
RETURN movie.title;

// Find the shortest path between two nodes, of maximum length 15.
MATCH (martin:Person { name: 'Charlie Sheen' }),
(oliver:Person { name: 'Oliver Stone' }),
p = shortestPath((martin)-[*..15]-(oliver))
RETURN p;

// Find all shortest paths between two nodes.
MATCH (martin:Person { name: 'Charlie Sheen' }),
(oliver:Person { name: 'Oliver Stone' }),
p = allShortestPaths((martin)-[*]-(oliver))
RETURN p;

// Find a friend of Oliver who has at least one other friend.
MATCH (david { name: 'David' })-[]-(otherPerson)-[]->()
WITH otherPerson, count(*) AS foaf
WHERE foaf > 1
RETURN otherPerson.name;

// Sort results before using collect.
MATCH (n)
WITH name
 ORDER BY n.name desc
RETURN collect(n.name);

// Get the distinct elements of a list.
WITH [1, 1, 2, 2] AS coll
UNWIND coll AS x
WITH DISTINCT x
RETURN collect(x) AS SET ;

// Create a node of type Person.
CREATE (:Person { name: 'Simone' });

// Create a node with more than one label.
CREATE (:Person:Student { name: 'Simone' });

// Create a relationship between nodes of type Person.
CREATE (:Person)-[:LOVES]->(:Person);

// Create a contraint on type Person.
CREATE CONSTRAINT ON (p:Person)
ASSERT p._id IS UNIQUE;

// Create an index on type Person.
CREATE INDEX ON :Person(age);

// Load data from a CSV file.
LOAD CSV FROM 'persons.csv' AS csvLine
MERGE (p:Person { userId: toInteger(csvLine[0]), name: csvLine[1] });

// Same, but one block at a time, to handle huge amounts of data.
USING PERIODIC COMMIT LOAD CSV FROM 'persons.csv' AS csvLine
MERGE (p:Person { userId: toInteger(csvLine[0]), name: csvLine[1] });

// Return information about the type of a relationship.
MATCH (a:Actor)-[r]-(m:Movie)
WHERE p.name = "Tom Hanks"
RETURN m.title, collect(type(r))

// Return all the labels in a graph
MATCH (n)
RETURN DISTINCT LABELS(n)

// Return all the relationships in a graph
MATCH ()-[r]-()
RETURN DISTINCT type(r)
