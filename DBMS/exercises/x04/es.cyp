// 1. retrieve the name and age of the users.
MATCH (u:User)
RETURN u.username, u.age

// 2. retrieve the name and age of the female users whose age is below 23.
MATCH (u:User { gender:'F' })
WHERE u.age <= 23
RETURN u.username, u.age

// 3. retrieve the name and age of the users whose age is 21 or 23.
MATCH (u:User)
WHERE u.age IN [21, 23]
RETURN u.username, u.age

// 4. identify the number of user for gender.
MATCH (u:User)
RETURN u.gender, count(*),
collect(u.gender)

// 5. identify the gender with the highest number of users.
MATCH (u:User)
RETURN u.gender, count(*) AS count,
collect(u.gender)
 ORDER BY count DESC
LIMIT 1

// 6. For each user identify the mail that he/she has sent.
MATCH (u:User)-[:SENT]->(e:Email)
RETURN u.username,
collect(e.id)

// 7. Identify the pair of friends such us the first one sent an email to the
//    other.
MATCH (u1:User)-[:SENT]->(:Email)-[:TO]->(u2:User), (u1)-[:FRIEND_OF]-(u2)
RETURN DISTINCT u1.username, u2.username

// 8. return all types of nodes used in the current database.
MATCH (n)
RETURN DISTINCT LABELS(n)

// 9. return all types of relatioships used in the current database.
MATCH ()-[r]-()
RETURN DISTINCT type(r)

// 12. return all the emails that are REPLY to a previous message.
MATCH (:Email)<-[:REPLY_TO]-(e:Email)
RETURN e

// 13. identify the users that replied to the email 6 and the length of the
//     reply chain.
MATCH p=(Email { id: "6" })<-[r:REPLY_TO*]-(:Reply)<-[:SENT]-(u:User)
RETURN u.username, length(p)

// 14. identify the pairs of messages that are in a reply_to chain of at most 3
//     emails.
MATCH (e1)<-[:REPLY_TO*1..3]-(e2)
RETURN e1, e2

// 15. identify the paths of messages that are in a reply_to chain of at most 3
//     emails.
MATCH p=()<-[:REPLY_TO*1..3]-()
RETURN p

// 16. Determine the number of times the email 11 has been forwarded.
MATCH (Email { id: '11' })<-[:FORWARD_OF*]-(e:Email)
RETURN count(*)

// 17. for each message identify the number of users to which the message is
//     sent to and carbon copy to.
MATCH (e:Email)-[:TO|:CC]-(u)
RETURN e.id, count(*), collect(u.username)
