// Consider the file issues.json representing issues of the database journal
// sigmod record. Each issue of the journal contains the volume and number of
// the issue and the list of articles that appears in that issue. For each
// article, the document contains the tile, initial and final page, and the list
// of authors.

// Provide the Mongodb query expressions for the following queries:
// 1. Retrieve the issues whose volume number is 15, 17, 18, 19, or 22, and that
//    present at most 5 papers, or the first article is titled   "A Study on
//    Data Point Search for HG-Trees."

db.issues.find({
  $or: [
    {
      'issue.volume': { $in: [15, 17, 18, 19, 22] },
      'issue.articles.article.5': { $exists: false },
    },
    {
      'issue.articles.article.0.title':
        'A Study on Data Point Search for HG-Trees.',
    },
  ],
});

// note: you cannot use $size (of an array) with a condition $leq (it is not
// supported)

// 2. Retrieve the article title, the first and last page, and the number of
//    pages

db.issues.aggregate([
  {
    $unwind: {
      path: '$issue.articles.article',
    },
  },
  {
    $project: {
      title: '$issue.articles.article.title',
      firstPage: '$issue.articles.article.initPage',
      lastPage: '$issue.articles.article.endPage',
    },
  },
  {
    $addFields: {
      numPages: {
        $subtract: ['$lastPage', '$firstPage'],
      },
    },
  },
]);

// since "article" is of type array, I have first to unwind the single articles
// This operation does not alter the structure of the single documents.
// Therefore, by means of a project, it is possible to create documents with the
// fields <title, firstPage, lastPage> Finally, I add the field "numPages" whose
// value is obtained by a mathematical operation

// 3. Retrieve the article titles with the number of pages and include a field
//    "authorship" that indicates "single author" or "multiple authors"
//    depending on the number of authors

db.issues.aggregate([
  {
    $unwind: {
      path: '$issue.articles.article',
    },
  },
  {
    $project: {
      title: '$issue.articles.article.title',
      firstPage: '$issue.articles.article.initPage',
      lastPage: '$issue.articles.article.endPage',
      authorship: {
        $cond: {
          if: {
            $eq: [
              {
                $type: '$issue.articles.article.authors.author',
              },
              'array',
            ],
          },
          then: 'Multiple authors',
          else: 'Single author',
        },
      },
      numPages: {
        $subtract: [
          '$issue.articles.article.endPage',
          '$issue.articles.article.initPage',
        ],
      },
    },
  },
]);

// In this collection, when a paper presents a single author, it is represented
// as a string, otherwise it is represented as an array. We take into account
// this characteristics for developing the query expression. Note that an
// alternative approach for counting the number of pages is introduced in this
// example Note also the use of the projection for changing the structure of the
// returned document. Indeed, in the returned document, we have removed the
// nesting structure of the issue for pointing out the title and other fields of
// an article

// 4.   For each article with at least 10 pages, returns the title and the
//      number of authors

db.issues.aggregate([
  {
    $unwind: {
      path: '$issue.articles.article',
    },
  },
  {
    $project: {
      title: '$issue.articles.article.title',
      numAuthors: {
        $cond: {
          if: {
            $eq: [
              {
                $type: '$issue.articles.article.authors.author',
              },
              'array',
            ],
          },
          then: {
            $size: '$issue.articles.article.authors.author',
          },
          else: 1,
        },
      },
      numPages: {
        $subtract: [
          '$issue.articles.article.endPage',
          '$issue.articles.article.initPage',
        ],
      },
    },
  },
  {
    $match: {
      numPages: {
        $gt: 10,
      },
    },
  },
  {
    $project: {
      _id: 0,
      numPages: 0,
    },
  },
]);

//   5. For each author, return the number of articles that he/she has authored
//      and the list of papers’ titles

db.issues.aggregate([
  {
    $unwind: {
      path: '$issue.articles.article',
    },
  },
  {
    $unwind: {
      path: '$issue.articles.article.authors.author',
    },
  },
  {
    $group: {
      _id: '$issue.articles.article.authors.author',
      numPapers: {
        $sum: 1,
      },
      authorPapers: {
        $push: '$issue.articles.article.title',
      },
    },
  },
]);

// the issue document contains a list of papers and then for each paper a list
// of authors. Therefore, I have to execute a double unwind for transforming a
// document with array in an array of documents (i.e. a document representing an
// issue with 10 papers becomes 10 documents repeating the information about the
// issue with a single article per document) Once completed the double
// unfolding, I can group per author's name and count the number of papers that
// he/she has authored and provide the list of titles

// 6.  Identify the top 10 authors of the collection (i.e. the authors that have
//     written the higher number of articles)

db.issues.aggregate([
  {
    $unwind: {
      path: '$issue.articles.article',
    },
  },
  {
    $unwind: {
      path: '$issue.articles.article.authors.author',
    },
  },
  {
    $group: {
      _id: '$issue.articles.article.authors.author',
      numPapers: {
        $sum: 1,
      },
      authorPapers: {
        $push: '$issue.articles.article.title',
      },
    },
  },
  {
    $sort: {
      numPapers: -1,
    },
  },
  {
    $limit: 10,
  },
]);

// it is a little variation of the previous solution in which I have ordered the
// authors according to the number of papers they have authored (in descending
// order) and then taken only the first 10

// 7.   Extract the collection “single_author_articles” (a document for each
//      article)

db.issues.aggregate([
  {
    $unwind: {
      path: '$issue.articles.article',
    },
  },
  {
    $project: {
      title: '$issue.articles.article.title',
      author: {
        $cond: {
          if: {
            $eq: [
              {
                $type: '$issue.articles.article.authors.author',
              },
              'array',
            ],
          },
          then: '$$REMOVE',
          else: '$issue.articles.article.authors.author',
        },
      },
    },
  },
  {
    $match: {
      author: {
        $exists: true,
      },
    },
  },
  {
    $project: {
      _id: 0,
    },
  },
  {
    $out: 'single_author_articles',
  },
]);

// 8.   Identify the articles in which one of the authors belongs to the
//      collection of “single_author_papers” and returns the article title, and
//      the name of the single author

db.issues.aggregate([
  {
    $unwind: {
      path: '$issue.articles.article',
    },
  },
  {
    $unwind: {
      path: '$issue.articles.article.authors.author',
    },
  },
  {
    $project: {
      title: '$issue.articles.article.title',
      author: '$issue.articles.article.authors.author',
    },
  },
  {
    $lookup: {
      from: 'single_author_articles',
      localField: 'author',
      foreignField: 'author',
      as: 'result',
    },
  },
  {
    $match: {
      'result.0': {
        $exists: true,
      },
    },
  },
  {
    $project: {
      article: '$title',
      author: '$result.author',
      _id: 0,
    },
  },
  {
    $unwind: {
      path: '$author',
    },
  },
  {
    $group: {
      _id: {
        article: '$article',
        author: '$author',
      },
    },
  },
]);

// 8. Identify the issue with the highest number of articles

// 9. Count the total number of pages that have been written

// 10. Identify the presence of a paper that has been published more than once
