type Article = {
  title: string;
  initPage: number;
  endPage: number;
  authors: string[];
};

type Issue = {
  volume: number;
  number: number;
  articles: Article[];
};

let db: { issues: Issue[] } = { issues: [] };

// 1. Retrieve the issues whose volume number is 15, 17, 18, 19, or 22, and that
//    present at most 5 papers, or the first article is titled "A Study on Data
//    Point Search for HG-Trees."
db.issues.find({
  // @ts-ignore
  'issue.volume': { $in: [15, 17, 18, 19, 22] },
  $or: [
    { 'issue.articles.article.5': { $exists: false } },
    {
      'issues.articles.article.0.title':
        'A Study on Data Point Search for HG-Trees.',
    },
  ],
});

// 2. Retrieve the article title, the first and last page, and the number of
//    pages

// @ts-ignore
db.issues.aggregate([
  { $unwind: { path: '$issue.articles.article' } },
  {
    $addFields: {
      numPages: {
        $subtract: [
          '$issue.articles.article.endPage',
          '$issue.articles.article.initPage',
        ],
      },
    },
  },
  {
    $project: {
      _id: 0,
      numPages: 1,
      'issue.articles.article.title': 1,
      'issue.articles.article.initPage': 1,
      'issue.articles.article.endPage': 1,
    },
  },
]);

// 3. Retrieve the article titles with the number of pages and include a field
//    "authorship" that indicates "single author" or "multiple authors"
//    depending on the number of authors

// @ts-ignore
db.issues.aggregate([
  { $unwind: { path: '$issue.articles.article' } },
  {
    $addFields: {
      numPages: {
        $subtract: [
          '$issue.articles.article.endPage',
          '$issue.articles.article.initPage',
        ],
      },
      authorship: {
        $cond: {
          if: {
            $eq: [{ $type: '$issue.articles.article.authors.author' }, 'array'],
          },
          then: 'Multiple authors',
          else: 'Single author',
        },
      },
    },
  },
  {
    $project: {
      _id: 0,
      numPages: 1,
      authorship: 1,
      'issue.articles.article.title': 1,
    },
  },
]);

// 4. For each article with at least 10 pages, returns the title and the number
//    of authors

// @ts-ignore
db.issues.aggregate([
  { $unwind: { path: '$issue.articles.article' } },
  {
    $match: {
      $expr: {
        $gte: [
          '$issue.articles.article.endPage',
          { $sum: ['$issue.articles.article.initPage', 10] },
        ],
      },
    },
  },
  {
    $project: {
      title: '$issue.articles.article.title',
      numberOfAuthors: {
        $cond: {
          if: {
            $eq: [{ $type: '$issue.articles.article.authors.author' }, 'array'],
          },
          then: { $size: '$issue.articles.article.authors.author' },
          else: 1,
        },
      },
    },
  },
]);

// 5. For each author, return the number of articles that he/she has authored
//    and the list of papers’ titles

// @ts-ignore
db.issues.aggregate([
  { $unwind: { path: '$issue.articles.article' } },
  { $unwind: { path: '$issue.articles.article.authors.author' } },
  {
    $group: {
      _id: '$issue.articles.article.authors.author',
      numArticles: { $sum: 1 },
      articles: { $push: '$issue.articles.article.title' },
    },
  },
]);

// 6. Identify the top 10 authors of the collection (i.e. the authors that have
//    written the higher number of articles)

// @ts-ignore
db.issues.aggregate([
  { $unwind: { path: '$issue.articles.article' } },
  { $unwind: { path: '$issue.articles.article.authors.author' } },
  {
    $group: {
      _id: '$issue.articles.article.authors.author',
      numArticles: { $sum: 1 },
      articles: { $push: '$issue.articles.article.title' },
    },
  },
  { $sort: { numArticles: -1 } },
  { $limit: 10 },
]);

// 7. Extract the collection “single_author_articles” (a document for each
//    article)

// @ts-ignore
db.issues.aggregate([
  { $unwind: { path: '$issue.articles.article' } },
  {
    $match: {
      $expr: {
        $eq: [{ $type: '$issue.articles.article.authors.author' }, 'string'],
      },
    },
  },
  { $out: 'single_author_articles' },
]);

// 8. Identify the articles in which one of the authors belongs to the
//    collection of “single_author_papers” and returns the article title, and
//    the name of the single author
// 9. Identify the issue with the highest number of articles

// @ts-ignore
db.issues.aggregate([
  { $sortByCount: '$issue.articles.article' },
  { $limit: 1 },
]);

// 10. Count the total number of pages that have been written

// @ts-ignore
db.issues.aggregate([
  { $unwind: { path: '$issue.articles.article' } },
  {
    $group: {
      _id: 'id',
      totalPages: {
        $sum: {
          $subtract: [
            '$issue.articles.article.endPage',
            '$issue.articles.article.initPage',
          ],
        },
      },
    },
  },
]);

// 11. Identify the presence of a paper that has been published more than once
