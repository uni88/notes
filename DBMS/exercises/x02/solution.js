// 1 Find the restaurant which is not in the borough Bronx
db.restaurants.find({ borough: { $ne: 'Bronx' } });

// 2 Find the restaurant_id, name, borough, and cuisine for all the documents in
// the collection restaurant (remove the object identifier)
db.restaurants.find(
  {},
  { _id: 0, restaurant_id: 1, name: 1, borough: 1, cuisine: 1 }
);

// 3 Find the restaurants that achieved a score in the set 65, 85,89
db.restaurants.find({ 'grades.score': { $in: [65, 85, 89] } });

// 4 Find the restaurants that achieved the score 10
db.restaurants.find({ 'grades.score': 10 });

// 5 Find the restaurants located in a latitude value less than -95
db.restaurants.find({ 'address.coord.1': { $lt: 95 } });

// 6 Find the restaurants which do not prepare any cuisine of American and
// achieved a score of more than 70 and located in the longitude less than
// -65.754168
db.restaurants.find({
  cuisine: { $ne: 'American ' },
  'grades.score': { $gt: 70 },
  'address.coord.0': { $lt: -65.754168 },
});

// 7 Find the restaurants which belong to the borough Bronx, Queens or Brooklyn
// and prepare either American or Chinese dish
db.restaurants.find({
  borough: { $in: ['Bronx', 'Queens', 'Brooklyn'] },
  cuisine: { $in: ['American ', 'Chinese'] },
});

// 8 Find the restaurant’s Id, name, and grades for those restaurants where the
// 2nd element of grades array contains a grade of "A" and score 9 on an ISODate
// "2014-08-11T00:00:00Z"
db.restaurants.find(
  {
    'grades.1.grade': 'A',
    'grades.1.score': 9,
    'grades.1.date': ISODate('2014-08-11T00:00:00Z'),
  },
  { _id: 0, restaurant_id: 1, name: 1, grades: 1 }
);

// 9 Find the restaurant’s ID, name, address, and geographical location for
// those restaurants where 2nd element of coord array contains a value which is
// more than 42 and up to 52
db.restaurants.find(
  { 'address.coord.1': { $gt: 42, $lt: 52 } },
  { _id: 0, address: 1, name: 1, borough: 1 }
);

// 10 Find the restaurant’s ID, name, address, and geographical location for
// those restaurants presenting three grades
db.restaurants.find(
  { grades: { $size: 3 } },
  { _id: 0, address: 1, name: 1, borough: 1 }
);

// 11 Check if all the restaurant addresses contain the street or not.
db.restaurants.find({ 'address.street': { $exists: true } });

// 12 For each borough identify the number of restaurants that offer Chinese
// dish
db.restaurants.aggregate([
  {
    $match: {
      cuisine: 'Chinese',
    },
  },
  {
    $group: {
      _id: '$borough',
      count: {
        $count: {},
      },
    },
  },
]);

// 13 For each restaurant identifies the name of the restaurant and the number
// of males and females that stay in the same borough of the restaurant
db.restaurants.aggregate([
  {
    $lookup: {
      from: 'borough',
      localField: 'borough',
      foreignField: 'name',
      as: 'borough_info',
    },
  },
  {
    $project: {
      _id: 0,
      name: 1,
      male: {
        $first: '$borough_info.male',
      },
      female: {
        $first: '$borough_info.female',
      },
    },
  },
]);

// 14 For each borough that presents at least 7000 males and 9000 females,
// return the number of restaurants for each cuisine
db.borough.aggregate([
  {
    $match: {
      male: {
        $gt: 7000,
      },
      female: {
        $gt: 9000,
      },
    },
  },
  {
    $lookup: {
      from: 'restaurants',
      localField: 'name',
      foreignField: 'borough',
      as: 'cuisine',
      pipeline: [
        {
          $group: {
            _id: '$cuisine',
            restaurants: {
              $count: {},
            },
          },
        },
      ],
    },
  },
]);

// 15 For each restaurant identify the top grade that it has got in the
// available evaluation (report the grade none in case it is not present) and
// store this information in a collection (please use your last name as name of
// the collection and remember to remove it when you have finished)
db.restaurants.aggregate([
  {
    $addFields: {
      max_grade: {
        $max: '$grades.grade',
      },
    },
  },
  {
    $project: {
      grades: 0,
    },
  },
  {
    $out: 'longoni',
  },
]);
