type Comment = {
  name: string;
  email: string;
  movie_id: string;
  date: Date;
  eval: number;
};

type Award = {
  wins: number;
  nominations: number;
  text: string;
};

type Movie = {
  genres: string[];
  runtime: number;
  cast: string[];
  title: string;
  released: Date;
  directors: string[];
  awards: Award;
  year: number;
  countries: string[];
  type: string;
};

type DB = { movies: Movie[]; comments: Comment[] };

const db: DB = { movies: [], comments: [] };

// 1. Identify the title and the directors of the movies that got no more than
//    one nomination award and they were realized in the year 1893 or 1981 or
//    the actress "Farrah Fawcett" was a member of the cast ordered according
//    the year (in ascending order) and the type (in descending order).

// @ts-ignore
db.movies.aggregate([
  {
    $match: {
      $and: [
        { 'awards.nominations': { $lt: 2 } },
        {
          $or: [{ year: { $in: [1893, 1981] } }, { cast: 'Farrah Fawcett' }],
        },
      ],
    },
  },
  { $sort: { year: 1, type: -1 } },
]);

// 2. For each movie of type "series", released before 2000, associate the list
//    of comments that it has received only if the number of comments is greater
//    than 2.

// @ts-ignore
db.movies.aggregate([
  { $match: { type: 'series', year: { $lt: 2000 } } },
  {
    $lookup: {
      from: 'comments',
      localField: '_id',
      foreignField: 'movie_id',
      as: 'comments',
    },
  },
  { $unset: { comments: { $size: { $lte: 2 } } } },
]);

// 3. For each actor that participate to at least 5 movies of genre "Western",
//    return the number of movies with the title and directors of the movies.

// @ts-ignore
db.movies.aggregate([
  { $unwind: '$cast' },
  { $match: { genres: 'Western' } },
  {
    $group: {
      _id: '$cast',
      numberOfMovies: { $sum: 1 },
      directors: { $push: '$directors' },
    },
  },
]);

// 4. For each user that provided a comment, return the name and email of the
//    user, the commented movies with the indication of "positive" feedback if
//    the vote is above 5 and "negative" otherwise.

// @ts-ignore
db.comments.aggregate([
  {
    $project: {
      name: 1,
      email: 1,
      _id: 0,
      movie_id: 1,
      feedback: {
        $cond: {
          if: { $gt: ['$eval', 5] },
          then: 'positive',
          else: 'negative',
        },
      },
    },
  },
]);

// 5. Identify the titles of the movies that share the first actor of the cast
//    (the same pair of movies should be returned only once).

// @ts-ignore
db.movies.aggregate([
  {
    $lookup: {
      from: 'movies',
      localField: 'cast.0',
      foreignField: 'cast.0',
      as: 'movie',
    },
  },
]);
