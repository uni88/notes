# x10

## Workload

1. Insertion of new athletes and assignment of a fitness coach (among the
   physicians working for the medical center) and initial medical tests.

   Patient -> Athlete -> Physician -> MedicalTest

2. For each physician, identify the prescriptions of the last month.

   Physician -> Prescription

3. For each drug, identify the average number of prescription per month in the
   last year.

   Drug -> Prescription

4. For each physician, identify the number of athletes they coach per sport type
   and the number of medical tests aggregated for sport type.

   Physician -> Athlete -> Patient -> MedicalTest

## MongoDB

```json
// Patient

{
  "SSN": "",
  "name": "",
  "date_of_birth": "",
  "free_exams": "",
  "athlete": { "physicians": [], "sport_type": "" },
  "medical_tests": [
    {
      "date": "",
      "type": "",
      "outcome": "",
      "price": 1,
      "level": "",
      "physician_id": "",
      "request_date": "",
      "request_level": ""
    }
  ]
}
```

```json
// Physician

{
  "name": "",
  "specialization": [],
  "income": 0.0,
  "prescriptions": [
    { "code": "", "date": "", "total_cost": 0, "patient_id": "", "drugs": [] }
  ]
}
```

```json
// Drug

{ "name": "", "category": "", "price": 0 }
```

## Patterns

### Subset pattern - circa - for prescriptions

Since the `prescriptions` property in `physician` might get quite big, and since
we are anyway interested only in the prescriptions of the last month, we can ad
a `prescription_archive` collection, in which we store all the prescriptions.

```json
// Prescription archive

{
  "code": "",
  "date": "",
  "total_cost": 0,
  "patient_id": "",
  "drugs": [],
  "physician_id": 1
}
```

Inside the `prescription` property of the `physician` collection we will only
keep the prescriptions of the last month, thus solving the size scalability
problem. This requires us to change a bit the way the write operation works. We
have to write the prescription in both places, and delete older prescriptions
from the `physician` collection.

### Computed pattern for drugs

Computing the average amount of prescription per drug in the last year might be
quite slow. We can maintain a `drug_status` collection, in which we keep this
information.

```json
// Drug status

{
  "drug_id": "",
  "year": "",
  "monthly_average": ""
}
```

#### Index for patients

Since we are interested in indexing the patient collection by physician, it will
be beneficial to create an index on `patient.athlete.physicians`, and one on
`patient.medical_tests.physician`.

## JSON schema

```json
// Physician

{
  "$schema": "https://json-schema.org/draft-04",
  "title": "Physician",
  "description": "Describes a physician and the prescriptions he/she prescribed
    in the last month",
  "type": "object",
  "properties": {
    "name": {
      "description": "The physician's name",
      "type": "string",
      "minLength": 1
    },
    "specialization": {
      "type": "array",
      "minItems": 1,
      "uniqueItems": true,
      "items": {
        "description": "A physician's specialization",
        "type": "string",
        "minLength": 1
      }
    },
    "income": { "description": "A physician's income", "type": "decimal" },
    "prescriptions": [
      {
        "description": "A prescription for one or more drugs",
        "type": "object",
        "properties": {
          "code": { "description": "A prescriptions's code", "type": "string" },
          "date": {
            "description": "A prescription's prescription date",
            "type": "date"
          },
          "total_cost": {
            "description": "The total cost for drugs in this prescription",
            "type": "number"
          },
          "patient_id": { "description": "A patient's id", "type": "string" },
          "drugs": {
            "type": "array",
            "items": {
              "description": "A drug's id",
              "type": "string"
            },
            "minLength": 1
          }
        },
        "required": ["code", "date", "patient_id", "total_cost", "drugs"],
        "additionalProperties": false
      }
    ]
  },
  "required": ["name", "income", "specialization"],
  "additionalProperties": false
}
```
