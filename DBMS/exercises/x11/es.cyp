// 1. In which rooms does course with course number "1" take place in? Retrieve
//    the course name and the names of the rooms in which the course takes
//    place.
MATCH (c { courseNr: "1" })-[:TAKESPLACEIN]-(r)
WITH c, collect(r.roomName) AS rooms
RETURN c.courseName, rooms

// 2. How many hours and in which projects does student with student number "1"
//    works on? Retrieve the first name of the student, the project the student
//    works on and the corresponding number of hours worked on the project.
MATCH (s { studentID: "1" })-[r:WORKSON]-(p)
RETURN s.firstName, p.projectName, r.hours

// 3. Which students and how many hours do they work on the project with project
//    number "24"? Retrieve the project name, the last name of the student and
//    the corresponding number of hours worked on the project.
MATCH (p { projectNr: '24' })-[r:WORKSON]-(s)
RETURN p.projectName, s.lastName, r.hours

// 4. Which students work in which projects and how many hours? Retrieve the
//    last name of the students, the name of the projects they work on, and the
//    corresponding number of hours. Order the results by the last name of the
//    students. Limit the results to four.
MATCH (p:Project)-[r:WORKSON]-(s)
RETURN s.lastName, p.projectName, r.hours
 ORDER BY s.lastName
LIMIT 4

// 5. Which students work on more than two projects and on how many projects
//    exactly? Retrieve the last name of the students and the corresponding
//    number of projects. Order the results by the number of projects.
MATCH (p:Project)-[:WORKSON]-(s)
WITH s, count(p) AS projects
WHERE projects > 2
RETURN s.lastName, projects
 ORDER BY projects

// 6. Which students have the same last name and work on the same projects?
//    Retrieve the first name of the students and the name of projects they
//    share.
MATCH (s1)-[:WORKSON]-(p:Project)-[:WORKSON]-(s2)
WHERE s1.firstName <> s2.firstName AND s1.lastName = s2.lastName
RETURN s1.firstName, s2.firstName, p.projectName
