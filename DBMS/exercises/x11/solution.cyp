// 1. Identify the rooms the course number "1" take place in. Retrieve the
//    course name and the names of the rooms in which the course takes place.

MATCH (c:Course{ courseNr:'1' })-[:TAKESPLACEIN]->(r:Room)
RETURN c.courseName, r.roomName

// Alternative solution (in this solution for each course name, I return the
// list of rooms in which the course is taken)

MATCH (c:Course{ courseNr:'1' })-[:TAKESPLACEIN]->(r:Room)
RETURN c.courseName, collect(r.roomName)

// 2.  How many hours and in which projects does student with student number "1"
//     works on? Retrieve the first name of the student, the project the student
//     works on and the corresponding number of hours worked on the project.

MATCH (s:Student{ studentID:'1' })-[w:WORKSON]->(p:Project)
RETURN s.firstName, p.projectName, w.hours

// 3. Which students and how many hours do they work on the project with project
//    number "24"? Retrieve the project name, the last name of the student and
//    the corresponding number of hours worked on the project.

MATCH (s:Student)-[w:WORKSON]->(p:Project)
WHERE p.projectNr='24'
RETURN s.lastName, p.projectName, w.hours

// Pay attention on the fact that the query returns two identical rows, but they
// refer to two different students (Ana Doe and John Doe)

// 4. Which students work in which projects and how many hours? Retrieve the
//    last name of the students, the name of the projects they work on, and the
//    corresponding number of hours. Order the results by the last name of the
//    students. Limit the results to four.

MATCH (s:Student)-[w:WORKSON]->(p:Project)
RETURN s.lastName, p.projectName, w.hours
 ORDER BY s.lastName
LIMIT 4

// 5. Which students work on more than two projects and on how many projects
//    exactly? Retrieve the last name of the students and the corresponding
//    number of projects. Order the results by the number of projects.

MATCH (s:Student)-[w:WORKSON]->(p:Project)
WITH s, COUNT(p) AS no_of_projects
WHERE no_of_projects>2
RETURN no_of_projects, s.lastName
 ORDER BY no_of_projects

// 6. Which students have the same last name and work on the same projects?
//    Retrieve the first name of the students and the name of projects they
//    share.

MATCH (s1:Student)-[w:WORKSON]->(p:Project), (s2:Student)-[:WORKSON]->(p:Project)
WHERE s1.lastName=s2.lastName
RETURN p.projectName, s1.firstName, s2.firstName

// Alternative solution

MATCH (s1:Student)-[w:WORKSON]->(p:Project)<-[:WORKSON]-(s2:Student)
WHERE s1.lastName=s2.lastName
RETURN p.projectName, s1.firstName, s2.firstName
