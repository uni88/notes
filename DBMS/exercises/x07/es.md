# Second midterm

The Milan municipality takes track of the drug prescriptions (around 3000 a day)
made by its Physicians (around 600) to the citizens with special needs (around
300.000 and referred to as patients). Moreover, the municipality takes track of
the visits (around 2000 a day) and the drugs the citizens are allergic to. For
each drug, we store the name, the category, and the price at which the drug
should be sold. Drugs are sold in pharmacies and can be sold only when a
Physician prescribes them through a prescription. A prescription can contain one
or more drugs with the corresponding number of packages. A prescription clearly
identifies the drugs, the number of packages of each drug, physician, patient,
the date it is filled, and the name and address of the pharmacy where the
prescription needs to be presented. For patients that are allergic, we wish to
store the usual allergy symptoms and the list of drugs to which he/she is
allergic. For physicians, we store the habilitation number, their specialty, and
the phone numbers with their type (i.e. “home”, ”work”, “cell”). The conceptual
model of this domain is in the figure.

## Workload

1. Given a Pharmacy, identify the prescriptions that it serves and the
   prescribed drugs ordered according to the prescription dates and the drug
   costs (obtained by multiplying the price of a drug with the number of
   packages).

   Prescription -> Drug

2. Given a Physician, identify the prescriptions that he/she has issued and the
   number of visits to patients that are allergic to the insulin.

   Physician -> Prescription
   Physician -> Patient -> Drug

3. Given a patient identify his prescriptions that have been processed by the
   Pharmacy “Cold Drops”.

   Patient -> Prescription

4. Given a Pharmacy, identify the prescriptions of the last week that it serves
   and the category of the prescribed drugs.

   Prescription -> Drug

## MongoDB

```json
// Drug
{
  "name": "name",
  "category": "category",
  "picture": "picture",
  "price": 1
}
```

```json
// Prescription
{
  "pharmacy": "pharmacy",
  "address": {
    "streetName": "streetName",
    "streetNumber": 1,
    "city": "city"
  },
  "filledDate": "filledDate",
  "drugs": [{ "name": "name", "number": 1, "price": 1 }]
}
```

```json
// Physician
{
  "name": "name",
  "habilitation": 1,
  "specialties": ["specialty"],
  "contacts": [{ "kind": "kind", "number": "number" }],
  "visited": ["patient_id"],
  "prescribed": ["prescription_id"]
}
```

```json
// Patient
{
  "name": "name",
  "surname": "surname",
  "address": {
    "streetName": "streetName",
    "streetNumber": 1,
    "city": "city"
  },
  "symptoms": ["symptom"],
  "allergies": ["drug_id"]
}
```

## Cassandra

1.
2. | prescriptions_by_physician |
   | -------------------------- |
   | physician_name K           |

   | patients_by_physician |
   | --------------------- |
   | physician_name K      |
   | drug_name C           |

3. | prescriptions_by_patient |
   | ------------------------ |
   | patient_name K           |
   | pharmacy C               |

4. | prescriptions_by_pharmacy |
   | ------------------------- |
   | pharmacy K                |
   | filled_date C             |
   | drug_category             |
