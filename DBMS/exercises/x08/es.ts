type Coordinate = number[]; // [longitude, latitude]

type Address = {
  building: string;
  coord: Coordinate;
  street: string;
  zipCode: string;
};

type Grade = { date: Date; grade: String; score: number };

type BoroughName = string;

type Restaurant = {
  address: Address;
  borough: BoroughName;
  cuisine: string;
  grades: Grade[];
  name: string;
  restaurant_id: string;
};

type Borough = {
  name: BoroughName;
  male: number;
  female: number;
};

type Restaurants = { restaurants: Restaurant[]; boroughs: Borough[] };

const db: Restaurants = { restaurants: [], boroughs: [] };

// 1. Find the restaurant which is not in the borough Bronx

// @ts-ignore
db.restaurants.find({ borough: { $ne: 'Bronx' } });

// 2. Find the restaurant_id, name, borough, and cuisine for all the documents
//    in the collection restaurant (remove the object identifier)

db.restaurants.find(
  // @ts-ignore
  {},
  { _id: 0, restaurant_id: 1, name: 1, borough: 1, cuisine: 1 }
);

// 3. Find the restaurants that achieved a score in the set 65, 85,89
db.restaurants.find({
  // @ts-ignore
  grades: { $elemMatch: { score: { $in: [65, 85, 89] } } },
});

// 4. Find the restaurants that achieved the score 10
db.restaurants.find({
  // @ts-ignore
  'grades.score': 10,
});

// 5. Find the restaurants located in a latitude value less than -95

// @ts-ignore
db.restaurants.find({ 'address.coord.1': { $lt: -95 } });

// 6. Find the restaurants which do not prepare any cuisine of American and
//    achieved a score of more than 70 and located in the longitude less than
//    -65.754168
db.restaurants.find({
  // @ts-ignore
  cuisine: { $not: { $regex: 'American' } },
  'grades.score': { $gt: 70 },
  'address.coord.0': { $lt: -65.754168 },
});

// 7. Find the restaurants which belong to the borough Bronx, Queens or Brooklyn
//    and prepare either American or Chinese dish
// 8. Find the restaurant’s Id, name, and grades for those restaurants where the
//    2nd element of grades array contains a grade of "A" and score 9 on an
//    ISODate "2014-08-11T00:00:00Z"
// 9. Find the restaurant’s ID, name, address, and geographical location for
//    those restaurants where 2nd element of coord array contains a value which
//    is more than 42 and up to 52
// 10. Find the restaurant’s ID, name, address, and geographical location for
//     those restaurants presenting three grades
// 11. Check if all the restaurant addresses contain the street or not.
// 12. For each borough identify the number of restaurants that offer Chinese
//     dish
// 13. For each restaurant identifies the name of the restaurant and the number
//     of males and females that stay in the same borough of the restaurant
// 14. For each borough that presents at least 7000 males and 9000 females,
//     return the number of restaurants for each cuisine
// 15. For each restaurant identify the top grade that it has got in the
//     available evaluation (report the grade none in case it is not present)
//     and store this information in a collection (please use your last name as
//     name of the collection and remember to remove it when you have finished)
