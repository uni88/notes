# First midterm

## Constraints and assumptions

1. The vote timestamp has to be included in the radio's period of availability.
2. The number of song entities with that reference the same competition cannot
   be higher than that competition's maximum songs number.
3. The date property of the registered relationship has to be lower than the
   referenced competition's start date.
4. For two songs to be part of the registered relationship with the same
   competition, their singer has to be different.
5. The vote timestamp should be higher than the competition start date and lower
   than its end date.
6. The gender property of the vote entity has to be either male or female.
7. The vote property of the vote entity has to be between 1 and 5.
8. The competition genre has to be either pop, rock, hip hop, soul or funk.
9. The phone property of the radio entity has to be a valid phone number.
10. The date of birth property of authors end singers has to be lower than the
    start date of the competition their songs are part of.
