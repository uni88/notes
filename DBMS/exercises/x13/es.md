# x13

The conceptual data model reported in the figure for investment portfolio data
contains users, accounts, trades and instruments.

- A user has a unique username and may have other attributes like name.
- An account has a unique number, cash balance, investment and total value
- A trade is uniquely identified by an id and can be either a buy transaction or
  a sell transaction. Other trade attribute types include a trade date, number
  of shares, price per share and total amount.
- An instrument has a unique symbol and a current quote. Stocks, mutual funds
  and exchange-traded funds (ETFs) are all types of instruments supported in
  this example.

While a user can open many accounts, each account must belong to exactly one
user. Similarly, an account can place many trades and an instrument can
participate in many trades, but a trade is always associated with only one
account and one instrument. Finally, an account may have many positions and an
instrument can be owned by many accounts (we remark that we are exploiting the
notation of the ER diagram and not the UML notation). Each position in a
particular account is described by an instrument symbol, quantity and current
value. In the diagram, dashed lines are used for representing derived
attributes.

Describe the column families that need to be developed in order to properly
represent the following workload.

## Workload

1. Find information about all investment accounts for a user.
2. Find all positions in an account. Order by instrument symbol.
3. Find all trades for an account. Order by trade date - DESC.
4. Find all trades for an account and date range. Order by trade date - DESC.
5. Find all trades for an account, date range and transaction type. Order by
   trade date - DESC.
6. Find all trades for an account, date range, transaction type and instrument
   symbol. Order by trade date - DESC.
7. Find all trades for an account, date range and instrument symbol. Order by
   trade date - DESC.

## Cassandra

1. | investments_accounts_by_user |
   | ---------------------------- |
   | username K                   |
   | account_number C             |
   | cash_balance                 |
   | investment                   |
   | total_value                  |

2. | positions_by_account |
   | -------------------- |
   | account_number K     |
   | instrument_symbol C  |

3. | trades_by_account |
   | ----------------- |
   | account_number K  |
   | trade_date C DESC |
   | trade_id C        |

4. See 3.

5. | trades_by_account  |
   | ------------------ |
   | account_number K   |
   | transaction_type C |
   | trade_date C DESC  |
   | trade_id C         |

6. | trades_by_account   |
   | ------------------- |
   | account_number K    |
   | transaction_type C  |
   | instrument_symbol C |
   | trade_date C DESC   |
   | trade_id C          |

7. | trades_by_account   |
   | ------------------- |
   | account_number K    |
   | instrument_symbol C |
   | trade_date C DESC   |
   | trade_id C          |
