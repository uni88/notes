* Retrieve the title, released, AND tagline values of all the movies that were released IN 2003

* Retrieve all people who wrote the movie Speed Racer

MATCH (p:Person)-[:WROTE]->(:Movie { title: 'Speed Racer' })
RETURN p.name

*mat

MATCH (m:Movie)<--(:Person { name: 'Tom Hanks' })
RETURN m.title

* Retrieve all movies connected
WITH Tom Hanks AND the kind of relatiship

MATCH (m:Movie)-[rel]-(:Person { name: 'Tom Hanks' })
RETURN m.title, type(rel)

* Retrieve information about the roles that Tom Hanks played IN the movies

MATCH (m:Movie)-[rel:ACTED_IN]-(:Person { name: 'Tom Hanks' })
RETURN m.title, rel.roles

* Retrieve the actors who have acted IN exactly five movies, returning the name of the actor, AND the list of movies for that actor

MATCH (a:Person)-[:ACTED_IN]->(m:Movie)
WITH a, count(m) AS numMovies, collect(m.title) AS movies
WHERE numMovies = 5
RETURN a.name, movies

* Retrieve the movies that have at least 2 directors, AND optionally the names of people who reviewed the movies.

MATCH (m:Movie)
WITH m, size((:Person)-[:DIRECTED]->(m)) AS directors
WHERE directors >= 2
OPTIONAL MATCH (p:Person)-[:REVIEWED]->(m)
RETURN m.title, p.name

* Retrieve the actor
WITH the list of films they acted IN only if the list CONTAINS at least 5 film

MATCH (p:Person)-[:ACTED_IN]->(m:Movie)
WITH p, collect(m) AS movies
WHERE size(movies) > 5
RETURN p.name, movies

* retrieve the name of the movie, the name of the director, AND the names of actors that worked
WITH the actor Gene Hackman

MATCH (a:Person)-[:ACTED_IN]->(m:Movie)<-[:DIRECTED]-(d:Person),
(a2:Person)-[:ACTED_IN]->(m)
WHERE a.name = 'Gene Hackman'
RETURN m.title AS movie, d.name AS director , a2.name AS `co-actors`

* Identify the direct AND undirect followers of James Thompson

MATCH (p1:Person)-[:FOLLOWS*]-(p2:Person)
WHERE p1.name = 'James Thompson'
RETURN p1, p2

* Count the number of actors that were born IN the same year

* count the number od roles that an actors has done IN his career

* Identifiy the pair of actors that have played at least two times IN the same movies
