// 1. Retrieve the title, released, and tagline values of all the movies that
//    were released in 2003

MATCH (m:Movie { released: 2003 })
RETURN m.title, m.released, m.tagline

// 2. Retrieve all people who wrote the movie Speed Racer

MATCH (p)-[:WROTE]->({ title: 'Speed Racer' })
RETURN p

// 3. Retrieve all movies connected with Tom Hanks

MATCH (m:Movie)-[]-({ name:"Tom Hanks" })
RETURN m

// 4. Retrieve all movies connected with Tom Hanks and the kind of relatiship

MATCH (m:Movie)-[r]-({ name:"Tom Hanks" })
RETURN m, type(r)

// 5. Retrieve information about the roles that Tom Hanks played in the movies

MATCH (m:Movie)-[r:ACTED_IN]-({ name:"Tom Hanks" })
RETURN r.roles

// 6. Retrieve the actors who have acted in exactly five movies, returning the
//    name of the actor, and the list of movies for that actor

MATCH (p:Person)-[:ACTED_IN]-(m)
WITH p, collect(m.title) AS movies, count(m) AS numMovies
WHERE numMovies = 5
RETURN p, movies

// 7. Retrieve the movies that have at least 2 directors, and optionally the
//    names of people who reviewed the movies.

MATCH (p:Person)-[:DIRECTED]->(m)<-[:REVIEWED]-(r)
WITH m, collect(p) AS directors, collect(r.name) AS reviewers, count(p) AS numDirectors
WHERE numDirectors > 1
RETURN m, reviewers

// 8. Retrieve the actor with the list of films they acted in only if the list
//    contains at least 5 film

MATCH (p:Person)-[:ACTED_IN]-(m)
WITH p, collect(m) AS movies, count(m) AS numMovies
WHERE numMovies > 4
RETURN p, movies

// 9. retrieve the name of the movie, the name of the director, and the names of
//    actors that worked with the actor Gene Hackman

MATCH ({ name: 'Gene Hackman' })-[:ACTED_IN]-(m), (m)-[:ACTED_IN]-(a), (m)-[:DIRECTED]-(d)
WITH m, collect(a) AS actors, collect(d) AS directors
RETURN m, actors, directors

// 10. Identify the direct and undirect followers of James Thompson

MATCH ({ name: 'James Thompson' })-[:FOLLOWS*]-(p)
RETURN p

// 11. Count the number of actors that were born in the same year

MATCH (p:Person)
RETURN p.born, count(p)

// 12. count the number od roles that an actors has done in his career

MATCH (p:Person)-[r:ACTED_IN]-()
WITH p, count(r.roles) AS numRoles
RETURN p, numRoles

// 13. Identifiy the pair of actors that have played at least two times in the
//     same movies

MATCH (p1)-[:ACTED_IN]-(:Movie)-[:ACTED_IN]-(p2)
WITH p1, p2, count(*) AS moviesTogether
WHERE moviesTogether > 1
RETURN p1, p2, moviesTogether
