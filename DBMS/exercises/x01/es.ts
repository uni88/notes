type Analytics = {
  accounts: Account[];
  customers: Customer[];
  transactions: Transactions[];
};

type account_id = number;

type Account = {
  account_id: account_id;
  limit: number;
  products: string[];
};

type Customer = {
  username: string;
  name: string;
  address: string;
  birthDate: Date;
  email: string;
  accounts: account_id[];
};

type Transaction = {
  date: Date;
  amount: number;
  transaction_code: string;
  symbol: string;
  price: string;
  total: string;
};

type Transactions = {
  account_id: number;
  transaction_count: number;
  bucket_start_date: Date;
  bucket_end_date: Date;
  transactions: Transaction[];
};

const analytics: Analytics = { accounts: [], customers: [], transactions: [] };

const ISODate = (date: string) => date;

// 1. Identify the name and email of the costumers that were born in the period
//    1972-1982 and present a gmail email, or they hold at least 3 accounts.
//    [You have to use the $regex condition /gmail.com$/]

analytics.customers.find({
  // @ts-ignore
  $or: [
    {
      $and: [
        {
          birthDate: {
            $gte: ISODate('1972-01-01T00:00:00.000+00:00'),
            $lte: ISODate('1982-01-01T00:00:00.000+00:00'),
          },
        },
        { email: { $regex: '/gmail.com$/' } },
      ],
    },
    { 'accounts.2': { $exists: true } },
  ],
});

// 2. Identify the account number of the accounts that present a limit not in
//    the range 8000 and 10000

analytics.accounts.find(
  {
    // @ts-ignore
    $or: [{ limit: { $lt: 8000 } }, { limit: { $gt: 10000 } }],
  },
  { _id: 0, account_id: 1 }
);

// 3. Identify the transactions presenting a number of single transaction
//    between 10 and 80 and whose interval is between 1950 and 2020.

analytics.transactions.find({
  // @ts-ignore
  $and: [
    { transaction_count: { $gte: 10, $lte: 80 } },
    { bucket_start_date: { $gte: ISODate('1950-01-01T00:00:00.000+00:00') } },
    { bucket_end_date: { $lte: ISODate('2021-01-01T00:00:00.000+00:00') } },
  ],
});

// 4. Identify the transaction presenting single transaction for which the
//    amount is between 7000 and 8000 and transaction code is sell or buy or
//    symbol is "adbe"

analytics.transactions.find({
  // @ts-ignore
  transaction_count: 1,
  transactions: {
    $elemMatch: {
      amount: { $gte: 7000, $lt: 8000 },
      $or: [{ transaction_code: { $in: ['sell', 'buy'] } }, { symbol: 'adbe' }],
    },
  },
});

// 5. Return the name and address of the costumer presenting between 4 and 7
//    accounts

analytics.customers.find(
  {
    // @ts-ignore
    'accounts.3': { $exists: true },
    'accounts.7': { $exists: false },
  },
  { _id: 0, name: 1, address: 1 }
);

// 6. Return the name and address of the costumer presenting an account that
//    presents the product "Commodity"

// @ts-ignore
analytics.accounts.aggregate([
  { $match: { products: 'Commodity' } },
  {
    $lookup: {
      from: 'customers',
      localField: 'account_id',
      foreignField: 'accounts',
      as: 'customer',
    },
  },
  { $unwind: '$customer' },
  { $project: { _id: 0, 'customer.name': 1, 'customer.address': 1 } },
]);

// 7. For each account identify the total number of transactions that were
//    executed on it

// @ts-ignore
analytics.transactions.aggregate([
  {
    $group: {
      _id: '$account_id',
      number_of_transactions: { $sum: '$transaction_count' },
    },
  },
]);

// 8. For each product that can occur in an account, determine the number of
//    accounts presenting it

// @ts-ignore
analytics.accounts.aggregate([
  { $unwind: '$products' },
  { $group: { _id: '$products', number_of_accounts: { $sum: 1 } } },
]);

// 9. Identify the 10 most representative products associated with an account

// @ts-ignore
analytics.accounts.aggregate([
  { $unwind: '$products' },
  { $group: { _id: '$products', number_of_accounts: { $sum: 1 } } },
  { $sort: { number_of_accounts: -1 } },
  { $limit: 10 },
]);

// 10. Identify transactions presenting a single transaction in which the field
//     "total" is different from the product between "amount" and "price".

// @ts-ignore
analytics.transactions.aggregate([
  { $match: { transaction_count: 1 } },
  { $unwind: '$transactions' },
  {
    $match: {
      'transactions.total': {
        $ne: { $multiply: ['transactions.amount', 'transactions.price'] },
      },
    },
  },
]);

// 11. Identify the customers that present accounts for which no transactions
//     have been executed

// @ts-ignore
analytics.accounts.aggregate([
  {
    $lookup: {
      from: 'transactions',
      localField: 'account_id',
      foreignField: 'account_id',
      as: 'transactions',
    },
  },
  { $match: { transactions: { $size: 0 } } },
  {
    $lookup: {
      from: 'customers',
      localField: 'account_id',
      foreignField: 'accounts',
      as: 'customer',
    },
  },
  { $project: { _id: 0, customer: 1 } },
]);

// 12. For each customer associate the array "accountInfo". This array contains
//     the following info for each account the customer holds: <limit, products>

// @ts-ignore
analytics.customers.aggregate({
  $lookup: {
    from: 'accounts',
    localField: 'accounts',
    foreignField: 'account_id',
    pipeline: [{ $project: { _id: 0, limit: 1, products: 1 } }],
    as: 'accounts_info',
  },
});

// 13. For each transaction identify the single transaction that it contains
//     presenting the highest total

// @ts-ignore
analytics.transactions.aggregate([
  { $unwind: '$transactions' },
  { $group: { _id: '$_id', highest_total: { $max: '$transactions.total' } } },
]);

// 14. For each transaction_code identify the number of transactions that has
//     been executed with such code and the sum of the total associated with it

// @ts-ignore
analytics.transactions.aggregate([
  { $unwind: '$transactions' },
  {
    $group: {
      _id: '$transactions.transaction_code',
      count: { $sum: 1 },
      total: { $sum: '$transactions.total' },
    },
  },
]);

// 15. Identify the presence of transactions in which all the single
//     transactions present the symbol "msft"

analytics.transactions.find({
  // @ts-ignore
  transactions: { $not: { $elemMatch: { symbol: { $ne: 'msft' } } } },
});
