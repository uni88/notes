// 1. Identify the name and email of the costumers that were born in the period
//    1972-1982 and present a gmail email, or they hold at least 3 accounts.
//    [You have to use the $regex condition /gmail.com$/]

db.customers.find(
  {
    $or: [
      {
        birthdate: {
          $gte: ISODate('1972-01-01T00:00:00.000+00:00'),
          $lte: ISODate('1982-01-01T00:00:00.000+00:00'),
        },
        email: { $regex: '(?-i)gmail.com' },
      },
      { 'accounts.2': { $exists: true } },
    ],
  },
  { _id: 0, name: 1, email: 1 }
);

// 2. Identify the account number of the accounts that present a limit not in
//    the range 8000 and 10000

db.accounts.find(
  { $or: [{ limit: { $lt: 8000 } }, { limit: { $gt: 10000 } }] },
  { _id: 0, account_id: 1 }
);

// note that the simpler condition ("limit in the range 8000 and 10000") can be
// expressed as follows

db.accounts.find(
  { limit: { $gt: 8000, $lt: 10000 } },
  { _id: 0, account_id: 1 }
);

// 3 Identify the transactions presenting a number of single transaction between
// 10 and 80 and whose interval is between 1950 and 2020.
db.transactions.find({
  transaction_count: { $gte: 10, $lte: 80 },
  bucket_start_date: { $gte: ISODate('1950-01-01T00:00:00.000+00:00') },
  bucket_end_date: { $lte: ISODate('2020-01-01T00:00:00.000+00:00') },
});

// 4. Identify the transaction presenting single transaction for which the
//    amount is between 7000 and 8000 and transaction code is sell or buy or
//    symbol is "adbe"

db.transactions.find({
  transaction_count: 1,
  'transactions.0.amount': { $gte: 7000, $lte: 8000 },
  $or: [
    { 'transactions.0.transaction_code': { $in: ['sell', 'buy'] } },
    { 'transactions.0.symbol': 'adbe' },
  ],
});

// note that the corresponding simpler query "Identify the transaction
// presenting one of the single transaction for which the amount is between 7000
// and 8000 and transaction code is sell or buy or symbol is "adbe"" can be
// obtained with the following expression

db.transactions.find({
  'transactions.amount': { $gte: 7000, $lte: 8000 },
  $or: [
    { 'transactions.transaction_code': { $in: ['sell', 'buy'] } },
    { 'transactions.symbol': 'adbe' },
  ],
});

// note also that the semantics of the query evaluation does not guarantee you
// that the basic conditions hold for the same simple transaction

// 5. Return the name and address of the costumer presenting between 4 and 5
//    accounts

db.customers.find(
  { 'accounts.3': { $exists: true }, 'accounts.5': { $exists: false } },
  { _id: 0, name: 1, address: 1 }
);

// Note that If I want to retrieve the costumer with exactly one account I have
// to specify

db.customers.find(
  { 'accounts.0': { $exists: true }, 'accounts.1': { $exists: false } },
  { _id: 0, name: 1, address: 1 }
);

// Note finally that this kind of queries can be also answered by using an
// aggregatation pipeline (first we count the number of accounts per costumer
// and then filter those that do not respect the constraint)

// 6. Return the name and address of the costumers presenting an account that
//    presents the product "Commodity"
db.accounts.aggregate([
  {
    $match: {
      products: 'Commodity',
    },
  },
  {
    $lookup: {
      from: 'customers',
      localField: 'account_id',
      foreignField: 'accounts',
      as: 'customers',
    },
  },
  {
    $unwind: {
      path: '$customers',
    },
  },
  {
    $project: {
      _id: 0,
      name: '$customers.name',
      address: '$customers.address',
    },
  },
]);

// this pipeline is well thought and also quite efficient for solving the
// problem I propose another equivalent pipeline. Please try to evaluate the
// execution times of the two pipelines and look after the best one

db.costumers.aggregate([
  {
    $unwind: {
      path: '$accounts',
    },
  },
  {
    $lookup: {
      from: 'accounts',
      localField: 'accounts',
      foreignField: 'account_id',
      as: 'my_account_info',
    },
  },
  {
    $unwind: {
      path: '$my_account_info',
    },
  },
  {
    $unwind: {
      path: '$my_account_info.products',
    },
  },
  {
    $match: {
      'my_account_info.products': 'Commodity',
    },
  },
  {
    $group: {
      _id: '$_id',
      name: {
        $first: '$name',
      },
      adress: {
        $first: '$address',
      },
    },
  },
  {
    $project: {
      _id: 0,
    },
  },
]);

// 7. For each account identify the total number of transactions that were
//    executed on it
db.transactions.aggregate([
  {
    $group: {
      _id: '$account_id',
      count: {
        $sum: '$transaction_count',
      },
    },
  },
]);

// 8. For each product that can occur in an account, determine the number of
//    accounts presenting it
db.accounts.aggregate([
  {
    $unwind: {
      path: '$products',
    },
  },
  {
    $group: {
      _id: '$products',
      count: {
        $count: {},
      },
    },
  },
]);

// 9. Identify the 10 most representative products associated with an account

// In this query we have first to establish the meaning of "most representative
// products". We assume that the representativeness of a product corresponds to
// the number of accounts in which it is used" This can be realized by including
// a match stage in previous query or by using the $sortByCount stage as
// proposed in the following query.

db.accounts.aggregate([
  {
    $unwind: {
      path: '$products',
    },
  },
  {
    $sortByCount: '$products',
  },
  {
    $limit: 10,
  },
]);

// 10. Identify transactions presenting a single transaction in which the field
//     "total" is different from the product between "amount" and "price".
db.transactions.find({
  transactions: { $size: 1 },
  'transactions.0.total': {
    $ne: {
      $multiply: ['transactions.0.price', 'transactions.0.amount'],
    },
  },
});

// 11. Identify the customers that present accounts for which no transactions
//     have been executed

db.customers.aggregate([
  {
    $lookup: {
      from: 'transactions',
      localField: 'accounts',
      foreignField: 'account_id',
      as: 'transactions',
    },
  },
  {
    $match: {
      transactions: {
        $size: 0,
      },
    },
  },
]);

// 12. For each customer associate the array "accountInfo". This array contains
//     the following info for each account the customer holds: <limit, products>
db.customers.aggregate([
  {
    $lookup: {
      from: 'accounts',
      localField: 'accounts',
      foreignField: 'account_id',
      as: 'accountInfo',
      pipeline: [
        {
          $project: {
            _id: 0,
            limit: 1,
            products: 1,
          },
        },
      ],
    },
  },
]);

// 13. For each transaction identify the single transaction that it contains
//     presenting the highest total

// The solution considers the generation of an intermediate collection
db.transactions.aggregate([
  {
    $unwind: {
      path: '$transactions',
    },
  },
  {
    $group: {
      _id: '$_id',
      max: {
        $max: '$transactions.total',
      },
    },
  },
  {
    $out: 'maxTransaction',
  },
]);

db.transactions.aggregate([
  {
    $unwind: {
      path: '$transactions',
    },
  },
  {
    $lookup: {
      from: 'MaxTransactions',
      localField: '_id',
      foreignField: '_id',
      as: 'result',
    },
  },
  {
    $project: {
      tobeReturned: {
        $cond: {
          if: {
            $eq: ['$transactions.total', '$result.0.max'],
          },
          then: '1',
          else: '$$REMOVE',
        },
      },
    },
  },
  {
    $match: {
      tobeReturned: {
        $exists: true,
      },
    },
  },
]);

// 15 Identify the presence of transactions in which all the single transactions
// present the symbol "team"

db.transactions.find({
  $expr: {
    $eq: [
      {
        $size: {
          $setDifference: ['$transactions.symbol', ['team']],
        },
      },
      0,
    ],
  },
});

db.transactions.find({
  $expr: {
    $setEquals: ['$transactions.symbol', ['team']],
  },
});
