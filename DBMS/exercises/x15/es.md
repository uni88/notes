# x15

We want to create an XML database for storing MP3 files that are downloaded from
the Web. Each MP3 file is identified by a unique code (the URL in which it is
located), along with its size in KB. Furthermore, each MP3 file refers to one
and only one piece of music (while a piece of music of interest to the
application is stored in at least one MP3 file, and can be stored in more than
one MP3 file). Each piece of music is identified by a code and is characterized
by the title, the duration in minutes, and by the authors (at least one), by the
year in which it was produced, and by the main singer (one and only one). Each
singer is under contract with a record company. A singer can be a single person
or a band. For each person, we are interested in storing the first name, the
last name, the artistic name, the date of birth, and gender, if any. For each
band, we are interested in storing its name, the year in which it was
constituted, and the members that belonged to the band over the years (actually
we wish to store the initial and final year in which a singer has been a member
of the band).

## MongoDB

```json
// MP3
{
  "url": "www.example.com",
  "size": 2000,
  "song_id": 1
}
```

```json
// Songs
{
  "code": 1,
  "duration": 180,
  "title": "I can't hear from this ear",
  "year": 1996,
  "authors": ["Mom", "Dad"],
  "singerType": "SinglePerson",
  "singer": 1
}
```

```json
// Band
{
  "id": 1,
  "name": "Me",
  "activeSince": "1993-12-30",
  "members": [
    { "singer_id": 1, "memberSince": "2019-01-18", "memberUntil": "2022-06-21" }
  ]
}
```

```json
// SinglePerson
{
  "id": 1,
  "firstName": "Simone",
  "lastName": "Guzzi",
  "artisticName": "Menno",
  "dateOfBirth": "1993-12-27",
  "gender": "M",
  "recordCompanies": ["This", "That"]
}
```

## Patterns

```json
// MP3
{
  "url": "www.example.com",
  "size": 2000,
  "song_id": 1
}
```

```json
// Songs
{
  "code": 1,
  "duration": 180,
  "title": "I can't hear from this ear",
  "year": 1996,
  "authors": ["Mom", "Dad"],
  "singerType": "SinglePerson",
  "singer": 1
}
```

```json
// Band
{
  "id": 1,
  "name": "Me",
  "activeSince": "1993-12-30",
  "members": [
    { "singer_id": 1, "memberSince": "2019-01-18", "memberUntil": "2022-06-21" }
  ]
}
```

```json
// SinglePerson
{
  "id": 1,
  "firstName": "Simone",
  "lastName": "Guzzi",
  "artisticName": "Menno",
  "dateOfBirth": "1993-12-27",
  "gender": "M",
  "recordCompanies": ["This", "That"]
}
```
