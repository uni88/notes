# x05

## Workload

1. Find articles published in a conference with a given name after a given year.
   Order results by year (DESC).

   Conference -> Article[^year]

2. Find articles published by a given author. Order results by year (DESC).

   Scientist -> Article[^year]

3. Find Scientists who liked a given article.

   Article -> Scientist

4. Find Scientists who liked a given article and who have expertise in a certain
   area.

   Article -> Scientist

5. Find an average rating of a given article.

   Article -> Review

6. Find conferences that a given Scientist participate.

   Scientist -> Conference

7. Find articles published after a certain year that a given Scientist liked.
   Order results by year.

   Scientist -> Article[^year]

8. Find reviews posted by a given Scientist with rating >=x. Order results by
   rating.

   Scientist -> Review

9. Find information about an article with a given id.

   Article

[^year]:
    Since each article appears only once in the `present` relationship, we
    can include the conference year in the article.

## MongoDB

```json
// Conference
{
  "...": "Conference data",
  "articles": [1, 2, 3]
}
```

```json
// Article
{
  "...": "Article data",
  "year": "year of the conference",
  "authored_by": [1, 2, 3],
  "liked_by": [1, 2, 3],
  "reviews": [
    {
      "id": "id",
      "rating": 0
    }
  ]
}
```

```json
// Scientist
{
  "...": "Scientist data",
  "participated_in": [1, 2, 3],
  "liked": [1, 2, 3],
  "authored": [1, 2, 3],
  "reviews": [
    {
      "...": "Review data",
      "post": "2023-01-01",
      "scientist": 1
    }
  ]
}
```

## Cassandra

1. Find articles published in a conference with a given name after a given year.
   Order results by year (DESC).

   | articles_by_conference |
   | ---------------------- |
   | conference_name K      |
   | conference_year C DESC |
   | article_id C           |

2. Find articles published by a given author. Order results by year (DESC).

   | articles_by_author |
   | ------------------ |
   | scientist_id K     |
   | year C DESC        |
   | article_id C       |

3. Find Scientists who liked a given article.

   | scientists_by_liked_article |
   | --------------------------- |
   | article_id K                |
   | scientist_id C              |

4. Find Scientists who liked a given article and who have expertise in a certain
   area.

   | scientists_by_liked_article_and_expertise |
   | ----------------------------------------- |
   | article_id K                              |
   | expertise C                               |
   | scientist_id C                            |

5. Find an average rating of a given article.

   | average_rating |
   | -------------- |
   | article_id K   |
   | review_id C    |
   | rating         |

6. Find conferences that a given Scientist participate.

   | conferences_by_participant |
   | -------------------------- |
   | scientist_id K             |
   | name C                     |

7. Find articles published after a certain year that a given Scientist liked.
   Order results by year.

   | articles_by_liking_scientist |
   | ---------------------------- |
   | scientist_id K               |
   | year C                       |
   | article_id C                 |

8. Find reviews posted by a given Scientist with rating >=x. Order results by
   rating.

   | reviews_by_scientist |
   | -------------------- |
   | scientist_id K       |
   | rating C             |
   | review_id C          |

9. Find information about an article with a given id.

   | articles |
   | -------- |
   | id       |

## Neo4J

Node Labels:

- Conference
- Article
- Scientist
- Review

Relationship Labels:

- Participates
- Presents
- Authors
- Likes
- Rates
