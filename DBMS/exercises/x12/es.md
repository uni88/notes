# x12

A big company that is organized in departments presents a legacy relational
database like the one proposed in the figure. Each department is located at a
given address and has associated a manager. For each employee, we store his
personal information, contact information, compensation voices, the department
in which he/she works on, and the reference to the manager. Finally, for each
employee we maintain the previous jobs in a given department of the company. The
company needs to move to a Mongodb database for storing such information and
make available the following operations.

## Workload

1. For each department identify the current number of employees, the name of the
   manager, the list for each year of the top 10 employees with the highest
   salary, and its address.

   Department -> Employee
   Department -> Location
   Job History -> Employee

2. For each employee the number of departments in which he/she has worked for,
   and the list of his managers for each previous job.

   Employee -> Job History
   Job History -> Employee

3. The information about the salary and the contact details are considered
   confidential and need to be protected differently from the other employee
   information.

## MongoDB

```json
// Department
{
  "name": "Research and Development",
  "managerId": "001",
  "location": {
    "streetName": "Main Road",
    "postalCode": "00000",
    "city": "Saffron City",
    "province": "Kanto",
    "country": "Japan"
  },
  "employees": ["001", "002"]
}
```

```json
// Employee
{
  "firstName": "Ash",
  "lastName": "Ketchum",
  "jobHistory": [
    {
      "startDate": "1995-01-01",
      "endDate": "2022-01-01",
      "managerId": "001",
      "departmentId": "001"
    }
  ]
}
```

```json
// Employee information
{
  "employeeId": "002",
  "email": "ash@silph.jp",
  "phoneNumber": "333444555666",
  "salary": "75000",
  "bonus": "1000"
}
```

## MongoDB with patterns

```json
// Department
{
  "name": "Research and Development",
  "manager": { "_id": "001", "name": "Giovanni" },
  "location": {
    "streetName": "Main Road",
    "postalCode": "00000",
    "city": "Saffron City",
    "province": "Kanto",
    "country": "Japan"
  }
}
```

```json
// Department status
{
  "department_id": "001",
  "numberOfEmployees": 2,
  "yearlyHighestSalaries": { "year": 1996, "employees": ["001", "002"] }
}
```

```json
// Employee
{
  "firstName": "Ash",
  "lastName": "Ketchum",
  "jobHistory": [
    {
      "startDate": "1995-01-01",
      "endDate": "2022-01-01",
      "managerId": "001",
      "departmentId": "001"
    }
  ]
}
```

```json
// Employee status
{
  "employee_id": "001",
  "numberOfDepartments": 1,
  "managers": ["001"]
}
```

```json
// Employee confidential information
{
  "employeeId": "002",
  "email": "ash@silph.jp",
  "phoneNumber": "333444555666",
  "salary": "75000",
  "bonus": "1000"
}
```
