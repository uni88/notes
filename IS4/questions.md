# Questions

1. Nowadays, the usage of classical feature extraction and data analysis methods
   is outdated since the capability of the recent deep learning models and
   methods made them obsolete and not more present in the common practice.

   - [x] True
   - [ ] False

2. Artificial Intelligence can be applied to the following sectors.

   - [ ] Robotics
   - [ ] Information extraction
   - [x] All the above

3. Artificial neural networks are capable to learn human biases.

   - [ ] False: the achievable complexity of the artificial neural networks is
         so far from the complexity of the human brain to make impossible to
         mimic this characteristic
   - [ ] False: human biases are not reproducible nor measurable
   - [x] True

4. Recent artificial intelligence models can solve analogy puzzles like “Paris
   is to France as Tokyo is to ?” producing the correct answer “Japan”.

   - [x] True
   - [ ] False

5. Considering the “Data knowledge spectrum plot” discussed in class, the
   minimum amount of data required is in the following case.

   - [ ] No knowledge about the model generating the data is available
   - [ ] A statistical model of the process is available
   - [x] A mathematical model of the process is available

6. It is possible to think to the single datum in input to the neural network as
   a point in the “input space” of the model, even if the input is a single
   value, a N dimensional vector, or an image

   - [x] True
   - [ ] False

7. It is correct to say the one of the key feature of an intelligent artificial
   system is the capability to learn (even if only a limited sense) and/or get
   better in time.

   - [x] True
   - [ ] False

8. According to Engelbrecht's definition of Computational intelligence, which of
   the following is not included?

   - [ ] Artificial Neural Networks
   - [ ] Evolutionary Computing
   - [ ] Swarm Intelligence
   - [ ] Artificial immune system
   - [ ] Fuzzy Systems
   - [x] All the above are included

9. According to the class discussion of the Gestalt capability, what of the
   following sentences is more correct?

   - [ ] The Gestalt capability is a typical feature present by-design in the
         model of classical neural networks
   - [ ] The Gestalt capability is a typical feature present by-design in the
         model of deep learning neural networks
   - [x] The Gestalt capability is a typical human feature not well (yet)
         mimicked in current artificial networks

10. The following activity: Data Selection, Data Filtering, Data Enhancing:

    - [ ] Are part of the job of the artificial intelligent specialist in normal
          activities
    - [ ] Contribute to keep lower the complexity of the learning task
    - [x] All the above (1. and 2.)
    - [ ] Are part of the classical machine learning approaches and they are
          (correctly) no longer used in deep learning applications

11. The Mean Squared Error is typically present in which step of the design?

    - [ ] Representation
    - [x] Evaluation

12. Considering IoT devices as source of data for external intelligent systems
    (IS is not intended to be embedded into the IoT device), what kind of IoT
    devices can be really used?

    - [ ] Passive data IoT devices
    - [ ] Active data IoT devices
    - [ ] Dynamic data IoT devices
    - [x] All of the above
    - [ ] None of the above

13. Referring to the class discussion, the (correct) design practice for neural
    networks considers

    - [ ] Start with deep learning models since they are the cutting edge and
          most advanced technology we have now
    - [ ] Start with deep learning models since they are the cutting edge and
          most advanced technology we have now, and then use classical method as
          reference
    - [x] Start with simple neural networks before to consider deep learning
          models

14. The missing values can also be occupied by computing mean, mode or median of
    the observed given values.

    - [ ] This is very unusual and not common in practice
    - [x] This is a very simple and effective solution in case the learning
          method is not capable to deal with missing data
    - [ ] This is not possible, since that is just descriptive statistics about
          the features, and cannot be used to fill missing data

15. Referring to the class discussion on data leakage what is the worst
    situation?

    - [x] The unwanted leakage of data from test dataset to training data set
    - [ ] The unwanted leakage of data from training dataset to test data set
    - [ ] None of the above since transferring data from test and/or training
          dataset is normal when the accuracy of the model is tested

16. An additional information can allow the model to learn or know something
    that it otherwise would not know and in turn invalidate the estimated
    performance of the model being constructed. This is called

    - [x] Data leakage
    - [ ] Data pre-processing
    - [ ] Data harmonization
    - [ ] Data wrangling

17. The degrees of freedom for a given problem are the number of independent
    problem variables which must be specified to uniquely determine a solution.
    Hence the #DoF is important to be considered

    - [ ] To design the number of vectors in the learning dataset.
    - [ ] To avoid over-fitting problem in the model
    - [x] All the above
    - [ ] None of the above

18. About the cosine metrics it is possible to say that

    - [ ] Two vectors with the same orientation have a cosine similarity of 1
    - [ ] Two vectors oriented at 90° relative to each other have a similarity
          of 0
    - [x] All of the above
    - [ ] None of the above

19. What similarity feature/features discussed in class offers/offer the
    property to allow a fast comparison based on a short 1D vector of elements
    or bits

    - [ ] p-hash
    - [ ] a-hash
    - [x] All the above
    - [ ] Cross-correlation

20. In agreement to the class discussion, which description better describes the
    design activity?

    - [ ] Similarity in the dataset requires more space and processing time
    - [ ] Similarity in the dataset can improve generalization
    - [x] Both of the above
    - [ ] None of the above

21. In agreement to the class discussion, in a dataset of 1100 labelled images,
    the search for duplications is typically achieved…

    - [ ] by manual exploration of the dataset for better results since the
          number of images is not critical.
    - [x] by automatic iterations

22. In agreement to the class discussion, what kind of labelling error is
    generally the worst case for the accuracy of the generalization of the
    model? ERR1 = Duplications with same labels, EER2 = Duplications with
    different labels

    - [ ] ERR1
    - [x] ERR2
    - [ ] ERR1 = EE2

23. According to the class discussion, about the relationship between the
    operation of cross-correlation and convolution it is possible to say that:

    - [x] They are very similar in meaning and mathematical expression
    - [ ] Despite the mathematical expression is similar, the meaning and their
          use is completely different
    - [ ] There is no specific relationship since they are different in meaning
          and mathematical expressions

24. According to the class discussion, what is the characteristic of the
    self-correlation $O=xCor2(A,A)$ map produced by a generic image?

    - [ ] A flat and noisy central plateau
    - [x] An evident spike at the center with a very well defined maximum
    - [ ] It is not possible to create an autocorrelation map from one single
          images, two different images are needed

25. If your data set contains extreme outliers it better to use as preprocessing

    - [x] Feature clipping
    - [ ] Min-max normalization
    - [ ] Z’ norm

26. A logarithmic scaling to one feature values is typically applied in a case
    of

    - [ ] Outliers presence
    - [ ] Negative values
    - [x] A very large range in the values (>0)

27. According to the scientific visualization rules presented in class, if you
    are plotting many figures of merit obtained by your trained neural network
    on a new dataset, which is the correct ranking of visual attributes to be
    used? Left: low accuracy Right: HIGH ACCURACY

    - [ ] Color intensity > Hue > Length
    - [ ] Area > Length > Hue
    - [ ] Slope > Angle > Volume
    - [x] Hue > Area > Length

28. According to the scientific visualization rules presented in class, is it
    possible to plot a graphical representation of the confidence level of your
    figures of merit of your trained model?

    - [ ] No, it is a statistical index with different units and meaning and
          hence can not be represented in the same plot
    - [x] Yes, the confidence interval data have the same units and meaning and
          they can be represented in the same plot

29. According to the discussion presented in class about the data visualization,
    and considering the following steps of the design workflow

    1. Get Data
    2. Clean Manipulate Data
    3. Train models
    4. Test Data
    5. Improve the design

    which are the main step/steps where data visualization should be involved?

    - [ ] #1
    - [ ] #5
    - [ ] #1 and #5
    - [x] #2, #3 and #5

30. According to the discussion presented in class about the similarity,
    Consider an image A(x,y) with internal similarity (repetitions of patterns).
    What happens to the output of the self cross-correlation $O = xCorr2(A,A)$

    - [ ] It is not possible to apply the cross-correlation to the same image
    - [ ] Output O tends to be a flat plateau with one clear central peak
    - [x] Output O tends to have many peaks and one evident maximum
    - [ ] Output O tends to have many equivalent peaks with the same maximum
          value
