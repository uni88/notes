# Intelligent systems for industry, supply chain and environment

## Table of contents

- [Intelligent systems for industry, supply chain and environment](#intelligent-systems-for-industry-supply-chain-and-environment)
  - [Table of contents](#table-of-contents)
  - [Trained neural networks](#trained-neural-networks)
    - [Neurons](#neurons)
    - [Neural networks](#neural-networks)
    - [Artificial neural networks](#artificial-neural-networks)
    - [Multi-layer neural networks](#multi-layer-neural-networks)
    - [Back-propagation](#back-propagation)
    - [Deep neural networks](#deep-neural-networks)
      - [Auto-encoders](#auto-encoders)
      - [Transfer learning](#transfer-learning)
      - [Fine-tuning](#fine-tuning)
      - [Designing and training](#designing-and-training)
    - [Convolutional neural networks - CNN](#convolutional-neural-networks---cnn)
  - [AI](#ai)
    - [XAI - explainable artificial intelligence](#xai---explainable-artificial-intelligence)
    - [Gestalts](#gestalts)
    - [Artificial intelligent systems](#artificial-intelligent-systems)
    - [Perception](#perception)
    - [Drivers](#drivers)
    - [Deductive vs inductive](#deductive-vs-inductive)
  - [Intelligent sensors](#intelligent-sensors)
  - [Machine learning](#machine-learning)
    - [Algorithms structure](#algorithms-structure)
    - [Taxonomy](#taxonomy)
    - [Design workflow](#design-workflow)
      - [Data gathering](#data-gathering)
      - [Data preparation](#data-preparation)
        - [Handling missing data](#handling-missing-data)
        - [Structured vs unstructured data](#structured-vs-unstructured-data)
        - [Data leakage](#data-leakage)
        - [Labelling errors](#labelling-errors)
        - [Similarity](#similarity)
        - [Exploratory data analysis and data visualization](#exploratory-data-analysis-and-data-visualization)
        - [Feature engineering](#feature-engineering)
      - [Train the model](#train-the-model)
        - [Testing the tool](#testing-the-tool)
        - [Encoding](#encoding)
        - [Prepare the input features](#prepare-the-input-features)
        - [Dataset balancing](#dataset-balancing)
        - [Dataset partition](#dataset-partition)
        - [Clustering](#clustering)
        - [Decision trees](#decision-trees)
      - [Test the model](#test-the-model)
        - [Accuracy/error assessment](#accuracyerror-assessment)
        - [Bayes optimal classification](#bayes-optimal-classification)
        - [Curse of dimensionality](#curse-of-dimensionality)
        - [Classical methods - non neural](#classical-methods---non-neural)
      - [Working with a new model](#working-with-a-new-model)
    - [Choosing the right technique](#choosing-the-right-technique)
    - [Hardware](#hardware)
    - [Software](#software)
  - [Digital ages](#digital-ages)
  - [Edge, fog, cloud](#edge-fog-cloud)
  - [Internet of things - IoT](#internet-of-things---iot)
    - [Examples](#examples)
    - [Security](#security)
    - [Artificial intelligence of things - AIoT](#artificial-intelligence-of-things---aiot)
    - [Use cases](#use-cases)
  - [Evolutionary computing](#evolutionary-computing)
  - [Fuzzy systems](#fuzzy-systems)
  - [Agents](#agents)
  - [Hybrid intelligent systems](#hybrid-intelligent-systems)
  - [Intelligent vision](#intelligent-vision)
    - [Image classification](#image-classification)
    - [Object detection](#object-detection)
    - [Object measurement](#object-measurement)
    - [Object segmentation](#object-segmentation)
    - [Intelligent systems for robotics](#intelligent-systems-for-robotics)
  - [Industry 4.0](#industry-40)

## Trained neural networks

### Neurons

Neural networks are based on the human neural system. The real strength of the
human neural system, rather than the number of neurons, is the number of
connections between them.

![human vs machine](./assets/images/human_machine.png)

Neural networks are capable of learning from experience and mistakes, of
generalizing from examples and of being quite independent from domain knowledge.
They are very adaptable, computationally efficient and capable of dealing with
non-linear problems.

The neuron is the basic computing element. The function which is inside the
neuron can change based on the behavior we want. Since back-propagation is based
on the gradient, the fact that a function is differentiable or not is a big
deal.

![neuron function](./assets/images/neuron_function.png)

The simplest neuron we can create is the perceptron. It's just a weighted sum
between two inputs followed by a comparison with a threshold. This means there
are three parameters: the two weights and the threshold.

![neuron](./assets/images/neuron.png)

The aim of the perceptron is two classify the input into two classes. The
decision plane is divided into two regions by a hyperplane.

![linear separability](./assets/images/linear_separability.png)

For a perceptron, learning means making small adjustments to the weights to
reduce the difference between the output $y$ and the desired output. The initial
values for the weights and the threshold are random in the range $[-0.5;0.5]$.

An iteration $p$ is the $p^{th}$ training example presented to the perceptron.
Each iteration $p$ produces an output $Y(p)$, with an error $e(p)$. If the error
is positive, we need to increase the perceptron output, and vice-versa. The
wights are adjusted using the error, the input and a learning rate $\alpha$: a
positive constant less than unity. This is called the $\Delta$ rule.
$\Delta*w_{i}(p)=\alpha*x_{i}(p)*e(p)$.

A linear decisor is obviously limited - i.e. it can learn an and, an or, but non
a xor.

### Neural networks

A trained neural network is just a non-linear function: it takes an input, and
returns an output - $Out=F(X)$. A neural network is measuring, not thinking. The
processing is performed layer after layer, with each layer changing the input
space.

![neural network](./assets/images/neural_network.png)

The _input space_ can be composed of anything. Every element is treated as a
point in the _feature space_: a $N$-dimensioned space with one axis/dimension
per-feature. Each neuron/layer will try to move each point towards the correct
section of the feature space. The latest neuron/layer has to take the final
decision: on which side of each _decision boundary_ to put each point. These
decision boundaries have to be determined in the process: this is part of the
exploring phase.

![decision boundaries](./assets/images/decision_boudnaries.png)

Working in a layered fashion can cause small distortions or noise to produce
errors.

Neural networks for pattern matching work in two phases: training and testing.

![nn training testing](./assets/images/nn_training_testing.png)

### Artificial neural networks

Used mainly for pattern recognition, clustering, function approximation and time
series prediction.

A feed forward layer is a layered structure. The first layer is the input layer,
the internal layers are called hidden layers and the last layer is the output
layer.

A radial basis function is the function that a neuron applies to compare the
input against a prototype. Training a radial basis network is easy, as only the
correct prototypes have to be learned.

![rbf network](./assets/images/rbf_network.png)

To train a neural network, we provide the input, compute the output, and, if
needed, back-propagate the error to tweak the parameters.

![nn training](./assets/images/nn_training.png)

### Multi-layer neural networks

It has been demonstrated that multi-layer neural networks can perfectly
approximate any function, provided they have enough neurons.

Using more than one neuron allows to produce more than one output, which is
important because it allows to convey confidence, rather than just a binary
output - yes/no.

A hidden neuron/layer is any neuron/layer that is neither input nor output. The
number of hidden layers can't grow arbitrarily. Commercial networks have 3/4
layers, experimental networks have 5.

Multi-layer neural networks learn in the same way as the perceptrons: with
back-propagation.

### Back-propagation

The weights and thresholds of each neuron $i$ are initialized to random numbers.
These numbers are uniformly distributed in a way that depends on the total
number of inputs of neuron $i$.

Then, the back-propagation works in two phases:

1. A training input is presented to the input layer. The input is propagated
   from layer to layer until the output is generated by the output layer.
2. If the produced output is different from the desired one, the error is
   calculated and propagated backwards through the network to the input layer.
   The weights are modified by applying the $\Delta$ rule. The weight correction
   will be proportional to the gradient of the function of the neuron. This
   should be an incentive to choose neuron functions for which the gradient is
   easy to compute.

This is done for every iteration, until the error criterion is satisfied, or if
the maximum number of epochs is reached, or if the gradient is null - and
therefore the error won't be changing anymore.

### Deep neural networks

A deep neural network is a neural network with many hidden layers. Deep neural
networks are able to autonomously process and classify images. This wasn't
always the case: a human had to manually extract features to give as input to
the network. Still, not every pixel is connected to a neuron. Features are still
extracted using filters that emphasize different features.

Most of the time, deep learning needs high amounts of data, to match the high
number of parameters with a high amount of vectors. This can be addressed by
data augmentation: replicating real data into multiple copies by rotating,
translating and scaling it. Generating adversarial networks are capable of
generating images from random inputs and they are very useful to perform data
augmentation.

Deep learning is a class of models and algorithms. The idea is to use different
non linear hierarchical layers, with feature extraction and transformation. Both
supervised and unsupervised learning can take advantage of deep learning.

High level features - i.e. a face - are derived from the low level features -
i.e. a set of corners -, creating a hierarchical description of the input. The
first layers of the network replace the manual job of the feature engineer. This
needs a lot of data, a lot of time, and a lot of resources, which means that not
all applications can use this techniques. Nonetheless, this is useful in a lot
of scenarios - i.e. when we are not experts of the domain.

The Hastad proof demonstrates that using more layers decrease the total number
of neurons needed to solve a problem. Highly varying, non-linear functions can
be efficiently represented with deep learning. Additionally, some sub-features
can be shared between multiple tasks.

#### Auto-encoders

Auto-encoders are unsupervised deep learning models that can be used to improve
the learning of convolutional neural networks. In convolutional neural networks,
it's easy to use back-propagation in the fully connected section, but it's very
difficult to do so in the previous part of the network, where the gradient is
exploding or vanishing. That's where auto-encoders can be useful.

The idea is to create generic features from data - encoding. By forcing the
input through a bottleneck, and expecting an output which is equal to the input,
we make it so the hidden layer is extracting features from data.

![auto-encoder](./assets/images/autoencoder.png)

By stacking multiple auto-encoders together, we can extract even more
sub-features. Then, we can use supervised layers to fine tune the wights and
have a classifier.

By doing this, we are able to summarize an image into very few features.
Auto-encoders are also very good at reducing noise and reconstructing the input.

#### Transfer learning

Useful to avoid over-fitting. Usually models have to be trained from scratch
when changing their domain. When it comes to deep learning, tho, the first
layers are feature extractors, which are general. We can try to transfer some
knowledge from one model to another. Shared models can be found in so-called
model zoos.

#### Fine-tuning

Fine-tuning consists in fixing some layers of the network, and allow only some
other layers to change.

#### Designing and training

There is no sure heuristic on how to design the topology of the network. Most of
the time it's about trial and error. It is a best practice to decrease the
number of kernel when moving from left to right.

When training, we should initialize the network with random weights, use weight
tying[^weight_tying], or use pre-trained models.

While training deep networks using back-propagation, early layers don't get
trained so well. They end up being almost random feature maps. It's also very
slow to train this kind of networks using back-propagation. To fix these
problems, we can use unsupervised learning as initialization. We can initialize
every layer by using auto-encoders. This is called greedy layer-wise training.

![greedy layer wise](./assets/images/greedy_layer_wise.png)

[^weight_tying]:
    share the weight matrix between input and embedding layer, and
    between output and softmax layer. This is proven to help combat
    over-fitting.

### Convolutional neural networks - CNN

A convolutional neural network is a neural network that has some convolutional
layers. A convolutional layer uses a number of filters to perform convolutional
operations. Every filter detects a small pattern.

The weights of traditional neural networks are replaced by filters. Learning
consists of tuning every element of every filter.

![convolutional filter](./assets/images/convolutional_filter.png)

The result is a feature map, which describes how the image responds to a certain
filter. The feature map is smaller than the original image, depending on the
size of the filter.

Some computation can be reused for adjacent areas of the image. This is because
the kernel elements are effectively shared weights.

![shared weights](./assets/images/shared_weights.png)

To simplify the learning, elements of the feature maps that are negative or
close to zero are forced to zero. This technique is called rectified linear unit
and it's based on the fact that we are interested only in the parts of the image
that respond to the feature. Different functions can be used as a transfer
function: the function that determines if a value should be set to zero.

With respect to fully connected networks we are saving a lot of computation and
we can use pooling operations, to furthermore reduce the computation needed.
Pooling is a sub-sampling technique, based on the fact that we can obtain the
same feature map even by reducing the size of the samples, so that we need less
parameters to describe it. This also decreases the importance of the exact
location of features, focusing more on the rough location, thus making data
invariant to small transitional changes. We always extract the maximum, because
we are interested in what responds to the filter.

![pooling](./assets/images/pooling.png)

Flattening merges channels together.

![flattening](./assets/images/flattening.png)

Convolutional neural network are an iteration of convolution layers and pooling
layers, with a flattening layer at the end. The end result is given as input to
the fully connected neural network.

The last layer of the fully connected network is a SoftMax: every single output
is firing if the input is similar to the class the output represents. Since
every output represent a confidence, we can draw some conclusion based on the
number of output that are firing - i.e. if only one output is high the network
is quite sure about the result.

A nice high-level tool for conventional neural networks is Keras.

Convolutional neural networks are also very useful for: signals - i.e. speech
recognition -, word2vec techniques - i.e. king - man + woman = queen.

Some well-known CNNs are: AlexNet, capable of classifying images into 1000
classes; LeNet 5, the first network capable of classifying hand-written digits;
VGG16; GoogLeNet; Inception-v3.

## AI

### XAI - explainable artificial intelligence

Very large neural networks tend to lack _explainability_. _Interpretability_ is
the degree to which the human can understand the cause of a decision, without
any knowledge of the model. Explainability goes a step further. For a neural
network to be explainable, a human should be able to understand and trust the
result, and to describe the model. In XAI, the confidence of the result, along
with its reasons, should be part of the result. This is also valuable because it
helps understanding mistakes, and how to fix them.

![xai](./assets/images/xai.png)

The goal with this is to achieve fair and unbiased models, along with model
drift mitigation and model risk management. Unfortunately, there is a tradeoff
between interpretability and accuracy.

### Gestalts

The human brain benefits a lot from Gestalts: it's good at recognizing patterns
and at analyzing the whole as more than the sum of its parts. This feature is
very hard to mimic in current AI models. Gestalts exploits different laws of
perceptual organization, such as: proximity, similarity, closure, symmetry,
continuity, connection, foreground and background distinction.

Pragnanz's law states that objects in the environment are seen by humans in the
way that makes them appear as simple as possible. This is not true for AIs.

### Artificial intelligent systems

A mix of software and hardware systems designed to perform complex task
employing strategies that mimic some aspect of human thought. The key to an
intelligent system is evolution: learning and/or getting better in time.

According to the IEEE computational intelligence society, the definition of
_computational intelligence_ is: the theory, design, application and development
of biologically and linguistically motivated computational paradigms,
emphasizing [...] systems in which these paradigms are contained.

According to Engelbrecht, the five components of computational intelligence are:

1. Artificial neural network.
2. Artificial immune systems.
3. Evolutionary computation.
4. Fuzzy systems.
5. Swarm intelligence.

### Perception

Users and researches feelings about AI are very mixed. The real value of AI and
ML goes side by side with its humanization/usability. As with every technology,
AI is on its journey on the Gartner hype cycle.

![gartner](./assets/images/gartner.png)

### Drivers

The four main drivers for AI are:

1. Gathering more data.
2. Developing better models. Deep learning models were the latest breakthrough.
3. Building more powerful GPUs.
4. Using our brain and how it works as inspiration - i.e. a priori knowledge,
   data selection, data filtering and enhancing, model selection, learning
   technique choice, experiment design, avoidance of brute force, hybrid
   systems.

### Deductive vs inductive

Different kinds of AI are employed depending on the kind of worker that we want
to mimic.

**Deductive learning** is a type of AI techniques that starts from a set of
rules and processes new decisions on new data. Then, it infers rules that are
more efficient in the context to be applied.

**Inductive learning** is based on inferring a general rule from datasets of
input-output pairs. The majority of AI methods fall into this category. Not
relying on rules means that there is no knowledge acquisition needed ahead to
the learning. It is very adaptive to changing conditions: it's enough to rerun
the learning. It shows good generalization, under certain conditions - i.e. the
dataset is good and complete. Generalization is the capability to generate good
rules even when starting from very few data. The risk is to generate bad rules
because of low quality datasets.

![deductive vs inductive](./assets/images/deductive_vs_inductive.png)

**Transduction** consists in deriving values of the unknown function for points
of interest starting from given data.

![transduction](./assets/images/transduction.png)

## Intelligent sensors

Every modern sensor has to be able to perform self-diagnostic and
self-calibration. They also have to be fault tolerant and adaptive. It may also
be very useful in some domains for the sensor to perform data analysis - i.e. a
thermostat could save its owner money by analyzing its workload.

AI can be used to create virtual sensors, which are able to create a metric by
cleverly processing metrics of other physical sensors.

Intelligent sensors are also useful for environmental monitoring.

## Machine learning

Machine learning is not fit for all cases. It works nicely if the nature of the
event is not well understood, or if there are too many exceptions to the rules,
or if the known algorithms are too complex or inefficient.

![ml approach](./assets/images/ml_approach.png)

The output of the neural network - the model - is just a program, capable of
applying its knowledge to new data.

### Algorithms structure

Every machine learning algorithm has three components:

**Representation**: How to process the information? I.e. decision trees, sets of
rules. This choice has a big impact on the features that the learning will show:
robustness, possibility to distribute or embed the algorithm.

**Evaluation**. Different evaluations may show different accuracy, precision,
maximum error, entropy. We should aim for the learning we need, not only care
about accuracy. Some errors may be punished more than others - i.e. false
negatives may be punished more/less then false positives.

**Optimization**. This is done by the engine and it's about moving weights,
thresholds, biases, to meet the desired output. This scales very badly with the
number of parameters, so brute force is not an option. Parameters are tweaked
one at a time, following trends in error decreasing until convergence is
achieved. This approach may get stuck in local minima, but there are ways to get
unstuck. This part of the algorithm can be simplified by restructuring data in a
more complex structure that always has one minimum. These adjustments are not
always available, tho. The optimization may aim at metrics different than the
accuracy.

### Taxonomy

![ml taxonomy](./assets/images/ml_taxonomy.png)

_Unsupervised learning_ uses unlabeled data to perform:

- _Clustering_, which allows to recognize different groups of objects, without
  information about what the groups consist of.

  ![clustering](./assets/images/clustering.png)

- _Dimensionality_ reduction, which compresses the input space by discarding
  some features, thus reducing the input space's dimension and complexity.

_Supervised learning_ uses labeled data to perform:

- _Classification_, which consists in taking a decision for every point in the
  input space: determining boundaries and the side of the boundary in which each
  point stands. The output is discrete.

  ![classification](./assets/images/classification.png)

- _Regression_, which returns an estimation with its associated confidence. The
  output is continuous.

  ![regression](./assets/images/regression.png)

_Reinforcement learning_ doesn't care about producing output from input. It
takes a model of an environment and changes the set of rules to apply to it in
order to optimize some metric - reward.

![reinforcement learning](./assets/images/reinforcement_learning.png)

Since in the real world the presence of labels is not always crisp, most
approaches end up being _semi-supervised_. Labels is a different way to call
desired output.

### Design workflow

In practice, machine learning is about understanding the domain, the prior
knowledge, and the goals. It's also about integrating, selecting, cleaning and
pre-processing data. It's about building a learning model and interpreting the
result. Finally, it's about consolidating and deploying the discovered
knowledge.

The workflow is composed of 5 main steps:

1. Get data.
2. Clean, prepare and manipulate data.
3. Train the model.
4. Test against data.
5. Improve the model.

These steps can be looped through, not necessarily in a sequential way.

Beforehand, there is usually a requirements collection phase. The data
management phase will produce two sets of data: the training set, used to learn
and tune the parameters; the verification set, used only to verify the quality
of the produced model. Before testing it against the verification set, the model
can also be tested in terms of performance. The verified model is then deployed,
monitored, and updated as needed. Once the model is embedded in the world, it
can help producing more, real data, from which to produce new training and
verification sets.

![ml workflow](./assets/images/ml_workflow.png)

#### Data gathering

Where is data coming from? How much data is coming? Data may come from a classic
db query, file processing, streams of data from IoT and AIoT, online platforms.
Data collection is the process of gathering and measuring information on
targeted variables in an established systems. The motto when it comes to data
is: garbage in, garbage out. The neural network won't save a bad data set.

IoT generates a huge amounts of data. The largest chunk of this data is video
surveillance. We need to structure that huge amount of data, which is otherwise
useless. This amount of data also asks for a lot of infrastructure.

Public datasets are the primary source for training - i.e. UCI machine learning
repository, Amazon datasets, Google datasets, governments datasets, computer
vision datasets. The list of public datasets providers grows on a daily basis.

One of the first problems to solve is data format heterogeneity: the issue of
data existing in different metric units. _Data harmonization_ is about
harmonizing data that comes from different sources - i.e. different weather
forecasts that are based on different locations, different measure unit and
different reference systems.

It's also important to perform _data synchronization_ over time. The assumption
that machine's internal clocks are inherently accurate is incorrect. This can be
fixed by synchronizing different machine, by using network time synchronization.

How much data? A neural network can be seen a set of interconnected elements.
The number of parameters determines the degrees of freedom with which these
elements can be rearranged to "build" neural network. Rearranging means
configuring the weights, to correctly classify the inputs. Each weight
corresponds to a linear system - at least -, which requires - at least - two
parameters to be solved. In general, as a rule of thumb, the degrees of freedom
are the number of variables minus the number of equations. For a network with
linear neurons, the amount of data that we need - the number of equations - has
to be at least equal to the number of parameters - the number of variables. If
the amount of data is not able to satisfy the amount of parameters, the degrees
of freedom will be too many and the network could have generalization problems.

#### Data preparation

Data has to become a matrix of features, so that it can be mapped onto a vector
of expected values. This process is divided in two steps: data cleaning and
feature engineering.

Data cleaning is an iterative process - also called data wrangling, or data
preprocessing - made of cleaning, integrating, transforming and reducing. The
flexible series of steps may look like this: discover data, understand data,
structure data, transform data, augment/enrich data, shape data, validate data,
optionally share data. There are many different techniques to do so. It is
important to identify: outliers, errors, missing values, different scales,
distributions, clusters in the dataset, salient features.

The main issues this process cures are: inaccurate or missing data, noisy data,
inconsistent data.

This can be done using Matlab - i.e. Matlab database explorer, R, python, etc.

##### Handling missing data

Missing data can be handled in different ways. The easiest is to delete the
features that are not present in every data source. But what if one of the
features is one of the salient ones? Another way of deleting is to delete the
incomplete vectors instead of the incomplete features.

It is also possible to ignore missing data: some tools are able to still work,
but this can work quite badly in case the amount of missing data is huge.

The best solution is of course to fill the missing data. This can be done
manually, but only when the dataset size is not very big and missing data is not
significant.

It is also possible to do it automatically, by using computed value: the mean,
mode or median for that feature, the value for that feature taken from the most
similar vector, the output of another learning algorithm - this approach may
introduce some bias. Matlab offers a very convenient `fillmissing` function,
which takes the approach as a parameter - i.e. `linear` - and fills the missing
values.

Sometimes complete matrix data may still have some feature not well covered. In
this cases data visualization may help identifying uncovered data.

##### Structured vs unstructured data

Ideally we want to work with structured data. This kind of data usually resides
in a relational db.

It's very complex to work with unstructured data - text files, emails, websites,
images, audio, video, sensor data. Unstructured data usually doesn't follow a
schema, and may be stored in a NoSQL database.

In the middle lies semi-structured data, like XML or JSON data.

![structured vs unstructured](./assets/images/structured_vs_unstructured.png)

To get from unstructured data to structured data we can use markup types.
Nevertheless, it's not compulsory to have structured data to achieve machine
learning - i.e. understanding audience sentiment from social media, detection of
spam emails.

##### Data leakage

Two main reasons:

- Relevant features are missing - i.e. a set of dogs and cats where all dogs are
  yellow and all cats are gray. This will lead to over-fitting and the network
  won't really learn.
- External, biasing features are present - i.e. two merged sets of data with
  different noise levels - that shouldn't be learned.

Something that can lead to data leakage is spilling testing data into the
training set, or vice-versa - slightly better, but still not ideal. This is
important, because statistical independence of the two data sets is key to a
good result. This is especially true if time sequence is involved and future
data leaks into past data.

Data leakage can be avoided by using timestamps to avoid time sequencing
problems and by using data visualization to check the coverage of the feature
space.

![minimize data leakage](./assets/images/minimize_data_leakage.png)

##### Labelling errors

Human supervisors are the typical sources of labels for datasets. Sooner or
later a labelling error will occur. The problem might also be that the label was
once correct, and now it's not anymore, because it hasn't been kept up to date.
This may also arise from automatic label conversion.

Data duplication is also dangerous in this regard, because different copies of
the same output may be labeled differently and lead to training problems.

Checking data to find this kind of problems can become a heavy burden when data
gets large. Brute forcing one to one comparisons between each sample of the
dataset can be too expensive, but might be needed to achieve the desired data
quality.

To compare very complex types of data - i.e. images, which would need to be
compared pixel by pixel - we can use hash functions. To avoid very similar
images to be mapped to completely different hash values - in other terms, to be
able to treat similarity as well as exact equality - we use image hashing. The
proper level of similarity can then be set with a threshold.

##### Similarity

Very similar data provides little information. It doesn't really improve
accuracy, but it still requires storing space and takes time to be processed.
The similarity metric must be tuned according to each specific application. Same
goes for the similarity threshold that is allowed.

Therefore, data augmentation will improve generalization: how good the model is
at learning from the given dataset and at applying the learnt information.

It's very hard to recognize similarities on unstructured data. When dealing with
unstructured data it's better to extract features and use structured data
techniques. Feature extractions allow to switch from the input space to the
feature space.

![feature extraction](./assets/images/feature_extraction.png)

When dealing with structured data, we have different metrics to measure
similarity. We can use the same metrics that we use for pattern matching in the
testing phase, which means that we can actually use a neural network as a
similarity function.

Trivial feature extraction leads to bad, noisy data and requires a huge neural
network. Engineered feature extraction can optimize the result a lot and lead to
a simpler neural network. This requires knowledge of the domain - i.e. by
talking to experts.

Neural feature extraction is about using a neural network to perform feature
extraction. This can lead to such nice features that follow-up decisions are so
easy to make that we can use a classical definition of distance for those. This
also improves explainability a lot.

![nn feature extraction](./assets/images/nn_feature_extraction.png)

The concept of similarity is very close to the concept of distance. They're
usually one the reverse of the other. The cosine distance is very important
because it's able to provide information about opposite points. The Chebyshev
distance - chess distance - only considers the largest magnitude among each
element of a vector. It's very useful when the system should be very strict. The
Minkowski distance allows to apply these three different distances by just
changing one parameter.

![distance](./assets/images/distance.png)

![distance comparison](./assets/images/distance_comparison.png)

Image similarity doesn't return pixel by pixel similarity, but similarity by
context. This is useful in cleaning datasets from duplicates, implementing image
search by similarity and in tracking changes in imagery.

This can be done using:

- Key-point matching - SIFT and SURF. Try to find specific features in the
  images - i.e. angles, specific shapes -, and see if they have any in common.
  This method can match images even under different scales, rotations and
  lighting, but the running time could be high. SURF is quicker and better in
  handling rotation, blurring and warping. SIFT is better at handling scale.

  ![key point matching](./assets/images/key_point_matching.png)

- Histograms. Compares the color histograms of the images. It's resilient to
  rotation and very fast, but doesn't take into account shapes and patterns.

  ![histogram](./assets/images/histogram.png)

- Image hash. The images are reduced to a small hash code and compared with
  hamming distance. Different techniques may be employed: aHash, pHash and
  dHash. They care about, respectively: average, perceptive and gradient.

  ![image hash](./assets/images/image_hash.png)

Cross-correlation is a measure of similarity for vectors or matrices. When
applied to images, other than a similarity level, cross-correlation also returns
the better alignment of the two images. This alignment is found by shifting one
image over the other and performing pixel by pixel multiplication. The sum of
the products will be maximum in correspondence of the best alignment. The
measure that is actually used is normalized cross-correlation. Cross-correlation
is the basis of pattern recognition and machine learning. The intelligent system
can recognize patterns because they are similar to a previously labelled one,
seen during the learning phase. Using two completely different images will not
produce only noise as a result, but a plateau, instead of the usual spike of
similarity.

![cross correlation](./assets/images/cross_correlation.png)

What is used in an algorithm is actually convolution, which differs from
correlation by just the way the image is shifted. This result in minuses instead
of plusses in the final formula.

![correlation vs convolution](./assets/images/correlation_vs_convolution.png)

Internal similarity - parts of the image being very similar to other parts of
the same image - can be exploited by cross-correlation to perform pattern
recognition.

![internal similarity](./assets/images/internal_similarity.png)

Performing the same pattern recognition with a slightly different image is still
possible. Scale is very important in this scenario. To get around this issue,
different kernels with different scales are used. On the other hand, convolution
is not sensible to shift.

![pattern recognition](./assets/images/pattern_recognition.png)

##### Exploratory data analysis and data visualization

This can be partly done with _statistical tools_: mean, median, std, min, max,
histograms - which convey information about data distribution - and box-plots -
which additionally convey information about outliers.

This can also be done with _data visualization_, which is also used in training
and improving the model. Data visualization is the process of converting raw
data into easily understandable pictures or information that enable fast and
effective decisions. It allows to spot nuances. It focuses on 2D and 3D
visualization, to meet humans perception, and tries to exploit human gestalts
like proximity, similarity, enclosure to encourage the eye to compare.

![data visualization](assets/images/data_visualization.png)

Bertin defined 7 visual variables that we can use: position, form, orientation,
color, texture, value and size. Combined together they allow us to link data
attributes to visual elements, encoding data using visual clues. Not all
visualization features work the same, especially when used by different all
people.

![data visualization anatomy](./assets/images/data_visualization_anatomy.png)

Ideally, to emphasize efficiency, a single image with not excessive details is
used. It should be intuitive, not only for the user, but also for the designer.
The key features to show are distribution, relationship, comparison, connection,
composition and location. Data should be revealed at several level of details
and serve a clear purpose. The graphic power is lost if data is corrupted, not
representative, not properly processed, or fake.

![data visualization focus](./assets/images/data_visualization_focus.png)

A very good tool for data visualization is R. It is also a standard, and highly
demanded for that reason, to have a guarantee on the uniformity of the result.

_Saliency maps_ are a useful visualization tool to determine which feature of
the input is mostly contributing to the output.

![saliency map](./assets/images/saliency_map.png)

It is useful sometimes to visualize the different kernels that are being used in
a convolution neural network, and the corresponding emphasized portion of the
image.

![data visualization kernel](./assets/images/data_visualization_kernel.png)

We can also build/use an entire machine learning model visualization tool - i.e.
Google TensorBoard, Meta Visdom. This is useful to analyze the trained network.

##### Feature engineering

It's an iterative process made of multiple phases: brainstorming features,
deciding which features to create, creating features, testing features,
improving features.

When optimizing features we start by optimizing one feature at a time,
consulting experts or using statistical methods, and then we optimize all
features at once, using unsupervised learning.

A key part of feature engineering is normalization - i.e. making images black
and white and scaled uniformly, to focus on shapes. In general, the goal is to
transform features to be on a similar scale, to improve performance and the
stability of the model. The most common techniques are: scaling to a range,
clipping - useful to get rid of outliers -, logarithmic scaling - useful when a
handful of values have many points, while most other values have few points -
and z-score. In particular, getting rid of outliers before performing other
types of normalization is extremely important.

![feature normalization](./assets/images/feature_normalization.png)

**Feature selection** consists in choosing a subset of the original pool of
features. This helps in fighting the curse of dimensionality. The idea is to
remove information which is irrelevant, or partially relevant, which can
negatively impact the performance. This reduces over-fitting due to noise, it
can improve accuracy and reduces training time.

There are three methods to perform feature selection:

- _Filtering_. A filter ranks the features in terms of relevance, so that we can
  only keep the top $N$ features. Finding a good filter is not easy. The filter
  should prefer features with high variance and low correlation. Of $N$
  correlated features, only one should be kept. This method is robust against
  over-fitting, but may drop important features.
- _Wrapping_. We create a subset of features, and we use them to train the
  model. If the obtained model is optimal, these will be the selected features,
  otherwise the process is repeated with different features. The process is
  slower, because of the repeated learning, but we are sure to keep very good
  features. It's prone to over-fitting. In sequential forward selection, given
  that the number of possible combinations of the feature is too high, we can
  add one feature at a time by trying all of them and keeping the one that
  performed better. We keep track of the training error, so that if we notice it
  dropping in the process we know which are the noisy features.

  ![sequential forward
selection](./assets/images/sequential_forward_selection.png)

  The reverse is also possible: starting with all the features and dropping them
  one at a time.

  ![sequential backward
selection](./assets/images/sequential_backward_selection.png)

- _Embedding_. A very large model containing all the features is created. By
  observing the model it's possible to identify the best features. It works
  similarly to wrapping, but requires less computation. It also has similar pros
  and cons. I.e. a weight in the final model is 0 tells that this feature is
  completely useless.

**Features extraction** is mostly performed by _principal component analysis_ -
PCA. PCA is an unsupervised - no labels - process about projecting the feature
space into a lower dimensioned space, while preserving as much information as
possible. This projection should minimize the squared error in reconstructing
the original data, and maximize the variance in the projected data. In the
following example, the initial three features are melted in the two dimensions
of the plane.

![pca](./assets/images/pca.png)

Features that lead to no variance when projecting onto them are useless, and can
be dropped.

![pca variance](./assets/images/pca_variance.png)

_Linear discriminant analysis_ - LDA - is another technique to perform feature
extraction, which is similar to PCA, but uses labels. The goal is to find a
linear combination of features that characterizes or separates the classes.

![pca vs lda](./assets/images/pca_vs_lda.png)

$t$-_distributed stochastic neighbor embedding_ - $t$-SNE - is a non linear
technique for dimensionality reduction that is particularly well suited for data
visualization, even with high-dimensional datasets.

![pca vs t-sne](assets/images/pca_vs_tsne.png)

_Independent component analysis_ is often used in signal processing and
industrial application to remove noise.

![ica](./assets/images/ica.png)

PCA can be automated by using deep learning and convolutional neural networks.
We can do this by removing the last, fully connected layer from a pre-trained
network.

#### Train the model

##### Testing the tool

It's a golden rule when testing a new tool to start with an easy, well-known
dataset, not the actual dataset of the application. This is because a well-known
dataset has a lot of reference and can act as a debug tool.

One example of such a dataset is the _IRIS_ dataset, aimed at classifying
different species of irises based on four features and 150 samples.

Some very useful data visualization tools for a dataset are:

- Box-plots.
- Parallel plots. Extremely useful plots that show patterns and salient feature,
  but require the same measure units and therefore good data harmonization.

  ![parallel plots](./assets/images/parallel_plots.png)

- Correlation plots. Useful for visually identifying correlation between
  features. When a lot of features exist, since it's not possible to compare
  them all, we can use principal component analysis: a method for reducing a
  dataset dimensionality while preserving spatial characteristics.

  ![correlation plot](./assets/images/correlation_plot.png)

##### Encoding

The way we encode the categories/labels will affect the learning.

_Integer encoding_ assigns an integer value to each category. It works well and
it's easily reversible. A downside is that integers have a natural ordered
relationship between each other. This is relevant because the algorithm may be
able to understand and harness this relationship. This may result in poor
performance or unexpected result.

_One-hot encoding_ uses variables where no such ordinal relationship exists -
$N$-tuples of 1 and 0, where $N$ is the number of categories.

![one hot encoding](./assets/images/one_hot_encoding.png)

##### Prepare the input features

To improve gradient descent, we can normalize the values.

##### Dataset balancing

A balanced dataset is one where there are usually equally many samples per
class. Both the training set and the testing set should be balanced.

Many real-world databases are unbalanced - i.e. in a medical dataset, with
regards of a certain disease, the majority of patients are healthy, while only a
small fraction suffers from the disease. Also, the cost of misclassification is
usually not equal for each class - i.e. erroneously diagnosing a healthy person
is bad, but not as bad as erroneously not diagnosing a sick person.

To improve the situation I can follow four different strategies:

1. Look for more data to cover under-represented classes.
2. Change the labelling, by grouping and merging rare classes together. This
   requires multiple, hierarchical classifiers. The first classifier identifies
   the rare class, then the second one distinguishes between the specific
   classes internally to that rare class - i.e. the first classifier
   distinguished between sick and healthy, the second classifier distinguishes
   between different types of sickness.
3. Change the sampling, by:
   - Ignoring samples from the most represented classes. Very simple, but causes
     loss of information.
   - Over or under sampling, by adding some noise.
   - Negative mining.
4. Increase the weight of the samples from rare classes.

##### Dataset partition

As mentioned multiple times: to train a good, non-biased model it's crucial to
separate training data from testing data. All of the design activities must be
performed on the training dataset.

By using the same data to train and evaluate the problem, or by partitioning the
data badly, we could run into under-fitting - the model is not accurate and has
a high error-rate - and over-fitting - the model is too adjusted to the train
dataset, resulting in poor generalization.

![under-fitting vs
over-fitting](./assets/images/underfitting_vs_overfitting.png)

The testing data has to possess some characteristics, like generality and
statistical confidence. At the same time, the training dataset should have
enough data so that the model can exploit the dataset and learn all cases.

Cross validation tries to predict the accuracy of the model with good
generalization, which means independently from the specific testing dataset that
is being used.

![cross validation](./assets/images/cross_validation.png)

There are different methods to perform cross validation:

- $k$-fold cross validation randomly partitions the dataset into $k$ partitions.
  Of these partitions, one is retained for testing the model, and the others are
  used to train it. The process is repeated $k$ times, using every time a
  different partition for testing. After all the iterations, we can compute the
  mean and the std of the obtained errors. The value for $k$ may vary. The most
  common values are 5 and 12. This number also needs to be adjusted depending on
  the dataset size, to not obtain a very small and therefore irrelevant test
  dataset. It is also important to be careful about having every class
  represented in the partitions.
- 5x2 cross validation randomly partitions the dataset in two subsets $A$ and
  $B$, $A$ is used to train, and $B$ is used to test. Then, $B$ is used to
  train, and $A$ is used to test. This process is repeated as needed. After all
  the iterations, we can compute the mean and the std of the obtained errors. By
  shuffling every time, this technique is less dependent on the order of the
  dataset.
- Leave one out is useful with small datasets where it's not possible to retain
  enough data for the test dataset. It's a corner case of $k$-fold where the
  test partition is made of only one case. $k=N$, where $N$ is the number of
  elements in the dataset.

##### Clustering

Clustering is the task of grouping a set of objects in a way that objects in the
same group are more similar to each other than to those in other groups.

Clustering is most of the time unsupervised. It's useful to look at large
amounts of data, to perform compression or de-noising, to create histograms of
images, or separate images in different regions, to make predictions - i.e.
images in the same cluster may end up having the same label.

$k$-means clustering is based on the euclidean distance. $k$ stands for the
number of clusters, and there is no one-solution-fits-all to decide which number
to use. Each point is assigned to the cluster belonging to the closest $k$
point. Clusters are generated as a Voronoi diagram, starting from the $k$
points.

We can start by choosing random points, and then moving them iteratively towards
the center of mass of the points that fell into their clusters. When the points
are not moving anymore, we found an equilibrium and we can exit the algorithm.

![k-means](./assets/images/k_means.png)

$k$-means is very efficient, guarantees convergence and it easily adapts to new
examples and clusters of different shapes and sizes.

On the other hand, $k$-means might converge to local minima. $k$-means is also
not very good at handling outliers, which tend to have a very high weight. It
can only be applied when a concept of mean exists - i.e. what about categorical
data? It doesn't handle well clusters with a non convex shape. We need to choose
$k$, which might not be trivial.

##### Decision trees

This is a very explainable model. A decision tree checks one feature at a time,
also depending on the previous features values. Internal nodes test the value of
a particular feature of the input, and branch according to the result of the
test. Leaf nodes assign a class to the input.

![decision tree](./assets/images/decision_tree.png)

The boundaries will define boxes. The accuracy of those boxes depends on how big
we are willing to make it.

![decision tree boxes](./assets/images/decision_tree_boxes.png)

#### Test the model

##### Accuracy/error assessment

To assess the accuracy of a model, for regressors we can use:

- $R$-squared: the proportions of variation in the outcome that is explained by
  the predictor variables. It's an non-dimensional value contained in $[0,1]$,
  with the perfect regressor having a value of 1.
- Root mean squared error, which measures the average error performed by the
  model in predicting the outcome for an observation. It has the same unit of
  the variable and doesn't have an upper bound.
- Mean absolute error: an average of the absolute values of the errors. Same
  unit and bounds as the root mean squared error.

For classifiers, accuracy is measured in as the ratio of predicted results that
match the expected result. An absolute value of accuracy doesn't carry much
value, because the error percentage, as low as it may be, may happen on a
crucial set of case - i.e. a rare class - and result in a poor model.

The confusion matrix may carry much more value and allows to compute more
significant figures: misclassification, precision, sensitivity and specificity.

![confusion matrix](./assets/images/confusion_matrix.png)

![confusion matrix multi-class](./assets/images/confusion_matrix_multiclass.png)

In a multi-class confusion matrix I can also look for very high error rates, to
identify classes for which I probably need more examples.

The receiver operating characteristic is not just a single number, but a
graphical plot of the errors of a binary classifier as its discrimination
threshold is varied. The ROC curve is created by plotting the true positive rate
against the false positive rate at various threshold settings.

![roc](./assets/images/roc.png)

##### Bayes optimal classification

Bayes optimal limit is the unsurmountable limit on accuracy - or on the error,
depending on the perspective - that a machine learning algorithm faces. Every
time the distribution of the classes are not completely separated, even the best
classifier is producing an error which is related to this overlap.

![overlapping](./assets/images/overlapping.png)

Bayes optimal classification is defined as the label produced by the most
probable classifier. Identifying the best classifier can be done using a formula
which works better in theory than in practice. No other classification method
can outperform this one.

To achieve a similar result, we can use the naive Bayes classification, which
builds the estimation of the distributions, and hence finds a proper separation
plane, starting from the actual distribution of the train set. For this system
to work with good accuracy, we need strong independence of features and high
dimensionality of the inputs. This is a very simple method which might be worth
starting with, before using other classical methods.

![naive bayes](assets/images/naive_bayes.png)

##### Curse of dimensionality

Adding a new feature doesn't always improve the system. Increasing
dimensionality will also increase exponentially the number of "bins" required to
cover the feature space, and there won't be enough data to populate each one of
them. To avoid this, we can use feature selection, dimensionality reduction and
model complexity control.

![curse of dimensionality](./assets/images/curse_of_dimensionality.png)

##### Classical methods - non neural

Classification methods divide in eager and lazy. Eager learner are quite good at
generalization: they create an input-independent function during the training.
Eager learners take longer to train and have a fixed number of degrees of
freedom, but they are fast at recalling and require low memory to do so. Lazy -
instance-based - learners learn by storing all training instances and delay the
generalization until a query is made to the system. Lazy learners are very fast
at learning and handle as many degrees of freedom as the data requires. On the
other hand, recalling takes longer and requires more memory.

![eager vs lazy](./assets/images/eager_vs_lazy.png)

**Nearest neighbor classifier - kNN** - is deterministic and not based on neural
techniques. It's good to create a reference. It's easy to design, as it needs a
low number of parameters. The learning is very simple and the explainability is
perfect. It's very powerful for understanding the dataset and as a debug tool.
Given enough data, kNN tends to the Bayes optimal classifier.

It requires: the set of stored record, a distance metric and the value $k$ of
the number of nearest neighbors to retrieve. A point $x$ will be classified the
same as the closest $k$ neighbors.

![kNN](assets/images/kNN.png)

For $k=1$, we can visualize this with a Voronoi diagram.

![voronoi](assets/images/voronoi.png)

$k$ shouldn't be even, otherwise a situation of tie might happen. A very low
value for $k$ is sensitive to noise points, whereas a very large value for $k$
may include points from other classes. High values for $k$ can be used to
perform regularization. This obviously requires more time to compute.

![regularization](./assets/images/regularization.png)

Most of the time, the best distance metric is the Euclidean distance. Votes can
be weighted by taking into account the distance of each point - i.e. a certain
point vote counts $1/d^2$ where $d$ is the distance to the original point.
Attributes may have to be scaled to prevent distance measures from being
dominated by one of the attributes.

This classifier is robust to noisy data, but suffers from the curse of
dimensionality and it's quite slow. The total time is dominated by the distance
computation. Performance improves if we are willing to sacrifice exactness. The
algorithm can be biased by scaling one feature - by default, the algorithms
gives the same importance to every feature -, choosing a different distance
function or choosing the value for $k$.

#### Working with a new model

When faced with a new model, we should start by reading the documentation and
the examples. We should check the memory needs, the dataset size, how the
parameters look and if the model is sensitive to tuning those parameters. We
should check for the need to re-train the model, or if fine-tuning is enough.
The portability of the model is also important. A model may behave differently
based on the architecture is running on.

### Choosing the right technique

To choose the right technique, it's essential to gather intimate knowledge about
the problem, as well as the candidate techniques. Real problems won't be solved
with one, big model. Most of the time a combination of techniques will be used.

Each technique will provide different features - learning curve, development
speed, tolerance for complexity and noisy or sparse data, independence from
domain experts -, and it will fullfil the requirements differently.

In general, it is a good heuristic to go "deep" only if necessary. Starting with
simpler, classical solutions first, often saves design and processing time.

### Hardware

The hardware requirements for machine learning are: mass storage, to store the
high amounts of data needed; high performance processors, specialized to perform
learning.

GPU are usually more suited for this purpose, both in terms of performance and
in terms of energy efficiency. This doesn't mean that a GPU is needed all the
time - i.e. working with signals or with small images; working with non-parallel
workloads.

The advantages of using CPUs instead of GPUs are: portability, market
availability for different prices and performances, ease of deployment across a
wide spectrum of devices - including the edge.

Tensor processing units - TPU - are application-specific AI accelerator
developed specifically for neural network machine learning.

We can use proprietary server farm instead of getting our own hardware.

The hardware is very relevant in training the model, in testing the model and
also in the actual deployment. Different computational units may use different
number of bits and lead to slightly different behaviors. Softwares like Matlab
allow to perform training with the same code on different devices, optimizing it
for each of those devices.

### Software

It's needed at all stages. Ideally, software tools provide some pre-built
features:

![ml software features](./assets/images/ml_software_features.png)

The current trend is to provide ML as a service: automated or semi-automated
machine learning available on the cloud - i.e. Azure ML, Amazon ML and Google
Cloud AI. This ML as a service is also deployable on the devices. They provide
high-level API and allow to feed data to already trained models for different
tasks. They provide multiple learning methods and graphical interfaces.

## Digital ages

Four digital ages have been identified:

- The internet era.
- The social media age.
- The collaborative economy age - the current one. Sharing products/information
  for collective value.
- The autonomous world age - which we are already moving into.

  ![collaborative economy](./assets/images/collaborative_economy.png)

We are already living in a digital world, which poses many environmental and
industry challenges. Some initiatives that go in these directions are: industry
4.0, internet of things, internet of everything. All of these application can
benefit a lot from AI.

## Edge, fog, cloud

![edge fog cloud](./assets/images/edge_fog_cloud.png)

## Internet of things - IoT

There are three types of IoT data sources:

- _Passive_ data sources aren't able to actively communicate. To consume data
  from these devices, we have to ask them explicitly to send the data they
  collected. These sensors are typically very cheap and very small.
- _Active_ data sources continuously stream data, so we need to absorb, process
  and store data in real-time, if we don't want to waste it.
- _Dynamic_ data sources are capable of communicate in both directions, and
  change the data and/or the parameters of the communication. They are also
  capable of self diagnostic, calibrating and updating. This is the kind of data
  source where we can apply AI.

We can also categorize IoT devices in terms of the target audience:

- _Consumer_, anything consumer oriented: watches, phones, laptops.
- _Commercial_, aimed at professional usage: device trackers, medical devices.
- _Industrial_, mostly sensors.

IoT is based on IP based protocols. They are very different in terms of:
real-time capabilities, determinism[^determinism], ease of interaction for
humans, security. The three main protocols are:

- _Consumer_. Non-real-time and non-deterministic. It's easy for human to
  interact with the device. In case of failure, the system has to be rebooted.
  Usually streams large amounts of data.
- _Industrial_. Usually streams smaller amount of data, but more reliably and
  with resilience to failures. It's real-time and secure. Sometimes a physical
  connectivity is required. This is one of the major trends, as it has a
  considerable global impact on economy and smart cities.

  ![iot industrial](./assets/images/iot_industrial.png)

- _Machine to machine_. No human is involved. It streams smaller amounts of
  data, but the communication has to be reliable. The location of data is
  valuable.

The next step is the _internet of everything_: reaching seamless interconnection
and autonomous coordination of computing elements, sensors, inanimate and living
entities, to enable a connected universe.

[^determinism]:
    In a deterministic system, every action, or cause, produces a
    reaction, or effect.

### Examples

![iot pirelli](assets/images/iot_pirelli.png)

The sensor in the tire communicate with a control unit. The control unit
communicates with the consumer's app and the dealer's app. Both apps communicate
with Pirelli's data lake.

The idea is to create business opportunities by using this sensors to provide
additional services to the consumer, and to extract value from data.

### Security

Given the sometimes sensitive nature of the task or of the data we entrust this
kind of technology with, it's imperative to focus on security. There are six
layers to take care of to ensure this type of security:

1. Device intelligence. The device is able to understand if it's being attacked.
2. Edge processing.
3. Device initiated connections.
4. Messaging control.
5. Identification, authentication and encryption.
6. Remote control and updates of devices, using over the air updates.

![iot security](./assets/images/iot_security.png)

### Artificial intelligence of things - AIoT

It provides data as a service. It needs dedicated hardware - protocols,
chip-sets - and software - API to allow interoperability through device,
software and platform.

The AIoT systems should be:

- Robust, self calibrating, self diagnosing, noise reducing.
- Secure, accurate and aware of the quality of data.
- Scalable, supporting different protocols and data format.

Most of the design and effort usually goes into the software. The idea is to
create a learning machine. Typical application are enterprises and industries.
Real time data is key.

Features that we expect are: remote management of the device, serverless
microservices - so that the device is self-sustained, rapid prototyping and
creation, API interaction.

### Use cases

_AWS_ sells chips - micro-controllers - to embed in devices, running their
FreeRTOS operating system, which collects data thanks to Greengrass, which in
turn sends it to the cloud, as soon as possible, from were they can be used for
processing or to further control the device.

![iot aws](assets/images/iot_aws.png)

_Matlab_ - specifically Simulink - allows to design, prototype and deploy IoT
applications easily, thanks to a building blocks approach. It provides a lot of
pre-built functions. Matlab ThingSpeak aggregates and analyzes data, without the
need of being aware of how the device is collecting it. While offline, the
sensor collects data. Periodically, the sensor connect to the network and sends
the collected data to ThingSpeak, which will analyze it.

_Microsoft Azure_ provides a more business oriented, easy to use workflow.
Alternatively, one can also embed a Windows operating system core.

![iot azure](assets/images/iot_azure.png)

A good example of hardware use case is _Arduino_. It offers different chips, in
terms of resources, networking capabilities and spatial and energetic
efficiency.

Many companies are producing industrial boards with deep learning model
accelerators.

## Evolutionary computing

Genetic algorithms are based on the principle of survival of the fittest. They
require to recognize a good result, but not how to get there.

An initial set of random solutions is ranked in terms of ability to solve the
problem at hand. The best solutions are then crossbred and mutated to form a new
set. The ranking and generation of new solutions is iterated until a good enough
solution is found.

## Fuzzy systems

Fuzzy systems allow to reason approximately about precise inputs - just like
humans - to then provide precise outputs.

![fuzzy logic](./assets/images/fuzzy_logic.png)

## Agents

One of the core paradigms in AI: a long-term operating program which reads the
environment, and takes decisions to change the environment accordingly. It has a
memory, sensors to perceive information from the environment and actuators to
influence the external world. It can use a high variety of tools and algorithms.

![agent](./assets/images/agent.png)

![agent types](./assets/images/agent_types.png)

![agent structure](./assets/images/agent_structure.png)

## Hybrid intelligent systems

A software system which employs in parallel a combination of methods and
techniques from artificial intelligence. The aim is to mitigate the drawbacks
that the different techniques show when used separately.

## Intelligent vision

Intelligent vision is composed of different tasks:

![intelligent vision](./assets/images/intelligent_vision.png)

### Image classification

Image classification is about processing a whole image and making a decision on
which label to assign to this image.

![image classification
alexnet](./assets/images/image_classification_alexnet.png)

To train and fine tune a model, the ImageNet challenge is a good starting point.
Its problem is that this kind of dataset are not very good when it comes to
industrial, domain specific application. It's still good to train the feature
extraction part of the network.

### Object detection

An image is processed, and a label is assigned to a specific portion of it. It's
almost impossible to get real measurements with object detection. This is very
different than object segmentation, which is capable of real measurement of the
objects.

Multiple object detection might get tricky in complex scenes, especially if
there is overlapping.

It can be very hard to manually extract features. Sometimes we are forced to use
deep learning. Hybrid solutions are possible and most of the time the most
convenient approach. Manual feature extraction can be used for blob detection,
and then deep learning will be used inside the blob.

### Object measurement

Typical of industrial applications. The first step is object detection. Then, we
need to perform object segmentation. Finally, we can perform the measurement.
The first step is detecting the bounding box.

![bounding box](./assets/images/bounding_box.png)

White backgrounds can be very beneficial for this purpose. Sometimes there is
overlapping, so we might want to perform pattern detection to distinguish the
different objects.

Sometimes two-dimensional images are not enough to perform the measurement.

### Object segmentation

The idea is to locate an object with respect to the background. The problem of
defining segmentation is that the concept of a region is context-dependent.

Segmentation is closely related to feature extraction. It can be done with three
main classical systems:

- Global knowledge. Uses global information about the image - i.e. histograms.
- Edge-based. Looks for objects by searching for edges.
- Region-based. Uses local information like brightness, texture or color to
  locate objects.

Semantic segmentation is an even more complex task: labeling each pixel in the
image with a category label. It doesn't care about different instances of a
category. Pixel classification is a good but unfeasible idea. We can use a fully
convolutional network with down-sampling and follow-up up-sampling. To scale up
the size of the down-sampled image we can use de-convolution.

![semantic segmentation](./assets/images/semantic_segmentation.png)

A very important use case for semantic segmentation are drones. They cannot use
generic models like RevNet50, because aerial images are very different than
generic images.

### Intelligent systems for robotics

Vision is a fundamental feature of robotics. The most important task where
vision is involved is control: controlling movements, power, etc.

In industry, intelligent vision is key to provide efficiency and high quality
standards. Most of the time there is an illumination system on the background of
what is being monitored with the camera. The cameras communicate with the PLC,
which in turn communicates with the rest of the system, based on the input from
the cameras.

Training is not possible even on modern intelligent cameras, due to
computational capabilities. Nonetheless, they are able to perform deep learning.

## Industry 4.0

Any application needs some feature to comply with industry 4.0. Industry 4.0
stands for highly intelligent connected processes and systems create through
digital networked data communication.

![industry 4.0](./assets/images/industry_40.png)

The design principles to keep in mind when designing a system for Industry 4.0
are:

- Interoperability, to be able to connect with machines, devices, sensors and
  people via the IoT or IoP.
- Information transparency, to create a virtual copy of the physical world that
  has some value to other systems.
- Improved technical assistance, like self-diagnosing.
- Decentralized decisions, the ability for the system to take decisions on its
  own, but also to delegate to an external system.
