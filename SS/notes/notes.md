# Software Security

## Table of Contents

1. Low-level attacks
   1. Buffer overflow
      1. Introduction
         - Origin
         - Relevance
         - Brief description
      2. Memory layout
         - 32-bit Intel Linux process memory
         - Activation record
         - Calling and returning from a function
      3. The attack
         - Exploiting the stack
         - Shellcode
         - Injection vector
         - NOP sled
   2. Heap overflow
      1. Memory layout
         - The heap
         - Chunks
      2. The attack
      3. House of force
   3. Use after free
2. Low-level defense
   1. Introduction
      - Channel problem
      - Types of defense
      - Threat model
   2. Memory safety
      1. Spatial safety
         - Idea
         - Pointers
      2. Temporal safety
         - Idea
         - Garbage collector
   3. Type safety
   4. Avoid exploitation
      1. Canary
         - Idea
         - Possible implementations
         - Weaknesses
      2. Non-executable memory
         - Idea
         - Return-to-libc
      3. Address-space layout randomization
         - Idea
         - Weaknesses
3. Return-oriented programming
   1. Introduction
      - Idea
      - Gadgets
      - Injection vector
      - CISC
      - ROP compilers
   2. Hacking blind
      - Threat model
      - Guess the canary
      - Find a pop gadget
      - Find a write call
4. Control flow integrity
   1. Introduction
      - Idea
      - Efficiency and efficacy
      - Weaknesses
   2. Control flow graph
      - Call graph
      - Basic blocks
      - Idea
   3. In-line monitor
      - Idea
      - Labels
      - Strengths
5. Static and dynamic flow analysis
   1. Introduction
      - Code analysis
   2. Static and dynamic analysis
      - Static analysis
      - Dynamic analysis
   3. Soundness and completeness
      - Soundness
      - Completeness
      - Hybrid approaches
6. Symbolic execution
   1. Introduction
      - Idea
      - Limits
   2. Symbolic expressions
      - Unknowns
      - Examples
   3. Execution paths
      - Path conditions
      - Feasibility
      - Constrain solver
      - Soundness and completeness
   4. Execution algorithm
      - Static approach
      - Concolic execution
      - Edges of the program
   5. Search
      - Path explosion
      - DFS and BFS
      - Random
      - Coverage-guided heuristics
      - Generational search
      - Combined search
   6. Satisfiability Modulo Theories
7. Fuzzing
   1. Introduction
      - Black box
      - Grammar-based
      - White box
      - Root cause
      - Symbolic execution
8. Meltdown and Spectre
   1. Introduction
      - Idea
      - Side channel attacks - with example
      - Speculative execution
   2. Meltdown
      - Idea
      - Execution
   3. Spectre
      - Idea
      - Execution
   4. Solutions
      - Hardware solutions
      - Fence Assembly directives
      - Firmware solutions
      - Kaiser
      - Research

## Low-level attacks

### Buffer overflow

The most well-known form of attack. It exists since the 80's - 1988, Morris
Worm[^worm]. It is still unresolved to this day. It is harder to perform, due to
increasing defenses, but it's still effective. They are still relevant because
`C` and `C++` are still popular programming languages, used in critical systems
like OS kernels and utilities, high-performance servers, and embedded systems.
This method is also important because it acts as a guideline on the
characteristics that a good defense should have.

It is a memory error, typical of the C language. The typical symptom is the
program crashing with a `segmentation fault` error. Depending on where the bug
is located, it is possible to: steal private information, corrupt valuable
information, run code of the attacker's choice. Identifying the bug is the first
step to perform the attack. Manual analysis of the code rarely achieves the
goal. After that, it's important to know at what level to place the defense:
compiler, OS, or architecture.

[^worm]:
    A virus-like infection. A stand-alone program that infects a machine,
    and from there it automatically expands infecting other vulnerable machines.

#### Memory layout

On a 32-bit Intel, Linux system a process memory goes from address `0x00000000`
to `0XBFFFFFFF` and it's divided into:

- _STACK_, which grows downwards - a key feature with respect to low-level
  attacks.
- _HEAP_, the dynamic memory of the process, which grows upwards.
- _.TEXT_, the code.
- _DATA_.

![memory layout](./assets/images/memory_layout.png)

Calling a function in C consists in using a `CALL` assembly instruction.
Whenever such an instruction occurs, after `PUSH`ing the function parameters on
the stack, the return address - from where to resume execution after the
function is done - is pushed on the **stack**, before `JMP`ing to the function.
This sequence of instruction is called _prologue_ of the function, and is
responsibility of the caller. When the function starts executing, local
variables of the function are also pushed on the stack. These three elements are
the _activation record_ of the function. The _stack pointer_ `SP` always points
to the last entry of the stack. The _frame pointer_ `FP` is an optional pointer
that points to the _save frame pointer_ `SFP = [FP]`[^square_brackets], which
contains the address of the previous activation record's frame pointer. The
frame pointer serves as a fixed offset to access the current activation record's
entries. The save frame pointer is needed when returning from the function, to
quickly locate the frame pointer of the previous activation record. The `RET`
instruction performs a series of micro-operations:

- Makes the frame pointer point to the save frame pointer - `FP = [SFP]`.
- De-allocates local parameters, running `POP SP` multiple times.
- Makes the program counter `PC` point to the return address - `PC = [SP + 4]`.
- Jumps to the return address - `JMP PC`.

  [^square_brackets]:
      Square brackets mean I am accessing the value of the
      register.

It's responsibility of the caller to de-allocate the global parameters,
performing `POP` as many time as needed. This sequence of instruction is called
_epilogue_ of the function.

```c
void f(int a, b) {
    int c;
    c = a + b;
    printf("%d", c);
}

void main(int argc, char **argv) {
    int c1, c2;
    f(c1, c2);
}
```

```c
// f(c1, c2) - prologue;
PUSH c1
PUSH c2
PUSH ret
JMP f

// inside f
PUSH c

// ret
FP = [SFP]
POP SP         // de-allocate c
POP SP         // de-allocate SFP
PC = [SP + 4]  // put return address in PC and move SP past RET ADDR
JMP PC

// epilogue
POP SP         // de-allocate c1
POP SP         // de-allocate c2
```

![activation record](./assets/images/activation_record.png) ![activation record
with pointers](./assets/images/activation_record_with_pointers.png)

![function return](./assets/images/function_return.png)

#### The attack

```c
void f(char *buf1) {
    char *buf[20];
    strcpy(buf, buf1);
}

void main(int argc, char **argv) {
    get(buf1);
    f(buf1);
}
```

![activation record with
buffer](./assets/images/activation_record_with_buffer.png)

This kind of operation can be **exploited**. Due to performance, the language
doesn't make any check on the memory, hence, if `buf1` is bigger than `buf`, it
is possible to overwrite any part of the stack. In most cases, this will result
in a crash. If the attacker manages to overwrite the return address, they can
jump to any location in memory. If the attacker also manages to inject code at
some location in memory, they can execute arbitrary code.

The injected code has to be a sequence of bytes representing machine code, which
is also called **shellcode**, due to the fact that historically the attacker's
goal has been to run the shell. The portion of memory on which we have the
necessary control to inject this code is the stack. The goal is to make the
attacked function's return address point to the first instruction of the
shellcode. It's important to leave some space between the shellcode and the
return address, so that the shellcode can use it as _working area_ on the stack.
Otherwise there would be the risk of overwriting the return address while
executing the shellcode, causing errors.

![activation record injected](./assets/images/activation_record_injected.png)

The input to be given to achieve this goal is called **injection vector**, and
has to be properly shaped. The address of the buffer is not fixed and must be
figured out. Particularly, it depends on the size of `argv`, and it might change
from an execution of the function to another. Also to be taken into account when
defining the injection vector, between the return address and the rest of the
activation record there are two empty words. Be also aware of big endian and
little endian. On some architecture, the address must be wrote backwards.

![injection vector](./assets/images/injection_vector.png)

Brute forcing the address by trial and error is not the best option. To increase
our chances, we can use a **nop sled**, a sequence of `NOP` instructions to put
before the shellcode to increase its "surface". Jumping at any point of the nop
sled doesn't make any difference, since all the instructions are `NOP` and
eventually lead to the shellcode.

![injection vector with nop
sled](./assets/images/injection_vector_with_nop_sled.png)

The main channels that the attacker can use are the command line input of the
program, and the standard input - user input or file input.

### Heap overflow

#### Memory layout

The stack is a fixed memory, known at compile time. The **heap** is a dynamic
memory, used to allocate objects, structures and buffers. It's slower than the
stack, and it's manual - it's managed by the programmer, whereas the stack it's
automatic.

Memory on the heap is allocated using the `malloc` function, and it's freed
using the `free` function.

<!--- cspell: disable --->

```c
int main(int argc, char *argv[]) {
    char *buf1 = malloc(128);
    read(fileno(stdin), buf1, 200);
    free(buf1);
}
```

<!--- cspell: enable --->

The heap is concretely handled by an allocator, which knows how and where to
choose a block of memory to write onto. The allocator handles both `malloc` and
`free` calls. The allocator works using **chunks** of memory: an atomic block of
memory. Each chunk contains data and metadata, used by the allocator to link the
different chunks together. These metadata include:

- `prev_size`, the size of the previous chunk, if it's free.
- `size`, the size of the current chunk. Ends with a 1 if the previous chunk is
  used, ends with a 0 otherwise.
- `fd`, points to next free chunk, only if the current chunk is free.
- `bk`, points to the previous chunk, only if the current chunk is free.

![heap memory layout](./assets/images/heap_memory_layout.png)

Upon calling the `free` function, the allocator writes the `fd` and the `bk`
property inside the freed chunks.

When the heap is empty, it contains only the so-called **top chunk**, which
contains metadata regarding the heap itself. When allocating new chunks, they
are subtracted from the top chunk. The start of the top chunk is always pointed
at by `AV_TOP`, which is moved when allocating new chunks. The top chunk
represents at any point in time the remaining available memory on the heap.

#### The attack

Heap overflow attacks are targeted at writing inside the chunk's metadata, to
execute arbitrary code. When unlinking a chunk from the double-linked list which
is the top chunk - so that it can be used by the program - several operations
occur.

```c
void unlink(malloc_chunk *P, malloc_chunk *BK, malloc_chunk *FD) {
   FD = P -> fd;
   BK = P -> bk;
   FD -> bk = BK;
   BK -> fd = FD;
}
```

![unlink](./assets/images/unlink.png)

The attacker can exploit these operations, to make the return address of the
current function point at a buffer on the stack. It is sufficient to have:

- `FD -> bk = return address`
- `P -> bk = address of the buffer`

![unlink exploit](./assets/images/unlink_exploit.png)

The `unlink` function has been eventually patched, by checking if the pointers
are pointing to the correct memory location, before performing any action.

#### A new attack: the house of force

This type of attack needs some pre-conditions - vulnerabilities: the program
needs to use three `malloc`, the second of which needs to accept a user
controllable size; the program needs to have a buffer overflow vulnerability -
i.e. copying a user controllable buffer onto a buffer with fixed dimension.

```c
buf1 = malloc(256);
strcpy(buf1, argv[1]); // has to be vulnerable to overflow
buf2 = malloc(argv[2]);
buf3 = malloc(256);
strcpy(buf3, argv[3]); // doesn't need to be vulnerable to overflow
```

1. The first `malloc` gives the attacker a place in which to perform the
   overflow.
2. The first `strcpy` allows the attacker to perform the overflow, writing in
   the `size` metadata field of the top chunk, tricking the allocator into
   thinking that the whole process's memory is part of the heap, and therefore
   can be allocated.
3. The second `malloc` allows the attacker to move the `AV_TOP` in an arbitrary
   position - which will be the position of the return address - by allocating
   as much memory as necessary - the delta between the actual position of
   `AV_TOP` and the return address.
4. The third `malloc` will make the buffer point to the return address directly.
5. The last `strcpy` allows to write an arbitrary address on the return address,
   to change the flow of the execution.

### Use after free vulnerability

Unlike the previous attacks, which attack spacial properties of the program,
this attack tries to exploit timing properties. It's based on a very well know
vulnerability: use after free - _UAF_. It's one of the most attacked
vulnerabilities, at this point in time.

This vulnerability exists when a pointer to an object that has been freed is
deferenced[^deference] again.

```c
char *foo() {
    char p, *q;
    q = &p;

    return q;       // de-allocation on the stack
}

int main() {
    char *a, *b;
    int i;

    a = malloc(16);
    b = a + 5;

    free(a);
    b[2] = 'c';     // use after free - heap

    b = foo();
    *b = 'c';       // user after free - stack
}
```

[^deference]:
    The operation of accessing the allocated space in memory at the
    address pointed by the pointer that is being deferenced. It is performed
    using the operator `*`.

This vulnerability usually generates a segmentation fault, but can also lead to
information leakage or arbitrary code execution. The goal is to re-allocate
sections of memory that have been de-allocated, so that the previously dangling
pointer now points to arbitrary data. In modern languages this vulnerability is
avoided. i.e. in Java this is handled by the garbage collector.

Tools exist to parse the code and find this kind of vulnerabilities. They are
very conservative. Hence, they produce a lot of false positives that need to be
distinguished from the real vulnerabilities.

## Low-level defense

### Introduction

As we saw, whenever control characters - i.e. the stack, the heap metadata - and
user input share the same memory, a vulnerability arises in case of errors in
the code of the program. This problem is called _channel problem_. How can this
be avoided? Using different kind of defense:

- _Memory safety_ and _type safety_, properties of the programming languages
  that ensure an application is immune to memory attacks.
- _Mitigation_ techniques, implemented in the OS or in the compiler. They
  analyze the attack and try to reduce the attack surface, denying at least one
  of its prerequisites. They only mitigate the attack, they don't ensure safety:
  - _Stack canaries_, compiler level defense.
  - _ASLR_, OS level defense that enforces memory randomization.
  - _Data execution prevention - DEP_, OS level defense that doesn't allow
    executing code from memory sectors devoted to data.
  - _Control flow integrity - CFI_, compiler level defense that sees every node
    in the program flow graph as a vulnerability, and ensures the graph's
    integrity.
- _Secure coding_, writing secure-by-design code, to avoid having to employ
  defense methods, which are effectively patches to the code.
- _Prevention_, code analysis systems that try to find vulnerability in the
  code.

In designing a defense system, it's important to have in mind the precise
_threat model_ that will be faced. I.e. the attacker will be able to attack only
when the function returns, which means that until the function returns we are
able to perform any check we want.

### Memory safety

Memory safety can be a property of a whole language, but also only of a single
program. Memory safety ensures that an area in memory can only be accessed by
pointers to which that area belongs. The idea has been around for a while, and
the limiting factor has always been performance. The overhead of having memory
safety isn't always acceptable.

Memory safety is built upon _temporal safety_ - against UAF - and _spatial
safety_ - against stack and heap overflow.

#### Spatial safety

The problem that spatial safety tries to address is the fact that when a certain
area of memory is pointed by a pointer, the program can actually use that
pointer to write data that exceeds the size of the area of memory.

We can view a pointer as a triple `(p, b, e)` - a _fat pointer_ -, where:

- `p` is the actual pointer.
- `b` is the base of the accessible memory region.
- `e` is the extent of that region.

```c
int *p;
p = malloc(sizeof(int))
// b == p and e == p + sizeof(int)
```

Access is allowed if and only if `b <= p <= e - sizeof(typeof(p))`.

```c
int x; // sizeof(int) == 4
int *y = &x; // p = x, b = x, e = x + 4
int *z = y + 1; // p = x + 4, b = x, e = x + 4
*y = 3; // x <= x <= x + 4 >> OK
*z = 3; // x + 4 <= x <= x + 4 >> KO
```

**Low-fat pointers** encode this triplet directly into the pointer. This is
because storing it in a shadow memory or in an external data structure is
resource consuming in both space and time. The new pointer is the union between
the pointer itself and its metadata - which cannot be used to address the
memory. For this to work efficiently, the memory needs to be cleverly divided in
chunks. The actual program is instrumented by the compiler to use low-fat
pointers. Dually, the same instrumentation is done to the stack and its
variables. These optimizations don't aim at being complete: the program crushing
is fine, we care about not being attacked. There is obviously still some
overhead, with good usability, though.

#### Temporal safety

Temporal safety ensures that the memory region is still in play. Memory regions
are either _defined_ - allocated and active - or _undefined_ - unallocated,
uninitialized, or de-allocated.

A defense technique already exists, and it's the _garbage collector_, which can
tell at any point for a given region of memory if it's defined or not. The
garbage collector maintains a table in which it keeps track of every pointer and
the region the pointer points to, in a way that allows it to recognize _dangling
pointers_.

The idea is to approximate the behavior of the garbage collector, even if it's
only on certain, delicate areas of the memory.

### Type safety

Even stronger than memory safety. It enforces a type on each object, and
guarantees that each type of data only interacts with types of data which is
supposed to interact with. This check happens at compile time and it's
inherently part of a programming language.

Type safety also allows to control the data flow of our program. We can define
policy to enforce a certain data flow - with allowed type changes and the point
in time when the change is allowed. This is done by using _invariants_ that have
to be always respected.

```java
int{Alice->Bob} x;
int{Alice->Bob, Chuck} y;
x = y; // Allowed, because the policy on x is stricter
y = x; // Not allowed, because the policy on y is looser
```

Typically, type safety is expensive in term of performances. This is why such a
defense is not employed in high-performance languages like C.

### Avoid exploitation

By programming wisely, we can concentrate on the nature of the attack to avoid
bugs and exposing vulnerabilities. We can both make the bug harder to exploit,
making one or more of the attack's prerequisites more difficult, or impossible.
For example, a buffer overflow requires to put the attacker's code into memory,
to get the `EIP` to point to that code and to find the return address. To
mitigate these requirements, we can work on three levels: libraries, compiler
and OS. We can also use advanced testing techniques - i.e. fuzzing.

#### Canaries

We can detect overflows using canaries. The canary typically protects the return
address, but can also be used to protect other data. The instrumentation is done
by the compiler. The canary is inserted into any function's activation frame on
the stack, pushed between the `SFP` and the local variables, so that any
overflow attempt will also overwrite it. Before the `RET` instruction, the
compilers also adds a check to make sure the canary hasn't been altered:

```c
[ESP + 4] == canary
```

The canary can be a random value, but there are other variants. The canary is
checked before returning from the function, and if its current value is
different from the original one actions can be taken accordingly. The random
value can be `XOR`ed with the return address, instead of just comparing it with
itself. This would also detect changes in the return address, not only in the
canary. It is also optimized performance wise. The canary can also be a
terminator value - `/0` -, so that a copy will terminate upon arriving at the
canary.

This doesn't allow the program execution to continue, of course, as the stack is
already compromised. It's important that the attacker doesn't know the value of
the canary, otherwise it could overwrite it with the same value.

Depending on the structure of the program, it could be possible to bypass the
canary. At the end of the day, the canary and the following defense techniques
are still exploitable, but they raise the bar of easiness of the available
attacks.

#### Non-executable memory

_Data execution prevention - DEP -_, makes so that the stack and the heap are
not executable, so that even if the canary is bypassed, the injected code cannot
be executed.

To bypass this defense, we have to drop the shellcode. We can use code that is
already on the machine, adopting another approach, which is called
**return-to-libc** and consists in using the return address to jump at libc
executables, which any C program is compiled from. When performing this attack,
is important to respect the prototype of the called function, putting the
necessary and desired parameters on the stack.

This attack is usually used to deactivate the DEP defense, and go back at using
the stack as executable memory.

The attack surface of this attack can be reduced by loading only the necessary
`libc` functions in memory. This defense also has the advantage of being static:
it doesn't have any overhead at execution time.

#### Address-space layout randomization

To defend the system from the return-to-libc attack, we can use address-space
layout randomization - ASLR. This technique consists in placing standard
libraries and other elements in random places in memory, making them harder to
guess. This is a very powerful technique that is also used as an additional
layer of defense to conceal the location of the return address.

ASLR can be attacked with sprain techniques, with which the attacker fills the
memory of the process entirely, so that even jumping at a random location makes
it likely that the shellcode will be executed.

Modern OSs adopt this technique. Memory areas are shifted as a whole, using an
offset. Locations within those areas are not randomized. It was previously only
applied to library, now it's starting to be adopted for the actual program code.
It needs enough randomness, otherwise it can be brute forced.

## Return-oriented programming

To answer all the limitations imposed by the low-level defenses - especially the
attack surface reduction caused by only loading the needed `libc` functions -
return-oriented programming - ROP - has been adopted. This is not a technique of
exploitation; it's a transversal technique to use along an exploitation
technique. The goal is to jump into library functions, without actually calling
them. We string together pieces of existing code - _gadgets_ - to compose an
arbitrary function.

On top of the actual memory we build an execution machine. Its execution memory
is the stack. Its program counter is the `ESP`. Its executable units are the
gadgets. A **gadget** is a functional block: a group of instructions that ends
with a `ret`. The gadgets are stringed together using their final `ret`
instructions. The gadgets receive their parameters from the stack, which is
manipulated using `pop` instructions.

![gadget example](./assets/images/gadget_example.png)

In the injection vector, the gadget is represented by a memory address - where
the gadget can be found - and zero, one or more parameters. The injection vector
will be composed of a sequence of gadgets.

By playing around with the content of the stack, it's possible to reach the
attacker's goal even using a gadget which sequence of instructions that are not
aimed at that goal. Another approach is to look for a specific sequence of
gadgets that behave in the desired way.

If the gadget is not exactly composed by the needed instruction, it's important
that any additional instruction doesn't interfere with the intent of the
sequence of the gadgets.

Another challenge is to find such a group of gadgets. A lot of tools exist to
perform this task automatically. It's been shown that a few number of libraries
can be enough to build Turing complete programs. This is due to Intel CISC
architecture, which allows for instructions with variable length. It is possible
to jump in the middle of a CISC instruction. Depending on the location inside
the instruction where we jump to, the same CISC instruction can behave like many
different instructions. Not all the location will be codified to a valid
instruction. This approach would not be possible on a RISC architecture, which
has a fixed-length set of instructions.

ROP compilers have been created. These compilers need as input the victim
program - to know which gadgets are available -, and can be used to compile a C
program into an injection vector to directly feed to the victim program.

### Hacking Blind

The **threat model** is the following. The target machine is a 64-bit Linux
machine. The attacker knows nothing about the target, except for it being an
NGINX server, with an exposed socket. ASLR, the canary and DEP are in place. The
server needs to have a known buffer overflow. The goal of the attacker is to
bypass the protections to blindly look for a gadget - i.e. a write function -
that allows to leak the NGINX binary and copy it to the memory of the attacker.

```c
write(
  sd, // socket descriptor [0-256], to write on the socket.
  buffer, // the memory address of the binary to dump.
  length
)
```

Once the attacker has the binary, he can use ROP to attack the program. To
succeed, the attacker has to guess both the socket descriptor and the binary
address in memory. This kind of attack requires ~20 mins and 4000 requests.

The first requirement is a generic method to read the canary and the return
address from memory. The return address is important because it gives an
estimate on where the binary is located in memory. We need a way to **read the
stack**. To do so, we exploit a weakness of ASLR in a multi-thread environment.
The values for the canary and the ASLR are the same for each thread, but a
thread crashing won't cause the whole application to crash. This makes the crash
an observable and repeatable behavior that the attacker can use to deduce both
the canary and the ASLR address.

To guess the canary, the attacker causes an overflow that overwrites only until
the first unknown byte of the canary. The attacker then tries every possible
value for that byte, until the connection isn't closed, which means the correct
value has been used. At that point, the attacker keeps that byte and proceeds to
overwriting the following byte, until the entire canary is correctly guessed.
The same technique can be used to guess the return address.

The second requirement is being able to look for the **write gadget**. The write
function is composed of a series of `pop` operations, followed by the `syscall`.

```c
// First part
pop rdi; ret // sd
pop rsi; ret // buffer
pop rdx; ret // length

// Second part
pop rax; ret // write syscall number
syscall
```

The attacker also needs to put the parameters for these instructions on the
stack. Luckily, `pop` instructions are used all over the place in C programs.
Usually, the hard ones to find are `pop rax` and `syscall`. This is because a
program usually doesn't call a `syscall` directly, but uses libraries instead.
Instead of looking for those two instructions, it's usually better to look for a
`write` call, using a procedure linking table - PLT.

To perform the first part, the binary's gadgets are divided in two groups: the
ones that cause the program to crush - dead gadgets - and the ones that cause
the program to loop, instead of crushing - stop gadgets. To perform this
distinction, different return addresses are used to jump to different gadgets
and observe their behavior.

The ultimate goal is to scan for `pop` gadgets. To do so, the attacker puts on
the stack a dead gadget, followed by a stop gadget, and tries jumping at
different return addresses. If the current return address points to a `pop`
instruction, the pop instruction will "eat" the dead gadget, and the application
will loop, because of the stop gadgets. If the return address doesn't point to a
`pop` instruction, the dead gadget will be executed, and the socket will crash.
Based on the reaction of the socket, the attacker is able to tell which return
addresses point to `pop` instructions.

![scan for pop](./assets/images/scan_for_pop.png)

To find a library call to a write function we exploit the PLT and the GOT data
structures. Those two tables are implemented by the compiler. PLT is a static
table to jump to when running `call write` in the binary. Inside the PLT there
is a jump to the original `write` function, whose address is loaded at runtime
from the GOT table. From then on, the PLT knows where the `write` function is.

![plt got](./assets/images/plt_got.png)

The exploitable behavior is that writing on the PLT doesn't cause a crash of the
program. We are able to distinguish between the PLT and stop gadgets because
jumping a byte before or after a stop gadget causes a crash, whereas jumping
around in the PLT still doesn't cause a crash. By changing the return address,
the attacker has to locate the PLT, and the section dedicated to the write
function inside it.

It is also necessary to try different combinations of the `pop` gadgets, in
order to find the right ones in the right sequence. The last thing to guess is
the address of the web socket - `sd` - to write onto.

## Control Flow Integrity - CFI

It doesn't focus on a single type of attack, which makes it a stronger defense
system - secure by design. It is based upon the behavior of the program to
defend. Once the behavior is defined, a monitor monitors that the program is
behaving as expected. If not, an anomaly is notified. False positives may occur.
This defense system falls in the category of anomaly detectors.

We have to:

- Define an expected behavior. This is done by using a **control flow graph -
  CFG**, which represents the possible execution paths of the application.
- Detect deviations from expectation in an efficient way. **In-line reference
  monitor - IRM** is a technique with which we include checks in the program
  directly - in the source code, or in the binary.
- Avoid compromise of the detector. To achieve this, the system needs to have
  these properties: **randomness** and **immutability**. If the monitor works,
  the program itself is a safe place to store the detector.

The overhead of the defense system is measured with **efficiency**. The original
CFI had an average overhead of 16%. Modular CFI lowered it down to 5%. The
percentage of attack from which the system grants defense is measured with
**efficacy**. Modular CFI is able to prevent usage of ~96% of ROP gadgets.
Another metrics is the percentage of indirect jumps that are prevented by the
defense system. CFI prevents an average of ~99% of those.

In general, CFI it's a very effective systems against attacks the aim at
modifying the execution flow of the problem, but not against manipulation of the
flow in a way that is allowed by the CFG and the IRM. This kind of attack is
called **mimicry**. CFI is also vulnerable to the _non data control_ class of
attacks, which aim at changing the value of variables. This is particularly
effective in attacking embedded systems.

### Control flow graph

The behavior of a program is usually associated to its execution flow, which is
representable with a graph - or a section of it.

One of the different kind of graphs that can be used is the **call graph**,
where the program is divided into functions and its behavior is determined by
the way these functions are connected. This is a static analysis. This graph has
a too coarse grain, which doesn't give us the control we need to avoid being
attacked.

![call graph](./assets/images/call_graph.png)

In the control flow graph, the basic unit is the **basic block**, which has a
finer grain then the function. A function can be composed of multiple basic
blocks. Each basic block ends with an instruction that changes the execution
flow of the program: a _jump_ instruction. Each basic block is _called_ by
someone, is composed by a sequence of instructions that don't change the
execution flow of the program, and ends with a _return_ statement, which jumps
to another basic block.

![control flow graph](./assets/images/control_flow_graph.png)

The CFG has to be defined in advance, during compilation or from the binary. The
flow of the program will be monitored to ensure that it follows only paths
allowed by the CFG. _Direct calls_ need not to be monitored, since the address
is fixed and cannot be changed as long as the code is immutable - which is given
by the threat model. Only _indirect calls_ need to be monitored. Their target
address needs to be calculated at run-time, and cannot be analyzed in advance.

### In-line monitor

Implemented as program transformation. We augment the compiler with plug-ins
that insert control instructions where indirect calls are located. The allowed
destinations are defined using _labels_ for allowed target addresses. These
labels will be determined using the CFG. If the jump address doesn't match any
label, the execution is aborted.

These **labels** can be managed with different approaches. The simplest one, is
to use the same label for every valid target address. Any execution flow change
that doesn't include the chosen label, is blocked. This labeling approach is
susceptible to mimicry attacks. To have even more control, specific labels might
be used for every call.

![simple labeling](./assets/images/simple_labeling.png)

![detailed labeling](./assets/images/detailed_labeling.png)

Injecting code that uses a legal label is not feasible, because we assume
non-executable data. Modifying the labels is not allowed, because the labels are
inside the code, which is immutable.

## Static and dynamic flow analysis

This is not a defensive technique, but a proactive technique to analyze our code
with the goal of finding bugs and vulnerabilities. **Code analysis** may be
applied both to the _source_ code and to the _binary_ of the application. Each
technique may be manual or automatic, static or dynamic. The goal is always to
develop automatic techniques, so that we don't have to rely on humans. Automatic
techniques are of course susceptible to false positives and false negatives.

![code analysis](./assets/images/code_analysis.png)

### Static and dynamic analysis

In **static** analysis we build a mathematical model of the program in order to
be able to analyze each possible execution path, given each possible input,
without actually executing the application. It's an offline computation that
inspects the code and produces results about the code quality. It might make use
of _symbolic execution_ techniques. This type of analysis is very fast, but can
be imprecise, as it relies on an approximation of the program. Compilers are
based on static analysis.

In **dynamic** analysis we analyze some behaviors of the program while actually
executing the program. It's slower but more precise, as it doesn't have to rely
on an approximation. It is able to analyze only one input at a time.

### Soundness and completeness

Each tool has two properties: soundness and completeness. Each tool is a
tradeoff between these two properties and the properties are defined only on the
category of vulnerabilities that the technique aims at detecting.

A detection technique is **sound** for a given category of vulnerabilities if it
can correctly determine that a program doesn't expose a vulnerability of that
category. An unsound technique may have _false negatives_. A requisite for
soundness is being able to explore all executions of the program, which in turn
requires to know every possible input that the program may receive. This has to
be done by _statically_ building an abstraction of the program.

A detection technique is **complete** for a given category of vulnerabilities if
any vulnerability it finds is an actual vulnerability. An incomplete detection
technique may have _false positives_. These techniques also indicates the input
that they used to successfully expose the vulnerability. An input may be
abstract - a category of inputs - or concrete - i.e. an actual injection vector.
This property is better provided by dynamic techniques.

To achieve a good tradeoff between these two properties, usually a tool uses an
hybrid approach, employing both static techniques and dynamic techniques. This
allows to reduce both false positives and false negatives. No tool exists which
achieves a perfect balance.

## Symbolic execution

A test added by a software developer checks only one input. Symbolic execution
aims at generalizing tests, increasing their soundness. It allows to use unknown
symbolic variables in assertions.

```python
user_input = α if symbolic_execution else 3
assert(f(user_input) == 5)
```

Symbolic execution is very compute-intensive. It also has limits as the size of
the program increases.

### Symbolic expressions

With symbolic execution it's possible to expand the language support for
expressions _e_ to include symbolic variables representing **unknowns**.
Symbolic variables are introduced when reading input. The language interpreter
should also be able to compute symbolically.

![symbolic expression](./assets/images/symbolic_expression.png)

![symbolic expression example](./assets/images/symbolic_expression_example.png)

### Execution paths

If an execution path depends on an unknown, conceptually the symbolic executor
forks. To each path, a **path condition** is associated, which is the logic
formula that has to be satisfied by the input in order to end up in that
execution path. A path if _feasible_ only if its path condition is satisfiable.
Assertions - i.e. array bounds checks - are translated into conditionals. If the
paths created this way are feasible, there is an input that violates the
assertion - i.e. leads to an out-of-bound access.

![feasibility example](./assets/images/feasibility_example.png) ![feasibility
example 2](./assets/images/feasibility_example_2.png) ![feasibility example
3](./assets/images/feasibility_example_3.png)

Given a path condition, we can use a **constraint solver** to generate a
concrete input that leads on that specific path. This is useful to identify an
input that violates one or more assertions. Constraint solvers have high
computation requirements.

![symbolic execution](./assets/images/symbolic_execution.png)

Each symbolic execution path stands for many runs of the actual program. Thanks
to this, the execution graph helps granting soundness. Constraint solvers allow
for completeness. Practically, soundness cannot be achieved because symbolic
execution usually doesn't terminate. Luckily, the actual aim is not perfect
soundness. It's enough to identify delicate areas of the code to which pay
attention.

### Execution algorithm

The different execution paths are explored using a classic depth first graph
visit algorithm. The algorithm checks feasibility during execution and queues
only feasible paths.

![execution algorithm](./assets/images/execution_algorithm.png)

This is a completely static approach. The hybrid solution which tries to balance
the tradeoff between soundness and completeness is called **concolic
execution**. This is also called dynamic symbolic execution and instruments the
program to do symbolic execution as the program concretely runs. This is done by
using a few concrete inputs. For each condition encountered during the execution
of one path, its expression is stored in a _shadow memory_. When the execution
of a path is completed, one of the expressions stored in the shadow memory is
pulled and symbolic execution is applied on it. By doing so, we achieve two
goals: we identify delicate areas of the program more quickly and we ease the
job of the constraint solver, given that we start from concrete inputs.

At some point, the symbolic executor will reach the "edges" of the program:
libraries, assembly code. This code is extremely complicated, and the executor
might get stuck. To avoid this, the executor may rely on simpler version of the
libraries, or on making models/interfaces of the code - i.e. if the library is
supposed to return the length of a string, the executor just considers this
output, without actually executing the code.

### Search

As said, symbolic execution is simple and useful on paper, but computationally
expensive in practice - when even feasibly solvable by the SMT solver. Because
of this, symbolic execution boils down to a research problem.

One of the issues is **path explosion**. The branches to analyze can increment
exponentially - i.e. with three `if`s based on three different variables we get
eight different paths. Looping on a symbolic variables makes the situation even
worse. With respect to this issue, generic static analysis has the benefit of
always terminating, taking advantage of some approximation on loops and branch
conditions. Such approximation cannot be performed in symbolic execution, which
has the goal of providing a well-defined input to trigger the bug. This
approximation generates false positives.

This search can be performed with different strategies, starting from the basic
ones: **depth-first search** and **breadth-first search**. This basic algorithms
have the drawback of not being guided in any way. They will try to explore the
whole graph, with the risk of not terminating - especially DFS. BFS behaves
better, but is more intrusive to implement. In practice, we need to prioritize
the search, to think about the program as a DAG, and think about a specific
search algorithm.

When using the **randomness** algorithm, the next path to explore is picked
uniformly at random among equal priority paths. The search is randomly restarted
if anything interesting haven't been found in a while. This allows to avoid
getting stuck. This algorithm leads to good results since it doesn't have
specific requirements. The main issue is the lack of reproducibility of the bug,
which force us to keep track of the random choices made.

**Coverage-guided heuristics** try to visit statements that haven't been seen
before. To do this, we keep a score for each instruction, which reflects how
many times the instruction has already been executed. When picking the next
instruction to visit, we choose the one with the lowest score. Errors are often
in hard to reach parts of the program, which this algorithm makes an effort to
reach. On the other side, this approach may never be able to get to a statement
if the proper precondition has not been set up.

**Generational search** is a hybrid between BFS and coverage-guided search. In
the first generation we pick one program at random, and execute to completion.
In the second generation, we take the paths from the first generation, and
negate one branch condition on a path, to yield a new path prefix. The same is
done for each following generation.

**Combined search** runs multiple search algorithm at the same time, alternating
between them. There is no solution that fits all cases. It always depends on the
conditions needed to exhibit the bug.

### Satisfiability Modulo Theories - SMT

The SMT - a SAT logical expressions solver - is in charge of solving the path
condition, finding the values for which the path condition is satisfied. The SMT
theories try to extend SAT with programming languages's constructs. An example
of SMT solver is Microsoft's **Z3**:

![z3](./assets/images/z3.png)

The program is instantiated and turned into a boolean, SAT solvable, model by
the theory reasoner. The SAT solver tries to solve the model. If the SAT solver
fails, the theory reasoner simplifies the problem by approximating it even more,
and the cycle continues. SMT level optimization is critical to have reasonable
performance - i.e. removing unused variables.

### KLEE

Works on source code compiled with _LLVM_. Forks the execution by running
multiple branches at the same time. It employs multiple strategies, primarily
randomness and coverage-guided heuristics. It mocks up the environment to deal
with syscall, libraries, etc.

## Fuzzing

Fuzzing is a kind of random testing. In some aspects, it is like symbolic
execution, but tries to fix its issues - i.e. it tries to be much more
performant. It complements normal testing. There are different kind of fuzzing:

- **Black box**. The fuzzing tool doesn't know anything about the program, it
  just give random - often anomalous - inputs to it and see what happens. It's
  easier, but only able to explore shallow states.
- **Grammar based**. The fuzzing tool has more consciousness on what needs to be
  tested. The inputs are built coherently with the grammar of the program - or
  not, to test anomalous scenarios. The set up is more complex, but the tool is
  able to explore deeper states. Usually different inputs are used to explore
  the program and to trigger the vulnerability.
- **White box**. The fuzzing tool is to some extent informed about the code of
  the program. It is often easy to use, but computationally expensive.

A big part of fuzzing is how to create, mutate and assign a score to the inputs.
Usually, we start from a legal input - which may be human, grammar, or SMT
produced - and mutate it - i.e. by changing some of its bits.

When a crash occurs, the problem is to know what is the **root cause**. The
point of vulnerability and the point of crash may be very different. It is also
important to determine if different input signal the same bug, so that not all
of those inputs need to be analyzed. After identifying the root cause, we may
also want to know if the program is actually exploitable.

Before fuzzing, it is possible to compile the program using tools that
instrument it to return different kind of signals upon errors - i.e. _Address
Sanitizer_ instruments accesses to arrays to check for overflows and use after
free errors. All these tools share the same issue: they slow down the testing.

Symbolic execution can be seen as a specific case of white box fuzzing. Symbolic
execution performs way better if the condition to be triggered is very specific.
On the other hand, symbolic execution is incredibly expensive. Since it is so
slow, it's unrealistic to achieve 100% of coverage. Also, the constraint solver
works only for linear arithmetic. It is also unrealistic to know all information
about the application - i.e. API, environment, etc - forcing us to use models.
The fuzzier goes around this limits by focusing only on the inputs. Some hybrid
forms of fuzzing exist, in order to exploit the advantages of both techniques.

### QSYM

QSYM is the state of the art of concolic execution and hybrid fuzzing. It was
born in the context of a DARPA challenge. QSYM reduces the usage of IR[^ir] -
intermediate representation -, which is possible thanks to the prior knowledge
of the program and where to use symbolic execution, to improve performance by
30%. QSYM also reduces the usage of snapshots[^snapshots], which is very costly.
QSYM uses concrete inputs to explore the environment of the program, instead of
modeling it. This leads to finding more bugs. QSYM was also able to simplify the
formulas that the constraint solver has to solve by removing redundant checks:

```python
if(a > 5):
   pass

if(a == 4): # This is redundant!
   pass
```

Thanks to these optimizations, QSYM is not only able to be more precise than
other fuzzers, but also to have a better coverage, given the better
performances.

[^ir]: Used when compiling the program to handle unknowns like registry values.
[^snapshots]:
    Used in symbolic execution when changing from one path to another,
    as a snapshot of the machine state in the last common node. Also referred as
    shadow memory.

## Meltdown and Spectre

These techniques are able to exploit the hardware of modern processors,
especially when heavy optimizations are performed. Their target is borderline
between software and hardware. They invalidate some of the axioms of
programming. Modern architectures are fundamentally flawed and need to be
re-built, which is not easy due to the process of optimization that has been
carried on during time.

They are based on **side channel** cache attacks. These attacks existed already.
By observing the environment in which the processor operates, it is possible to
understand some of its behavior. An example of metric that can be analyzed is
the electric consumption of the processor - i.e. multiplication will take much
more time than addition. The execution time of an operation can be used to
distinguish between different operations or operands - i.e. a multiplication for
0 will take much less time than a multiplication for 42.

An example of attacking the cache is the following. This attack is possible when
two processes - a spy process and the victim process, whose code is known -
share the same memory. The spy process operates the so called _flush & reload_
attack. When it's its turn to execute, it flushes the cache. When the turn of
the victim process come, it will load some virtual address in the cache. When
the execution is back to the spy process, it reloads the whole cache, and pays
attention to the time needed to reload each virtual address. Now the process
knows which virtual addresses have been used by the victim process - the ones
for which less time is needed to reload, as no reload will be performed there -
and can deduce which point in the code the victim process is executing.

![cache](./assets/images/cache.png)

![side channel](./assets/images/side_channel.png)

These attacks are also based on **speculative execution**. Modern processors
execute batch of sequential instructions in parallel, hoping that they don't
have dependencies among each others. When they don't, the processor is able to
increase its performance, when they do exceptions may be raised. Hence, the
results of these operations are temporarily stored in a shadow memory, which is
committed on the real architecture only when we are sure that no exceptions are
raised. Updates to the shadow memory are also carried out in the cache, though,
acting as the attack surface.

![speculative execution](./assets/images/speculative_execution.png)

### Meltdown

Meltdown allows to read outside the memory of a process, including inside the
kernel, which memory was considered accessible only using `syscall`. No software
vulnerabilities are needed to perform this attack. Meltdown takes advantage of
speculative execution and the cache, to access content that lies in protected
parts of the memory - typically the kernel. The attacker writes its own Meltdown
program, which will execute normally, without any privileges. Due to the way
speculative execution works, when trying to access a protected location of
memory, a sequence of instruction like this one:

```python
target_data = x[kernel_address] # This will raise an exception!!!
access(probe_array[target_data * 4096])
```

Will throw an exception, but will access the memory location anyway and update
the shadow memory - and, most importantly, the cache - accordingly.
`probe_array` is a normal array defined in the user space of the process. `4096`
is the size of each line of the cache. By doing so, a single line of the cache
will be used, the one indexed by `target_data`. At this point, it's sufficient
to time the access to every line of the cache to identify which line has been
filled. `target data` can be computed as the unknown in this equation:

```bash
address = probe_array + target_data * 4096
```

Where `address` is the user space address that took the most time to be accessed
in the cache. Repeating this for each value of `kernel_address` allows to map
the entire kernel in user space.

![meltdown](./assets/images/meltdown.png)

### Spectre

Whereas the Meltdown attack has its own process, Spectre uses an injection
vector on an already existing process.

Spectre is able to read reserved areas of a process - i.e. the canary - using an
injection vector. It takes advantage of **branch prediction**, an optimization
technique that the processor uses, based on the assumption that if an execution
branch has been chosen multiple times in the past, it will be chosen again in
the future.

The attacker creates injection vectors for an already existing program in
memory. This injection vector makes so that the same execution branch is chosen
a lot of times, just to then abruptly switch to another branch. Due to the way
branch prediction and speculative execution work together, the instructions that
are part of the first branch will still be executed, and the results saved in
the cache. From here on, the same approach used for the Meltdown attach allows
to read the valued that has been written inside the cache. I can access
protected locations of memory by doing something like:

```python
array = [1, 2, ..., 50]
addresses = read() // [1, 1, 1, ..., 51]

for address in addresses:
   if (address < array_len):
      y = array[address] #  array[51] will be executed due to branch prediction
```

### Solutions

Removing this optimization is not an option, as the performances would drop
dramatically. Protecting the cache is possible, but wouldn't solve the problem,
as the cache is not the only building block from which to gain this kind of
information.

Using **fence Assembly directives** allows to declare portions of code in which
not to use speculative execution, protecting the program from Spectre. This
solution is not optimal because it introduces a lot of sequential computation
and overhead.

Firmware solutions are very hard to implement, as the processor is unaware of
the context and where instructions come from - it's unable to distinguish user
or kernel instruction.

**Kaiser** un-maps the kernel portion of memory every time a user process starts
executing. This makes so that if the user process tries to access the kernel
memory it finds random data. Unfortunately, this introduces a 50% of overhead.

Some studies have begun to design an architecture that is not vulnerable to
these attacks.
