# Artificial Intelligence

## Table of Contents

1. Neural Networks
   1. Biological Background
      - Motivation
        - Neural networks definition
        - Neuron
      - Biological background
      - Computers vs the human brain
   2. Threshold Logic Units
      - Threshold logic units
      - Geometric interpretation
      - Linear separability
        - Limitations of TSU
      - Networks of threshold logic units
      - Representing arbitrary boolean functions
   3. Training Threshold Logic Units
      - Training threshold logic units
        - Error function
        - Online vs batch training
      - Delta rule
        - Learning rate
        - Convergence theorem
      - Training examples
      - Convergence
   4. Artificial Neural Networks
      - Artificial neural networks
        - Input, output, and hidden neurons
        - Input, activation, and output function
        - Weights
        - Feed-forward vs recurrent networks
        - Operation
      - Examples of neural networks
      - Training neural networks
        - Fixed vs free learning task
      - Preprocessing
   5. Multilayer Perceptrons
      - Multi-Layer Perceptrons
      - Sigmoid activation functions
      - Weight matrices
      - Bi-implication
      - Function approximation
   6. Regression
      - Linear regression
      - Polynomial regression
      - Multi-linear regression
      - Logistic regression
      - Two-class problems
   7. Training Multilayer Perceptrons
      - Gradient descent
        - Manhattan training
        - Flat spot elimination
        - Momentum term
        - Self-adaptive error back-propagation
        - Resilient error back-propagation
        - Quick propagation
        - Weight decay
      - Error back-propagation
      - Number of hidden neurons
      - Cross validation
   8. Sensitivity Analysis
      - Sensitivity Analysis
   9. Deep Learning
      - Motivation
      - Main problems
      - Over-fitting
        - Weight decay
        - Sparsity
        - Dropout training
      - Vanishing gradient
        - Increase interconnection weight
        - Smoother activation function
      - Auto-encoders
        - Sparsity
        - Noise
      - Stacked auto-encoders
      - Convolutional neural networks (CNNs)
        - Receptive field
        - Maximum pooling
   10. Radial Basis Functions Networks
       - Radial basis function networks
         - Network and activation functions
         - Catchment region
         - Distance function
         - Geometrical interpretation
       - Radial activation functions
       - Examples of radial basis function networks
       - Function approximation
         - Anomalies
       - Gaussian functions
   11. Training Radial Basis Function Networks
       - Radial basis function network
       - Initialization
         - Simple radial basis function network
       - C-means clustering
       - Training radial basis function network
         - Gradient descent
       - Recognition of handwritten digits
   12. Learning Vector Quantization
       - Learning vector quantization
         - Clusters
         - Time-dependent learning rate
       - Learning vector quantization networks
       - Learning rules
         - Repulsion ule
         - Window rule
       - Soft learning vector quantization
       - Expectation maximization
   13. Self-Organizing Maps
       - Self-organizing maps
         - Network, activation, and output functions
         - Neighborhood relationship
         - Reference vector
         - Vector quantization
       - Topology preserving mapping
       - Training
       - Examples of self-organizing maps
   14. Hopfield Networks
       - Hopfield networks
         - Network, activation, and output functions
         - Simplified and matrix representation
       - Examples of hopfield networks
       - State graph
       - Convergence
         - Energy function
       - Associative memory
         - Hebbian learning rule
       - Solving optimization problems
         - Constraints
         - Problems
       - Simulated annealing
       - Simulated annealing in Hopfield networks
   15. Boltzmann Machines
       - Boltzmann machines
         - State
       - Update procedure
       - Training
       - Restricted Boltzmann machine
         - Variations
       - Deep Boltzmann machines
   16. Recurrent Networks
       - Recurrent Networks
       - Representing differential equations
       - Vectorial Neural Networks
       - Error Back-propagation in Time
2. Fuzzy Systems
   1. Introduction to Fuzzy Logic
      - Motivation
        - Imprecision vs uncertainty
      - Fuzzy sets
        - Membership function
        - Support and core
        - Linguistic variables and values
        - Use cases
      - Representation of Fuzzy Sets
        - Vertical representation vs horizontal representation
        - Height
        - Fuzzy numbers
      - Multi-valued Logics
      - From Logic to Fuzzy Logic
      - Fuzzy Set Theory
        - Intersection
        - Union
        - _t_-norm and _t_-conorm
      - Fuzzy Set Complement
      - Fuzzy Sets Inclusion
        - Implication
   2. Theory of Fuzzy Logic
      - The Extension Principle
      - Fuzzy Arithmetic
      - Linguistic Information Processing
        - Linguistic variables
        - Fuzzification
        - Operate on membership functions
        - Linguistic approximation
      - Fuzzy Relations
        - Projection
        - Cylindric extension
        - Cylindric closure
      - Binary Fuzzy Relations
        - Domain, range, and height
      - Binary Relations on a Single Set
        - Properties
      - Fuzzy Control Basics
      - Fuzzy Reasoning
   3. Fuzzy Control
      - Tabular approach
      - Rules
      - Mamdani Control
        - Cartpole problem
        - De-fuzzification
      - Takagi-Sugeno Control
      - Fuzzy Control as Similarity-Based Reasoning
   4. Fuzzy Data Analysis
      - Interpretations
      - Clustering
        - Distance
        - Prototype
        - Hard _c_-means
      - Fuzzy Clustering
        - Probabilistic approach
        - Fuzzy _c_-means
        - Outliers
        - Possibilistic approach
      - Cluster Validity
      - Extensions of Fuzzy Clustering
      - Distance Function Variants
        - Fuzzy shell clustering
        - Kernel-based fuzzy clustering
      - Objective Function Variants
        - Covariance
        - Noise clustering
      - Analysis of Fuzzy Data
      - Possibility Theory
      - Fuzzy Random Variables
   5. Neuro-Fuzzy Systems
      - Neuro-fuzzy systems
        - Cooperative approach
        - Hybrid approach
      - From fuzzy rules to network structure
        - 5 steps
      - Neuro-fuzzy control
        - Adaptive-network-based fuzzy inference system
        - Learning
        - Neuro-fuzzy controller model
        - Neuro-fuzzy controller model learning
      - Neuro-fuzzy classification
3. Evolutionary Computing
   1. Introduction to Evolutionary Algorithms
      - Introduction
        - Optimization problems
        - Guided random search
        - Gradual improvement
      - Biological basics
        - Diversity
        - Variation
        - Inheritance
        - Speciation
        - Differential reproduction
        - Randomness
        - Gradualism
        - Opportunism
        - Evolution-strategic principles
        - Ecological Niches
        - Irreversibility
        - Unpredictability
        - Increasing complexity
      - Principles of evolutionary computing
        - Terms and meanings
          - Individual
          - Chromosome
          - Gene
          - Allele
          - Locus
          - Phenotype
          - Genotype
          - Population
          - Generation
          - Reproduction
          - Fitness
        - Building blocks of an evolutionary algorithm
          - Encoding
          - Initial population
          - Fitness function
          - Selection method
          - Genetic operators
            - Mutation
            - Crossover
          - Termination criterion
          - Parameters
      - Introduction Example: The n-Queens Problem
   2. Foundations of Meta Heuristics
      - Meta heuristic
      - Local search methods
        - Gradient ascent or descent
        - Hill climbing
        - Simulated annealing
        - Threshold accepting
        - Great deluge
        - Record-to-record
      - Example: The Traveling Salesman Problem
      - EA-related methods
   3. Elements of Evolutionary Algorithms
      - Encoding
        - Desirable properties
        - Hamming cliffs
        - Epistasis
        - Closeness of the search space
        - Disconnected search spaces
      - Fitness
        - Selection intensity
        - Selective pressure
        - Roulette-wheel selection
        - Premature convergence
        - Vanishing selective pressure
        - Adapting of the fitness function
      - Selection
        - Roulette-wheel selection
        - Dominance problem
        - Vanishing selective pressure
        - Adapting the fitness function
        - Variance problem
        - Expected value model
        - Stochastic universal sampling
        - Rank-based selection
        - Tournament selection
        - Elitism
        - Niche techniques
        - Characterization of selection methods
      - Genetic Operators
        - One-parent operators
          - Bit mutation
          - Gaussian mutation
          - Self-adaptive gaussian mutation
          - Standard mutation
          - Pair swap
          - Shift
          - Inversion
          - Permutation
        - Two- and Multi-parent operators
          - 1-, 2-, and N-point crossover
          - Uniform crossover
          - Shuffle crossover
          - Uniform order-based crossover
          - Edge recombination
          - Diagonal crossover
        - Characteristics of recombination operators
          - Positional bias
          - Distributional bias
        - Interpolating and extrapolating recombination
          - Arithmetic crossover
          - Jenkins nightmare
        - Self-adapting algorithms
   4. Evolutionary Algorithm Meta Heuristics
      - Swarm- and population-based optimization
        - Swarm intelligence
      - Population-based incremental learning
        - Learning and mutation rate
      - Particle Swarm Optimization
        - Local and global memory
        - Automatic parameter adaptation
        - Diversity control mechanisms
      - Ant Colony Optimization
        - Pheromones
        - Prerequisites
        - Pheromone evaporation
   5. Evolutionary Algorithms - Some Theoretical Foundations
      - Motivation
        - Genetic algorithm
      - Schema Theorem
        - Mean relative fitness
        - Defining length
        - Order
        - Building blocks
      - No-Free-Lunch Theorem
   6. Genetic Programming
      - Motivation
      - Genetic programming
      - Symbolic expressions
        - Grammar
        - Desirable properties
        - Well-defined expression
        - Parse tree
      - Implementation
      - Initialization
        - Grow vs full, half-and-half
      - Genetic operators
        - Mutation
        - Crossover
      - Editing
        - General
        - Special
      - Introns
        - Fitness penalty
        - Recombination
   7. Evolutionary Strategies
      - Chromosome representation
      - Selection
        - Elite principle
        - Plus strategy
        - Comma strategy
      - Global variance adaptation
        - Success rule
      - Local variance adaptation
      - Covariances
      - Crossover/recombination
        - Blending
   8. Multi-Criteria Optimization
      - Multi-criteria optimization
      - Pareto-optimal solutions
        - Dominance
        - Pareto frontier
      - Pareto-optimal solutions with Evolutionary Algorithms
        - Vector evaluated genetic algorithm
        - Domination-based approach and niche techniques
        - Non-dominated sorting genetic algorithm
      - Preventing the genetic drift
   9. Parallel Evolutionary Algorithms
      - Parallelization of evolutionary algorithms
      - Which steps can be parallelized
        - Initial population creation
        - Evaluation
        - Fitness computation
        - Selection
        - Genetic operators
        - Termination criterion
      - Island model
        - Pure model
        - Migration model
          - Random model
          - Network model
        - Contest model
      - Cellular evolutionary algorithms
   10. Behavioral Simulation
       - Evolutionary Algorithms for Behavioral Simulation
       - The Prisoner’s Dilemma
         - Payoff matrix
         - Nash equilibrium
       - Genetic Approach
         - Encoding
         - Initialization
         - Fitness
         - Tit for tat
   11. Learning Fuzzy Control
       - Learning Fuzzy Control
       - Approaches
       - Goals
       - Encoding the rule base
       - Mutation
       - Crossover
       - Optimization
       - Disadvantages
       - Encoding the fuzzy partitions
       - Genetic operators
       - Repairing fuzzy sets

## Neural Networks

### Biological Background

#### Computers vs the Human Brain

The computational capabilities of the human brain is much higher. The human
brain is better in parallelizing, has more storage capabilities and a faster
processing speed. The human brain also has some fault tolerance and the
consequent "graceful degradation". Reasonably localized damages can be endured
without compromising the functioning of the whole system. This is why the goal
of mimicking the human brain is so interesting.

### Regression

Training neural networks is closely related to regression. They both start from
a given dataset and an hypothesis about a functional relationship, and try to
minimize the sum of squared errors. Fermat's theorem poses minimum conditions
that allow to construct a system that can be solved with standard methods from
linear algebra. The result - which is called a regression line - is unique,
unless all _x_-values are identical.

Generalizations can be done for _polynomial_ regression, _multi-linear_
regression, _logistic_ regression, and _two-class_ problems.

### Training Multilayer Perceptrons

#### Cross validation

We incur in the risk of under-fitting or over-fitting reality with our model. To
check if this happens we can perform **Cross Validation**. We divide our set of
points randomly in two sets, one that is used to create the model, and one to
check if the model is valid. It’s important for both sets to describe our
reality well.

### Deep Learning

#### Motivation

A modern approach to _multi-layer perceptrons_, with the goal of simplifying the
structure of the hidden layers and avoid the the need to know in advance what
the data will look like and how the system works in order to configure the
network. Using multiple simpler hidden layers may greatly reduce the number of
neurons. We force the extraction of the knowledge contained in the data, to
configure the network, one layer at a time. We try to compress the knowledge to
obtain a high-level view of the data.

#### Over-fitting

We may have difficulties in finding the parameters, due to the over-fitting
caused bu the increased number of adaptable parameters. To address this, we can
use **weight decay**; preventing large weights helps the network not getting
biased towards any neuron path. This will lead to a more dynamic learning and
avoid an overly precise adaptation.

We can also introduce **sparsity** constraints, limiting the number of neurons
in each layer and implicitly forcing the extraction of some knowledge or making
so only few hidden neurons are active.

**Dropout training** consists in ignoring the contribution of some neurons for
the entire computation. This will enforce some specialization on the remaining
neurons.

#### Vanishing Gradient

Having this many layers to back-propagate through may also lead to vanishing
gradient. Because of this, learning in the early hidden layers may happen very
slowly. The simplest solution would be to **increase the interconnection
weights**. By doing so, we would bias the network towards the neurons which
weight we artificially increased, leading to over-fitting. We can slightly
change the activation function to address this gradient issue, for example using
**rectified linear units**, or **softplus function** - which has a much smoother
behavior. These functions, usually assume the shape of a _ramp_, allowing the
function to keep growing and having a influent gradient.

#### Auto-Encoders

A 3 layer perceptron that tries to map the inputs into the outputs maintaining
more approximately the same value. By doing so, if the hidden layer has less
neurons than the number of inputs, we force some knowledge to be extracted, and
I will find it at the output of the hidden layer as a concise view of the input
data. We will need to train the network in order to minimize the error in the
reconstruction. The first two layers are an **encoder**, the last two layers are
a **decoder**.

![auto encoder](./assets/images/auto_encoder.png)

How many neurons do we need in the hidden layer? If the number of hidden neurons
is similar to the number of input neurons this is not very useful. **Sparse
auto-encoders** limit the number of neurons in the hidden layer. **Sparse
activation scheme** reduces the number of neurons which are used during each
training, but not the number of neurons altogether. The principle of these two
approaches is the same, and it's based on the concept of _sparsity_. A different
approach - **de-noising auto-encoders** - adds some noise to the input data,
using random variations. The back-propagation is still performed to the original
dataset.

We can build an auto-encoder by stacking auto-encoders on each other, to
compress the data even more and obtain some more concise knowledge at each step.
Such encoder is called **stacked auto-encoder**. In the first step, I use the
raw input data and I feed it to the first encoder. The decoder should
reconstruct the input of the last encoder. If the reconstruction is successful,
I use the concise view of the data I just generated and I feed it as input to
the next encoder, and so on. At the last step, the decoder reconstructs the
final output. We should be aware that for each layer that we add, we may also
add some error.

![stacked auto encoder](./assets/images/stacked_auto_encoder.png)

#### Convolutional Neural Networks

A tentative to mimic how humans perceive with sight, using our _retina_. It's
proven very useful and efficient to recognize images and writing. It can also be
used for general application when we need to recognize a characteristic in an
image. It associate to a neuron an area of the image the network is observing
and tries to understand what is happening in this area. This area - the
_receptive field_ - is then moved around the image, to understand the whole
picture. Interconnection weights are shared among neurons on the same layer.
Moving around allows to learn the difference between each area and embed this
knowledge in the interconnection weights. This way, the network realize what is
similar and what is different. In the subsequent layers _maximum pooling_ will
be applied, taking the stronger stimulus for each neuron. This allows to learn
what is relevant in each portion of the image.

![convolutional neural
network](./assets/images/convolutional_neural_network.png)

### Boltzmann Machines

Very similar to the _Hopfield_ networks, with differences in how the neurons are
updated and the return of an energy function. The goal is to capture the
behavior of a statistical distribution. If we understand the probability
distribution of a function in its domain, we understand the system in which the
function is being observed. To do so, data should be properly placed in the
domain. We want to define the state of the network as a function of the energy
associated to the state, using a temperature to control the progress of the
machine.

The **state** consists in a vector of the neuron activations. The energy
function uses as parameters the matrix of connection weights and the vector of
threshold values. The probability that a neuron is in an active state is a
logistic function of the energy difference between its active and inactive
state, which is closely related to the network input.

#### Update Procedure

The update steps are the following. A neuron is chosen randomly and its energy
difference and probability of being active are computed. The neuron is activated
according to the computed probability. This update is repeated many times for
different random neurons. Meanwhile, we progressively reduce the _temperature_,
like in _simulated annealing_. This process is called Markov-Chain Monte Carlo
simulation. The network will converge independently from the initial activation
state.

#### Training

Training works differently than an Hopfield network. We separate the network in
two different parts: visible - input and output - neurons, and hidden neurons.
The error is minimized by gradient descent, with the goal of capturing the
probability distribution of the function. **Kullback-Leibler information
divergence** method measures the difference between the two probability
distributions, the original one and the one produced as output.

The machine is run twice. In the first phase, visible neurons are frozen, while
hidden neurons are allowed to update. In the second phase, all neurons are
allowed to update, until thermal equilibrium is reached. The activation
probability is computed both in the positive and in the negative phase, and the
difference is analyzed. The greater the difference, the more the weight of the
specific connection is improved, so that the information that has been learned
will spread in the network. If the difference in the probability is low, the
weight will be slightly changed, or not changed at all. This is called Hebbian
learning rule.

#### Restricted Boltzmann Machines

This training procedure is not very practical, especially with large networks.
This is why restricted boltzmann machines are introduced. Neurons are grouped in
two layers: hidden neurons and visible neurons. Connections may occur only
between neurons on different layers.

![restricted boltzmann
machine](./assets/images/restricted_boltzmann_machine.png)

The training procedure is more efficient, because we can take advantage of this
biparted structure by running the computation of each neuron of the visible
layer in parallel. In the first step we fix the visible units value with the
dataset and we update hidden units at once, in parallel. Then, hidden units are
fixed and we update the visible unit at once, in parallel. Hidden units are
updated once more in the same way and then I adjust the connection weights by
considering how much the probability distribution has changed.

Several variations have been introduced to speed up the process: _momentum
term_, to take into account what happened into the past; _actual probabilities_
instead of binary reconstructions; _online-like training_ based on small
batches, taking into account the error of a few patterns altogether.

Restricted boltzmann machines have also been stacked in a similar way
auto-encoders were, to create **deep boltzmann machines**. The first two layer
network learns some general characteristic on the distribution, then we freeze
its output and we use it as input of the next Boltzmann machine.

## Fuzzy Systems

### Introduction to Fuzzy Logic

#### Motivation

Humans describe the world with many linguistic terms which don't describe exact
quantities - E.g. this person is _tall_. The notions we express with these terms
are _imprecise_. From imprecision comes _graduality_: an individual may fully
possess a characteristic, to a certain degree or not at all. A continuum of
values exists, and a catachrestic may assume any value on this continuum. This
is also true for truthfulness: a proposition may be fully true, partially true
or not true at all - hence, false. Imprecision is different from _uncertainty_,
which refers to situations involving imperfect or unknown information.

- imprecision: today the weather is fine.
- uncertainty: tomorrow the weather will probably be fine

Classic mathematical approaches are not suited to capture and manipulate these
terms in order to derive results from them. This is why we need to introduce
Fuzzy Logic and Fuzzy Systems.

Fuzzy Logic and Fuzzy Systems are also used as mechanism for abstraction of
unnecessary or too complex details. The advantages of abstracting from the exact
numbers are useful in many fields.

#### Fuzzy sets

![fuzzy set](./assets/images/fuzzy_set.png)

A fuzzy set describes how much each member of the universe possess the
characteristic that the fuzzy set tries to describes. A member of the universe
has a **membership function** of 1 if it fully possess the characteristic, 0 if
doesn't possess it at all, or a value in between.

![fuzzy set example](./assets/images/fuzzy_set_example.png)

The value of the membership function implies a _context_. The membership
function may assume any shape.

![membership function](./assets/images/membership_function.png)

Interval [a, d] is called _support_ of the fuzzy set. Interval [b, c] is called
_core_ of the fuzzy set.

**Linguistic variables** represent attributes in fuzzy systems - E.g. the height
of a person. They assume **linguistic values** - E.g tall, short, etc. Values
usually partition the continuum according to which numerical value are
associated to which linguistic value.

![linguistic values](./assets/images/linguistic_values.png)

We can use fuzzy sets to perform classification and data analysis, to solve
decision-making problem and to approximate reasoning. The membership function
assumes three different interpretation, respectively:

- **similarity**, the degree of proximity from the element of the universe to a
  certain value. This can be used with patterns of data or images.
- **preference**, intensity of preference in favor of the elements of the
  universe, or feasibility of selecting the elements of the universe as value of
  a decision variable.
- **possibility**, the possibility that a certain parameter has value equal to
  the elements of the universe. This can be used in expert systems.

Fuzzy sets offer a natural mapping between linguistic and numerical
representations.

Classic - boolean - sets can be viewed as a special case of fuzzy sets, where
only full membership and absolute non-membership are allowed.

#### Representation of Fuzzy Sets

Two representations exist. In the **vertical representation**, fuzzy sets are
described by their membership function, assigning the degree of membership to
each element of the universe. In the **horizontal representation**, fuzzy sets
are described by their _a-cuts_. Each _a_-cut contains every value that has a
value lower than _a_. The membership is obtained by considering the maximum of
the _a_-cuts to which the element belongs.

The largest degree of membership grade obtained by any element of a set defines
its **height**. If the height of a fuzzy set is 1, the set is said to be
_normal_. Otherwise, it'll be called subnormal. The height is also equal the
maximum _a_-cut.

A set is **convex** if each of its _a_-cuts is convex. Otherwise, it's
_concave_. If a fuzzy set is bounded, closed and convex, it's also called a
**fuzzy number**.

#### Multi-valued Logics

We are familiar with _aristotelian_ logic, used as a formal approach to human
reasoning - also in the context of artificial intelligence. The same logic can
be applied to finite set theory, using the right operands. Both of these systems
are also isomorphic to boolean algebra, which is defined by a quadruple and has
a lot of useful properties. All these three approaches allow to express the same
reasoning and the same information. They also possess counterparts of the same
theorems.

Various **_n_-valued logics** were developed. They usually split the [0, 1]
interval in _n_ parts, associating 0 to absolute falseness, and 1 to absolute
truthfulness. Different operands need to be defined. E.g. generalization of
Lukasiewicz three-valued logic:

![lukasiewicz](./assets/images/lukasiewicz.png)

#### Fuzzy Set Theory

![fuzzy set operators](./assets/images/fuzzy_set_operators.png)

![fuzzy set operators example](./assets/images/fuzzy_set_operators_example.png)

**Intersection** can be seen as the set of elements that possess both
characteristic. For this reason, I can implement it using the minimum of the
membership function. Intersection can be seen as logical _conjunction_.

**Union** cares only if the element possess either of the two characteristic,
hence, I can use the maximum value of the membership function. Union can be seen
as logical _disjunction_.

The **complement** measures how much the element does NOT possess the
characteristic, which is nicely represented by `1 - µ`. Other various
definitions of the complement operation exist. The only requisites are that the
complement of 1 should be 0 - and vice versa - and the operand should be
_decreasing_. If we ask for the operand to be _strictly_ decreasing we obtain
_strict negation_. If we also for the negation to be _involutive_, the negation
is _strong_.

![identity commutativity](./assets/images/identity_commutativity.png)

![associativity monotonicity](./assets/images/associativity_monotonicity.png)

A **triangular norm** - t-norm - is a property that satisfies conditions T1 and
T4, whereas a property is a **triangular conorm** - t-conorm - if it satisfies
conditions C1 and C4. Typically, _min_ is chosen as norm, and _max_ as conorm.
This is because these two operations are the easiest to observe and numerically
process. In some problem, smoother operators might be better - E.g. product and
probabilistic sum.

**Implication** is easily associable to set inclusion. Let's say the implication
asserts that if an element possesses a certain characteristic, it also possesses
a different characteristic; I know for a fact that the fuzzy set associated to
the first characteristic is contained in the set associated to the latter. The
fuzzy implication operator can be defined in various ways - E.g. using t-conorm
as disjunction an fuzzy complement as negation. _R-implication_ is an example of
a different way to represent fuzzy implication. Which one to use depends on the
specific problem. For truth values of 0 or 1, all these approaches collapse to
the classical implication.

### Theory of Fuzzy Logic

#### The Extension Principle

With fuzzy logic we are trying to extend boolean logic. Conceptually, they both
start from a _universe_, and by using a _function_ - that has some _variables_ -
they generate an _output_, which will be also part of the universe. As we try to
extend boolean logic, any kind of norm and conorm can be used to represent
conjunction and disjunction, respectively. If _truth_ is the function that takes
value from the universe of the imprecise propositions to the interval [0, 1],
minimum and maximum operator may operate on the output of the truth function to
represent conjunction and disjunction. minimum and maximum operators can be used
to calculate _inferior_ and _superior_. E.g. extension of the addition:

![fuzzy addition](./assets/images/fuzzy_addition.png)

![fuzzy addition graph](./assets/images/fuzzy_addition_graph.png)

This can be also extended to sets and fuzzy sets.

#### Fuzzy Arithmetic

Using fuzzy sets on real numbers allows to manage numerical and linguistic
information at the same time. Fuzzy numbers can be described using different
shapes.

A fuzzy set is **normal** if at least one element has a membership function of;
which is the same to say that the membership function itself has to be normal. A
fuzzy set is **upper semi-continuous** in a certain point it that point defines
a _compact_ _a_-cut. This property greatly simplifies the representation of the
fuzzy set. **Fuzzy intervals** are fuzzy set defined on the values of the
universe contained in a certain interval [a, b], which membership value is
greater than the minimum of the membership values of a and b. The core of a
fuzzy interval is a regular interval.

#### Linguistic Information Processing

![linguistic to fuzzy](./assets/images/linguistic_to_fuzzy.png)

**Linguistic variables** are defined by a quintuple:

- the _name_ of the variable.
- the _set_ of linguistic terms the variable is allowed to take.
- the quantity - _base variable_ - the variable is supposed to describe.
- the _syntactic rule_ to follow in generating linguistic terms.
- the _semantic rule_ that assigns meaning to every term.

This divides the space of the base variable in different sections, and on each
part I define the membership function assigned o each term. After interpreting
linguistic data as fuzzy data, I can perform any kind of processing on this
data. I will be then able to go back to the linguistic world by means of
_linguistic approximation_. An example of an application of this approach is
clouds modeling in weather analysis.

How can we **operate on membership functions** in an efficient way? When
applying the extension principle, it's useful to represent membership functions
using _a_-cuts instead of vertical representation. They are easily storable in a
computer and to compute. It's convenient to represent the membership function as
a family of the infinite _a_-cuts - which is called _set representation_. In
each point, the membership function will be equal to the superior that we can
extract from such a set - hence, the value of the highest _a_-cut. I will then
operate on the _a_-cuts, building the resulting membership function from the new
_a_-cuts obtained by applying the operator to each one of them. **Interval
arithmetic** also benefits from the horizontal representation of membership
functions. I just apply classic interval arithmetic to the _a_-cuts.

#### Fuzzy Relations

A crisp relation just denotes the presence of absence of an association between
two or more elements from different sets. Fuzzy relations introduces a degree of
membership to a relation. A relation is a subset of the _cartesian product_ of
the sets from which the involved elements are taken. Set operators - union,
intersection, etc - can also be applied to relations. In crisp relations, if an
element of the cartesian product belongs to the relation, its degree of
truthfulness is 1, 0 otherwise. In a fuzzy relation, the degree of membership of
a tuple to a relation, depends on the degree of membership of the single
elements to their own sets. The membership function of each element to the
cartesian product is obtained by applying the t-norm - typically, the _min_
operator - to the membership functions of the single sets involved in the
product. If the number of sets is 2 the problem is obviously simplified.
Sometimes it could be useful to consider just a _subsequence_ of a tuple of the
cartesian product .

![cartesian product](./assets/images/cartesian_product.png)

In this domain, we can manipulate the membership function with new tools.
**Projection** removes some of the elements from the relation, _projecting_ on
those I want to discount, to see how the remaining ones behave in their absence.
**Cylindric extension** performs the reverse operation, adding a new variable to
the relation. Using cylindric extension on the projection results in a relation
that is usually called _cylindric closure_, made of the intersection of the
different extensions. The cylindric closure is an approximation of the original
shape and it may have some differences from it. This is because some information
is lost in projecting membership functions.

#### Binary Fuzzy Relations

Having only two elements simplifies things. The _domain_ is the set in which the
membership function has the maximum value among every possible (_x_, _y_), given
_x_. The _range_ is th set in which the membership function has the maximum
value among every possible (_x_, _y_), given _y_. The _height_ the maximum value
of the membership function for every _x_ and _y_. The relation can be represent
using a _membership matrix_. The inverse of the inverse of this matrix must
coincide with the original matrix. On binary relations we can apply
_composition_ and _inverse composition_. Composition returns pairs, making the
middle tuple disappear; _relational join_ yields triples, instead.

This reasoning can be applied to a **single set**. This can be represent in a
tabular way or with a graph. In this scenario, the relation assumes some nice
properties:

- _reflexivity_, if the degree of membership of the relation is 1 for every
  element in the universe.
- _symmetry_, if the degree of membership is the same on both directions for
  every pair of elements in the universe.
- _transitivity_.

![single set binary relation
representation](./assets/images/single_set_binary_relation_representation.png)

### Fuzzy Control

#### Fuzzy Control Basics

![fuzzy controller](./assets/images/fuzzy_controller.png)

In practice, fuzzy theory allow us to avoid the need of being prohibitively
precise in defining the essential operations that characterize the system we are
observing and that we wish to control. If we have an idea on how the system
should work, and want to enforce some linguistic rules to make it behave in a
certain way, we can use an approximate **tabular approach**.

To use such an approach, we will need some **input variables**, that carry
information on the status of the system, typically collected using _sensors_,
which data will be transformed into numbers by _analog-to-digital converters_.
Real, observed quantities must be transformed into fuzzy numbers - by a
_fuzzification interface_ - which will be the inputs of the decision logic. We
will also need a **control variable**, the quantity that we are able to control.
Finally, we will need a **control function**, that takes arguments from the
domain on which the system is defined - the cartesian product of the domain of
the input variables - to the domain of the control variable. The output must be
transformed from fuzzy to a real number - by a _de-fuzzification interface_ -,
before being used as a _signal_ to control the system.

I formulate a set of linguistic rule and determine the linguistic terms they
should work on, also creating fuzzy sets on them. The struggle is to define the
right boundaries on how to partition the fuzzy sets, to obtain the desired
behavior. Each linguistic term range will have some trapezoidal shape, the
remainder of the domain will be shaped in order to tend to infinite. For each
state of the variables - the fuzzy interval they are in -, a **if-then rule** to
apply has to be defined. A single rule or multiple rules may exist. In case of
multiple rules, they can be _disjunctive_ or _conjunctive_. The best approach
depends on the specific use case.

![fuzzy control rule](./assets/images/fuzzy_control_rule.png)

![disjunctive rules](./assets/images/disjunctive_rules.png)

![conjunctive rules](./assets/images/conjunctive_rules.png)

Once rules are defined, I would like to identify a relation that is a solution
for the system, in the form of an equation. If the _Gödel relation_ is a
solution, I am sure that the relational equation I identified is solvable. This
can be also generalized to a set of fuzzy relations and, as a consequence, to a
set of equations. In case there is no solution, the Gödel solution will still be
a good approximation.

#### Mamdani Control

![cartpole](./assets/images/cartpole.png)

The goal of the **cartpole** problem is to ensure the pole remains standing. If
the pole is falling, the system has to react and apply an appropriate force to
avoid it. The _input variables_ are the angle of the pole w.r.t. the vertical
direction and the angular velocity at which the pole is falling, and the
_control variable_ is the force to apply to the pole. The sets of the variables
are partitioned in a certain number fuzzy sets, we should then specify _if-then
rules_ that define the appropriate reaction to the possible combination of the
statuses of the variables. Not all the combinations need to be specified. In
this example, 19 out of 49 possible combinations have been handled. If a
combination has not been handled, this either means that the combination can't
occur, or that measures are being taken so that the system is not allowed to
reach that status. Each rule combine different variables by calculating the
minimum on the membership functions, whereas the combination of multiple rules
results in the maximum of the membership functions obtained from the evaluation
of the single rules. _De-fuzzification_ is now needed to transform this output
in an actual value for the control variable.

![cartpole table](./assets/images/cartpole_table.png)

![mamdani rule evaluation](./assets/images/mamdani_rule_evaluation.png)

The **max criterion method** suggests to just take the maximum value of the
membership function. If only one point possess this membership degree, that will
be the output of the method. If multiple values are suitable, the method
randomly choses one of them. This method is very simple, but doesn't take into
account the distribution of the membership function.

The **mean of maxima** method requires that the fuzzy set is an interval, and
that it is non empty. This method applies a mean on the values that share the
maximum value of the membership function. If the interval is finite, the method
uses the cardinality of the interval; otherwise, integrals will need to be used.

The **center of gravity** method has the same preconditions, but computes the
center of gravity instead of the mean. The output value will divide the
membership function in two section with the same area. This allows a smoother
behavior. This may not be easy to compute, especially if integrals are
necessary.

#### Takagi-Sugeno Control

Starts as an extension of the Mamdani controller. When writing rules, the output
will not be a fuzzy set, but a - typically linear - function of the input
variables, with a crisp output. Thus, no de-fuzzification method is necessary.
The truthfulness of the antecedent is evaluated in the same way. Each rule which
antecedent is not equal to 0 will contribute to the final output with a weight
that is equal to the truthfulness of the antecedent. The total contributions
will be normalized dividing by the summation of the weights.

![takagi-sugeno rule](./assets/images/takagi_sugeno_rule.png)

#### Fuzzy Control as Similarity-Based Reasoning

Both approaches we have seen are heuristic based, they don't provide concrete
interpretation. Using a generic approach, based on the similarities between the
actions and the reactions could allow us to interpolate the behavior of the
system even if no rules have been defined. We need to define the concept of
similarity, so that we can have a continuity of actions in our control.

For a binary relation to be acceptable as a **similarity** - also called fuzzy
equivalence relation w.r.t the t-norm -, it should enjoy _reflexivity_,
_symmetry_, and _transitivity_. For this reason, the classic concept of distance
is not acceptable, since it wouldn't ensure transitivity. Using Lukasiewicz
t-norm - `max{a + b - 1, 0}` -, we can use `1 - min{d(x, y), 1}` as a fuzzy
equivalence relation, where `d` is a pseudo metric on the fuzzy set. This
similarity is so well suited that it can actually be used to the define the
concept of fuzzy set itself. Obviously, the fuzzy equivalence depends on the
measurement unit, and has to be augmented with scaling and transformation to
integrate different units.

In fuzzy control, we have to deal with some imprecisions in the measurements.
This can be modeled using the fuzzy equivalence we just defined, on the input
domain. We could then compare tuples of input variables and desired outputs, to
provide some continuity on the actions taken. The similarity of the tuple will
be equal to the minimum similarity across the elements of the tuple. If the
control function is only partially given, we can fill the gaps exploiting the
concept of similarity.

### Fuzzy Data Analysis

We can use regular crisp data and use fuzzy system techniques to perform the
analysis. In this case, we want to cluster the elements in a fuzzy way.
Otherwise, we can use fuzzy data in form of fuzzy sets, using random sets or
random variables.

#### Clustering

We can group the data by similarity with an unsupervised learning task. Objects
belong to the same cluster if they are as similar as possible, and vice versa.
Similarity is measured with any distance function: the smaller the distance, the
more similar two data tuples are. Meaningful clusters should have a homogenous
number of members. For each cluster, we identify a **prototype** that represents
the elements of the cluster. The prototype can or cannot be one of the examples
belonging to the cluster. It may be the center of the cluster. The clusters are
built by the **clustering algorithm**.

**Hard c-means clustering** aims at partitioning the entire dataset in a crisp
way: the elements belong either to one cluster or to another. The _boundaries_
between the clusters are hard. _c_, the number of clusters to obtain, is given
as input. Initial centers for the clusters are chosen at random. Each elements
that is not a center, will be assigned to the cluster to which is the closest.
We update the center of the clusters by computing the center of gravity of the
elements of their cluster. This may change one or more of the cluster centers.
With these new centers, we repeat the assignment. The process continues,
typically until the centers don't change significantly anymore. _c_ can be
chosen in different ways. We could just look at how the data is distributed in
the domain. Other, more complex, approaches are _Delaunay triangulation_ and
_Voronoi diagram_. The quality of the clustering depends on the number of
clusters and the initial centers. It's important to try to avoid remaining stuck
in _local minima_. This can be done by trying to keep the clusters as compact as
possible. One may also run multiple iterations with different initializations,
and pick the best use; or use a sophisticated initialization method, like
_hypercube sampling_. The hard nature of this algorithms force us to choose a
single cluster for every data point, which may not be ideal in some situation.

#### Fuzzy Clustering

Allowing gradual membership of data points to clusters introduces the concept of
soft boundaries. Clusters will be fuzzy sets, and each one of them will be
associated with a membership function. Each element will have a membership
degree to each cluster.

This partitioning can be interpreted in a **probabilistic**. In this case, the
number of clusters needs to be lower than the number of elements. The membership
function represent the probability that a data point belongs to a certain
cluster. We are implicitly adding two constrains: for each element, the sum of
its membership degrees across every cluster must be 1 - the element fully exists
in the dataset -, and, for each cluster, the sum of the membership degrees
across each element must be more than 0 - no cluster is empty. As a consequence,
no cluster can contain all data points with full membership.

**Fuzzy _c_-means** uses the same approach of classic _c_-means, but with fuzzy
clusters. The initialization can still be random. The algorithm stops after a
predefined number of iteration or if the changes are not significant anymore.
This method is not as sensitive to the initialization as the hard _c_-means was.
Hence, it's more robust and has less tendency to get stuck in local minima.

How good a cluster is can be measured by **validity measures**. A cluster should
contain a sufficient number of elements and should be as compact as possible -
otherwise the element wouldn't be much similar. Partition _density_ and
_entropy_ both address this aspect.

**Outliers**, if not present in high numbers, can be excluded from the
clustering, since they are not relevant to the core of the clustering. If we
allow the sum of the membership function of an element across all cluster to
exceed 1, we can deal with _noise_ and outliers in a more natural way, obtaining
more intuitive membership assignments. Relaxing this property allow us to use a
**possibilistic** approach. Membership functions represent the possibility of an
element to belong to a certain cluster. With this approach, clusters can
coincide - which is avoidable using cluster repulsion - and some data may not be
covered. Moreover, this approach tends to interpret low membership data as
outliers. Usually, a better solution can be obtained by using the probabilistic
approach to initialize the algorithm, running the possibilistic approach for the
first step and then compting the final result with the probabilistic one.

![fcm pcm](./assets/images/fcm_pcm.png)

#### Fuzzy Clustering Variants and Extensions

Sometimes, data doesn't have the shape of an hyper-plane. **Adjusting** the
distance function may help in dealing with these scenarios - E.g. _Mahalanobis
distance_, used in _Gustafson-Kessel algorithm_.

**Fuzzy shell clustering** uses different distance functions to tackle problem
on different shaped data-sets. **Kernel-based fuzzy clustering** allows to
generalize even more, handling non vectorial data: sequences, trees, graphs,
images. We need to introduce an abstract representation of these elements, the
_Hilbertian_ space. Elements of the domain are mapped to their abstract
representation. Data is handled using dot - scalar - product. The _kernel
function_ will then take two elements from the domain to an element in the
domain of real numbers.

The cluster's optimization function can also be adjusted, for example, using
_covariance_. **Noise clustering** improves the handling of noisy data by adding
one extra cluster, dedicated to embedding outlier elements. This additional
cluster will have a different way to measure the distance of the elements.

#### Analysis of Fuzzy Data

**Upper probability** measures the proportion of elements whose images touch a
given subset. **Lower probability** measures the proportion of elements whose
image is fully contained in a given subset.

The **ontic** view allows for several elements of the set to be true.
**Epistemic** view allows for just one element of the set to be true.

### Neuro-Fuzzy Systems

#### Neuro-fuzzy systems

In a neural network we deal with low level operation on data, to try to
understand the knowledge that lies into the data set. In a fuzzy system we use
an abstract point of view, the one of the rules. Also, neural network are
configurable by learning the desired behavior from the data, fuzzy systems are
not capable of learning, so they exploit the fuzzy description of the fuzzy set.
What we would like to do is to combine the rules of the fuzzy systems with the
capability to learn of the neural networks.

The first approach is **cooperative**. The neural network and the fuzzy
controller work and are configured independently. The neural network generates
and learn, while the fuzzy part optimizes the parameters. **Hybrid** models
configure a single system made of different components that may be neural or
fuzzy. Fuzzy rules can be used as connection weights of the input neurons, or as
activation functions of a hidden layer of neurons. I can then use the t-norm as
activation function of the output neuron.

![hybrid systems](./assets/images/hybrid_systems.png)

#### From fuzzy rules to network structure

When we have a set of rules for our system/controller and we need to transform
it into a neuro-fuzzy structure, we have to follow these steps:

1. for every _input variable_, we create an _input neuron_.
2. for every _output variable_, we create an _output neuron_.
3. for every _fuzzy set_ of an input variables, we create a neuron in the first
   hidden layer, and connecting its respective input neuron to it.
4. for every _rule_, we create a neuron in the second hidden layer, specify a
   t-norm for computing the rule activation, and connect each neuron of the
   first hidden layer to each neuron of the second hidden layer.
5. For a _Mamdani_ controller, connect each rule neuron to the output neuron,
   using the fuzzy set of the rule as connection weight. For a _Takagi-Sugeno_
   controller, create a sibling neuron for each rule neuron and connect all
   input neurons to it. This will compute the output function of the fuzzy rule.
   The resulting network structure can be trained with error back-propagation,
   to reach the final configuration.

An additional normalization step - in the form of a hidden layer on neurons -
may exist. This is because rules's outputs may have different ranges, and
therefore dominate each other.

![fuzzy network structure](./assets/images/fuzzy_network_structure.png)

This model is called **adaptive-network-based fuzzy inference system**. The
first layer receives crisp inputs and determines the degree to which this input
belongs to its neuron's fuzzy set. In the second layer, each neuron corresponds
to a Takagi-Sugeno fuzzy rule. The third layer normalizes the output of the
rules. The fourth layer receives both the normalized output and the initial
inputs with the goal of de-fuzzification. The single neuron in layer five will
combine the outputs using summation.

The **learning** is performed using error back-_propagation_. In the forward
pass, we try to optimize the wights, while in the backward pass we update the
parameters of the neurons. These two steps improve convergence. Gradient descent
are applicable only if differentiation is possible - like with Takagi-Sugeno.
When using Mamdani rules, we don't have any gradient information. What need to
impose some constraints: fuzzy sets must be normal and convex, the cannot
exchange their relative positions and they should always overlap. Other optional
constraint are that fuzzy sets should be symmetric and the membership degrees
should add up to 1. The learning algorithm, other than optimizing, should also
make sure to enforce these constraints.

A neural network may also be used to represent the rules of a **neuro-fuzzy
controller**, or to implement **neuro-fuzzy classification**. We can train a
neuro-fuzzy classification system by creating initial fuzzy partition, from
which the initial rule base will be selected. Then we will adjust the rules
using the forward and backward passes and perform error back-propagation. We
need to apply the same constraints proposed for fuzzy inference systems. We may
also perform an analysis of the quality of the classification, and judge if all
the rules we defined are well suited for the data points.

![neuro fuzzy control](./assets/images/neuro_fuzzy_control.png)

![neuro fuzzy classification](./assets/images/neuro_fuzzy_classification.png)

## Evolutionary Computing

### Evolutionary Algorithm - Some Theoretical Foundations

#### No-Free-Lunch Theorem

There is no algorithm which is better than any other algorithm. This is proven
by the no-free-lunch theorem. Algorithms are more or less suited depending on
the use case.

As preconditions, we give a search space, the space of all optimization
problems, and the assumption that the choice of which problem to analyze follows
a uniform distribution. For each algorithm that we evaluate, we have a vector of
optimal solutions, generated by _n_ executions of that algorithm. Algorithms are
compared in performance using a quality measure. We also define an expected
performance.

The no-free-lunch theorem states that the expected quality of any two
algorithms, computed on the same set of problems, is the same. The consequence
is that we always look in the literature, to see what algorithm has been
typically used for the problem we are facing.

### Learning Fuzzy Control

We want to generate and optimize fuzzy control with evolutionary algorithms.
Mamdani control can be optimized by changing the rule base, the fuzzy
sets/partitions - in shape, location, size, cardinality -, the t-norm or
t-conorm, the parameters of the de-fuzzification method and which input
quantities to use for the rules.

A possible complex approach is to optimize the rule base and the fuzzy
partitioning at the same time. A simpler approach is to optimize the fuzzy
partitioning with an a priori fixed rule base at first, and only after to
optimize the rule base. The inverse is also possible, optimizing the rule base
at first, with a fixed fuzzy partitioning, and the partitioning later, once the
rule base is fixed.

A good control system should satisfies some criteria. First of all, the goal
state should be defined for every potential situation. Then, this goal should be
reached quickly, in order to prevent the control from wasting too much time on
other statuses which are not the optimal ones. Finally, the goal should be found
with minimum effort. The control is applied multiple times, until we reach the
desired equilibrium of our system.

The rule base should be complete: there must be a rule for every combination of
input fuzzy sets. This allow us to run the evolutionary algorithm with complete
knowledge of the possible actions. To **code** a rule base, we can use a
_table_, that mirrors the rule matrix. Otherwise, we can _linearize_ the matrix,
merging the row in one long vector. This way we lose the connections of the
column. This is important, because column connections should involve similar
terms, to ensure continuity.

As for genetic operators, **standard mutation** changes one of the entries,
chosen at random. We can choose a new random term, or limit the mutation, so
that a similar term is chosen - a concept of distance could be involved in this.
This would, as always, ensure come continuity in the actions of the controller.
**Recombination** with _one point crossover_ swaps random inner grids, built by
choosing a random inner point and a random corner. Recombination with _two
points crossover_ will choose two inner points and swap the grids built in this
way.

Optimization of the fuzzy sets - with fixed rule base - can improve the control
unit. **Coding** the fuzzy set can be done by choosing a shape of the membership
function. During the optimization we can try to change the various parameters of
the membership function.

![fuzzy set coding](./assets/images/fuzzy_set_coding.png)

Such a coding gives a quite rigid representation, which might not be well suited
for a lot of genetic operators. It also requires to ensure that the sets don't
overtake each other. What we can do is to try encoding the distribution of the
membership function. For every linguistic term, I represent which parts of the
trapezoid it lies onto.

![fuzzy set coding 2](./assets/images/fuzzy_set_coding_2.png)

Still, mutation can introduce a non-zero element over a 0 and crossover can
extinguish fuzzy sets. It is possible to repair fuzzy sets. The goal is to have
a trapezoidal shape with the core equal to 1, or a triangular shape, with one
point equal to 1. The fuzzy set must be _uni-modal_, not _bi-modal_ - or, more
generically, _multi-modal_.

![repairing fuzzy set](./assets/images/repairing_fuzzy_set.png)
