# Contributing

## Commit format

Be a good [commitizen](https://commitizen-tools.github.io/commitizen/)

### `type` conventions

- **feat**: new content
- **fix**: revise existing content
- **ci**: infrastructure changes
